window.slate = window.slate || {};
window.theme = window.theme || {};

/*================ Theme ================*/
(function(theme, $) {
	$.holdReady( true );
	if (theme.current_object && theme.current_object.page) {
			if (theme.current_object.page.title == 'fr-ca') {
			$.ajax({
				type: 'POST',
				url: '/cart/update.js',
				data: { 'attributes[language]': 'fr'},
				dataType: 'json',
				success: function(data) {
					window.location.href = theme.shop.url;
				}
			});
		}
	}
	$.holdReady( false );
})(window.theme = window.theme || {}, jQuery);
/**
 * Utility helpers
 * -----------------------------------------------------------------------------
 * A collection of useful global functions
 *
 */

/**
 * Initializes the handleize function on the String object.
 * Based on {@link https://github.com/nicbou/stringops stringops} by Nicolas Bouliane.
 *
 * @private
 * @memberof Utilities
 */
function _handleize() {
  // UTF to ASCII map. Removes accents.
  var defaultDiacriticsRemovalap = [
    {'base':'A', 'letters':'\u0041\u24B6\uFF21\u00C0\u00C1\u00C2\u1EA6\u1EA4\u1EAA\u1EA8\u00C3\u0100\u0102\u1EB0\u1EAE\u1EB4\u1EB2\u0226\u01E0\u00C4\u01DE\u1EA2\u00C5\u01FA\u01CD\u0200\u0202\u1EA0\u1EAC\u1EB6\u1E00\u0104\u023A\u2C6F'},
    {'base':'AA','letters':'\uA732'},
    {'base':'AE','letters':'\u00C6\u01FC\u01E2'},
    {'base':'AO','letters':'\uA734'},
    {'base':'AU','letters':'\uA736'},
    {'base':'AV','letters':'\uA738\uA73A'},
    {'base':'AY','letters':'\uA73C'},
    {'base':'B', 'letters':'\u0042\u24B7\uFF22\u1E02\u1E04\u1E06\u0243\u0182\u0181'},
    {'base':'C', 'letters':'\u0043\u24B8\uFF23\u0106\u0108\u010A\u010C\u00C7\u1E08\u0187\u023B\uA73E'},
    {'base':'D', 'letters':'\u0044\u24B9\uFF24\u1E0A\u010E\u1E0C\u1E10\u1E12\u1E0E\u0110\u018B\u018A\u0189\uA779'},
    {'base':'DZ','letters':'\u01F1\u01C4'},
    {'base':'Dz','letters':'\u01F2\u01C5'},
    {'base':'E', 'letters':'\u0045\u24BA\uFF25\u00C8\u00C9\u00CA\u1EC0\u1EBE\u1EC4\u1EC2\u1EBC\u0112\u1E14\u1E16\u0114\u0116\u00CB\u1EBA\u011A\u0204\u0206\u1EB8\u1EC6\u0228\u1E1C\u0118\u1E18\u1E1A\u0190\u018E'},
    {'base':'F', 'letters':'\u0046\u24BB\uFF26\u1E1E\u0191\uA77B'},
    {'base':'G', 'letters':'\u0047\u24BC\uFF27\u01F4\u011C\u1E20\u011E\u0120\u01E6\u0122\u01E4\u0193\uA7A0\uA77D\uA77E'},
    {'base':'H', 'letters':'\u0048\u24BD\uFF28\u0124\u1E22\u1E26\u021E\u1E24\u1E28\u1E2A\u0126\u2C67\u2C75\uA78D'},
    {'base':'I', 'letters':'\u0049\u24BE\uFF29\u00CC\u00CD\u00CE\u0128\u012A\u012C\u0130\u00CF\u1E2E\u1EC8\u01CF\u0208\u020A\u1ECA\u012E\u1E2C\u0197'},
    {'base':'J', 'letters':'\u004A\u24BF\uFF2A\u0134\u0248'},
    {'base':'K', 'letters':'\u004B\u24C0\uFF2B\u1E30\u01E8\u1E32\u0136\u1E34\u0198\u2C69\uA740\uA742\uA744\uA7A2'},
    {'base':'L', 'letters':'\u004C\u24C1\uFF2C\u013F\u0139\u013D\u1E36\u1E38\u013B\u1E3C\u1E3A\u0141\u023D\u2C62\u2C60\uA748\uA746\uA780'},
    {'base':'LJ','letters':'\u01C7'},
    {'base':'Lj','letters':'\u01C8'},
    {'base':'M', 'letters':'\u004D\u24C2\uFF2D\u1E3E\u1E40\u1E42\u2C6E\u019C'},
    {'base':'N', 'letters':'\u004E\u24C3\uFF2E\u01F8\u0143\u00D1\u1E44\u0147\u1E46\u0145\u1E4A\u1E48\u0220\u019D\uA790\uA7A4'},
    {'base':'NJ','letters':'\u01CA'},
    {'base':'Nj','letters':'\u01CB'},
    {'base':'O', 'letters':'\u004F\u24C4\uFF2F\u00D2\u00D3\u00D4\u1ED2\u1ED0\u1ED6\u1ED4\u00D5\u1E4C\u022C\u1E4E\u014C\u1E50\u1E52\u014E\u022E\u0230\u00D6\u022A\u1ECE\u0150\u01D1\u020C\u020E\u01A0\u1EDC\u1EDA\u1EE0\u1EDE\u1EE2\u1ECC\u1ED8\u01EA\u01EC\u00D8\u01FE\u0186\u019F\uA74A\uA74C'},
    {'base':'OI','letters':'\u01A2'},
    {'base':'OO','letters':'\uA74E'},
    {'base':'OU','letters':'\u0222'},
    {'base':'OE','letters':'\u008C\u0152'},
    {'base':'oe','letters':'\u009C\u0153'},
    {'base':'P', 'letters':'\u0050\u24C5\uFF30\u1E54\u1E56\u01A4\u2C63\uA750\uA752\uA754'},
    {'base':'Q', 'letters':'\u0051\u24C6\uFF31\uA756\uA758\u024A'},
    {'base':'R', 'letters':'\u0052\u24C7\uFF32\u0154\u1E58\u0158\u0210\u0212\u1E5A\u1E5C\u0156\u1E5E\u024C\u2C64\uA75A\uA7A6\uA782'},
    {'base':'S', 'letters':'\u0053\u24C8\uFF33\u1E9E\u015A\u1E64\u015C\u1E60\u0160\u1E66\u1E62\u1E68\u0218\u015E\u2C7E\uA7A8\uA784'},
    {'base':'T', 'letters':'\u0054\u24C9\uFF34\u1E6A\u0164\u1E6C\u021A\u0162\u1E70\u1E6E\u0166\u01AC\u01AE\u023E\uA786'},
    {'base':'TZ','letters':'\uA728'},
    {'base':'U', 'letters':'\u0055\u24CA\uFF35\u00D9\u00DA\u00DB\u0168\u1E78\u016A\u1E7A\u016C\u00DC\u01DB\u01D7\u01D5\u01D9\u1EE6\u016E\u0170\u01D3\u0214\u0216\u01AF\u1EEA\u1EE8\u1EEE\u1EEC\u1EF0\u1EE4\u1E72\u0172\u1E76\u1E74\u0244'},
    {'base':'V', 'letters':'\u0056\u24CB\uFF36\u1E7C\u1E7E\u01B2\uA75E\u0245'},
    {'base':'VY','letters':'\uA760'},
    {'base':'W', 'letters':'\u0057\u24CC\uFF37\u1E80\u1E82\u0174\u1E86\u1E84\u1E88\u2C72'},
    {'base':'X', 'letters':'\u0058\u24CD\uFF38\u1E8A\u1E8C'},
    {'base':'Y', 'letters':'\u0059\u24CE\uFF39\u1EF2\u00DD\u0176\u1EF8\u0232\u1E8E\u0178\u1EF6\u1EF4\u01B3\u024E\u1EFE'},
    {'base':'Z', 'letters':'\u005A\u24CF\uFF3A\u0179\u1E90\u017B\u017D\u1E92\u1E94\u01B5\u0224\u2C7F\u2C6B\uA762'},
    {'base':'a', 'letters':'\u0061\u24D0\uFF41\u1E9A\u00E0\u00E1\u00E2\u1EA7\u1EA5\u1EAB\u1EA9\u00E3\u0101\u0103\u1EB1\u1EAF\u1EB5\u1EB3\u0227\u01E1\u00E4\u01DF\u1EA3\u00E5\u01FB\u01CE\u0201\u0203\u1EA1\u1EAD\u1EB7\u1E01\u0105\u2C65\u0250'},
    {'base':'aa','letters':'\uA733'},
    {'base':'ae','letters':'\u00E6\u01FD\u01E3'},
    {'base':'ao','letters':'\uA735'},
    {'base':'au','letters':'\uA737'},
    {'base':'av','letters':'\uA739\uA73B'},
    {'base':'ay','letters':'\uA73D'},
    {'base':'b', 'letters':'\u0062\u24D1\uFF42\u1E03\u1E05\u1E07\u0180\u0183\u0253'},
    {'base':'c', 'letters':'\u0063\u24D2\uFF43\u0107\u0109\u010B\u010D\u00E7\u1E09\u0188\u023C\uA73F\u2184'},
    {'base':'d', 'letters':'\u0064\u24D3\uFF44\u1E0B\u010F\u1E0D\u1E11\u1E13\u1E0F\u0111\u018C\u0256\u0257\uA77A'},
    {'base':'dz','letters':'\u01F3\u01C6'},
    {'base':'e', 'letters':'\u0065\u24D4\uFF45\u00E8\u00E9\u00EA\u1EC1\u1EBF\u1EC5\u1EC3\u1EBD\u0113\u1E15\u1E17\u0115\u0117\u00EB\u1EBB\u011B\u0205\u0207\u1EB9\u1EC7\u0229\u1E1D\u0119\u1E19\u1E1B\u0247\u025B\u01DD'},
    {'base':'f', 'letters':'\u0066\u24D5\uFF46\u1E1F\u0192\uA77C'},
    {'base':'g', 'letters':'\u0067\u24D6\uFF47\u01F5\u011D\u1E21\u011F\u0121\u01E7\u0123\u01E5\u0260\uA7A1\u1D79\uA77F'},
    {'base':'h', 'letters':'\u0068\u24D7\uFF48\u0125\u1E23\u1E27\u021F\u1E25\u1E29\u1E2B\u1E96\u0127\u2C68\u2C76\u0265'},
    {'base':'hv','letters':'\u0195'},
    {'base':'i', 'letters':'\u0069\u24D8\uFF49\u00EC\u00ED\u00EE\u0129\u012B\u012D\u00EF\u1E2F\u1EC9\u01D0\u0209\u020B\u1ECB\u012F\u1E2D\u0268\u0131'},
    {'base':'j', 'letters':'\u006A\u24D9\uFF4A\u0135\u01F0\u0249'},
    {'base':'k', 'letters':'\u006B\u24DA\uFF4B\u1E31\u01E9\u1E33\u0137\u1E35\u0199\u2C6A\uA741\uA743\uA745\uA7A3'},
    {'base':'l', 'letters':'\u006C\u24DB\uFF4C\u0140\u013A\u013E\u1E37\u1E39\u013C\u1E3D\u1E3B\u017F\u0142\u019A\u026B\u2C61\uA749\uA781\uA747'},
    {'base':'lj','letters':'\u01C9'},
    {'base':'m', 'letters':'\u006D\u24DC\uFF4D\u1E3F\u1E41\u1E43\u0271\u026F'},
    {'base':'n', 'letters':'\u006E\u24DD\uFF4E\u01F9\u0144\u00F1\u1E45\u0148\u1E47\u0146\u1E4B\u1E49\u019E\u0272\u0149\uA791\uA7A5'},
    {'base':'nj','letters':'\u01CC'},
    {'base':'o', 'letters':'\u006F\u24DE\uFF4F\u00F2\u00F3\u00F4\u1ED3\u1ED1\u1ED7\u1ED5\u00F5\u1E4D\u022D\u1E4F\u014D\u1E51\u1E53\u014F\u022F\u0231\u00F6\u022B\u1ECF\u0151\u01D2\u020D\u020F\u01A1\u1EDD\u1EDB\u1EE1\u1EDF\u1EE3\u1ECD\u1ED9\u01EB\u01ED\u00F8\u01FF\u0254\uA74B\uA74D\u0275'},
    {'base':'oi','letters':'\u01A3'},
    {'base':'ou','letters':'\u0223'},
    {'base':'oo','letters':'\uA74F'},
    {'base':'p','letters':'\u0070\u24DF\uFF50\u1E55\u1E57\u01A5\u1D7D\uA751\uA753\uA755'},
    {'base':'q','letters':'\u0071\u24E0\uFF51\u024B\uA757\uA759'},
    {'base':'r','letters':'\u0072\u24E1\uFF52\u0155\u1E59\u0159\u0211\u0213\u1E5B\u1E5D\u0157\u1E5F\u024D\u027D\uA75B\uA7A7\uA783'},
    {'base':'s','letters':'\u0073\u24E2\uFF53\u00DF\u015B\u1E65\u015D\u1E61\u0161\u1E67\u1E63\u1E69\u0219\u015F\u023F\uA7A9\uA785\u1E9B'},
    {'base':'t','letters':'\u0074\u24E3\uFF54\u1E6B\u1E97\u0165\u1E6D\u021B\u0163\u1E71\u1E6F\u0167\u01AD\u0288\u2C66\uA787'},
    {'base':'tz','letters':'\uA729'},
    {'base':'u','letters': '\u0075\u24E4\uFF55\u00F9\u00FA\u00FB\u0169\u1E79\u016B\u1E7B\u016D\u00FC\u01DC\u01D8\u01D6\u01DA\u1EE7\u016F\u0171\u01D4\u0215\u0217\u01B0\u1EEB\u1EE9\u1EEF\u1EED\u1EF1\u1EE5\u1E73\u0173\u1E77\u1E75\u0289'},
    {'base':'v','letters':'\u0076\u24E5\uFF56\u1E7D\u1E7F\u028B\uA75F\u028C'},
    {'base':'vy','letters':'\uA761'},
    {'base':'w','letters':'\u0077\u24E6\uFF57\u1E81\u1E83\u0175\u1E87\u1E85\u1E98\u1E89\u2C73'},
    {'base':'x','letters':'\u0078\u24E7\uFF58\u1E8B\u1E8D'},
    {'base':'y','letters':'\u0079\u24E8\uFF59\u1EF3\u00FD\u0177\u1EF9\u0233\u1E8F\u00FF\u1EF7\u1E99\u1EF5\u01B4\u024F\u1EFF'},
    {'base':'z','letters':'\u007A\u24E9\uFF5A\u017A\u1E91\u017C\u017E\u1E93\u1E95\u01B6\u0225\u0240\u2C6C\uA763'}
  ];

  // Generate the diacritics map to replace accented characters.
  var diacriticsMap = {};
  for (var i = 0; i < defaultDiacriticsRemovalap.length; i++) {
    var letters = defaultDiacriticsRemovalap[i].letters.split("");

    for (var j=0; j < letters.length ; j++) {
      diacriticsMap[letters[j]] = defaultDiacriticsRemovalap[i].base;
    }
  }

  /**
   * Replaces accented characters with their non-accented counterparts.<br>
   * Based on {@link https://github.com/nicbou/stringops stringops} by Nicolas Bouliane.
   *
   * @param {boolean} [remove=false] - Whether to remove unicode characters that are not mapped to a non-accented characters.
   * 	This includes all non-ascii characters.
   * @param {string} [replacement=Empty string] - The replacement character(s) to use for invalid characters.
   * @returns {string} String without accents.
   * @memberof Utilities
   * @example
   * // returns 'My accented string$.'
   * 'My áccentéd string$.'.noAccents();
   */
  String.prototype.noAccents = function(remove, replacement) {
    // x00-x7E is the ASCII character range
    var withoutAccents = this.replace(/[^\u0000-\u007E]/g, function(ch) {
      return diacriticsMap[ch] || ch;
    });

    replacement = replacement || '';

    if (remove) {
      withoutAccents = withoutAccents.replace(/[^\u0000-\u007E]/g, replacement);
    }

    return withoutAccents;
  };

  /**
   * Returns a "handleized" version of a string suitable for URLs and file names.<br>
   * Based on {@link https://github.com/nicbou/stringops stringops} by Nicolas Bouliane.
   *
   * @param {string} [seperator=-] - The character to use as a separator.
   * @param {string} [replacement=Empty string] - The replacement character(s) to use for invalid characters.
   * @returns {string} The handleized string.
   * @memberof Utilities
   * @example
   * var myString = 'My áccentéd string$.';
   *
   * // returns 'my-accented-string'
   * myString.handleize();
   *
   * // returns 'my_accented_string##'
   * myString.handleize('_', '#');
   */
  String.prototype.handleize = function(separator, replacement) {
    var slug;
    separator = separator || '-';
    replacement = replacement || '';
    slug = this.noAccents(true, replacement).trim().replace(/[^a-z0-9\s-]/ig, replacement).replace(/\s/ig, separator);

    return slug.toLowerCase();
  };
}

_handleize();

theme.utils = {
  // Mimics the Liquid handleize filter in JavaScript
  // requires the stringpos.js library
  handleize: function(str) {
    return str.handleize();
  },

  /**
   * Created an object from a parameter string.
   *
   * @param {string} object_string - The string to parse to an object.
   * @returns {object} The object created from the string.
   * @memberof Utilities
   * @example
   * // If called on a form that takes a name and email, could return { name: "Joe", email: "joe@diffagency.com" }
   * theme.utils.toObject($form.serialize());
   */
  toObject: function(object_string) {
    var parameters = {};
    var elements;

    if (!object_string) {
      return {};
    }

    // Decode the query string to get original characters
    object_string = decodeURIComponent(object_string);

    // Split into each element
    elements = object_string.split('&');

    for (var i = 0; i < elements.length; i++) {
      var element = elements[i].split('=');
      parameters[element[0]] = element[1] || null;
    };

    return parameters;
  },

  /**
   * Extracts the URL parameters into an object.
   *
   * @param {string} [url=window.location.href] - The URL to parse.
   * @returns {object} The query parameters object.
   * @memberof Utilities
   * @example
   * // Returns { name: "Joe", email: "joe@diffagency.com" }
   * theme.utils.getURLParams('https://example.com/?name%3DJoe%26email%3Djoe%40diffagency.com');
   */
  getURLParams: function(url) {
    var url = url || window.location.href;
    // Get the string after the '?' from the URL
    var query_string = url.split(/\?([^\#]+)/)[1];

    if (!query_string) {
      return {};
    }

    return this.toObject(query_string);
  },

  setURLParams: function(params, options) {
    if (!params) {
      return false;
    }

    var options = options || {};
    options.overwrite = options.overwrite || true;

    var current_params = this.getURLParams();

    var param_keys = Object.keys(params);
    for (var i = 0; i < param_keys.length; i++) {
      var param_key = param_keys[i];
      var param_value = params[param_key];
      current_params[param_key] = !options.overwrite ? (current_params[param_key] || param_value) : param_value;
    }

    var new_param_keys = Object.keys(current_params);
    var query_array = [];
    for (var i = 0; i < new_param_keys.length; i++) {
      var param_key = new_param_keys[i];
      var param_value = current_params[param_key];
      query_array.push(encodeURIComponent(param_key) + '=' + encodeURIComponent(param_value));
    }

    var query_string = '?' + query_array.join('&');

    if (options.push_state) {
      window.history.pushState({}, null, query_string);
    } else {
      window.history.replaceState({}, null, query_string);
    }
  },

  clearURLParams: function(params, options) {
    var options = options || {};

    if (options.push_state) {
      window.history.pushState({}, null, window.location.pathname);
    } else {
      window.history.replaceState({}, null, window.location.pathname);
    }

    if (params && Object.keys(params).length) {
      // If state was pushed to clear, don't push an extra state
      options.push_state = false;
      this.setURLParams(params, options);
    }
  },

  /**
   * Creates a copy of the passed object.
   *
   * @param {object} obj - The object to clone.
   * @returns Copy of the object passed.
   */
  clone: function(obj) {
    if (null == obj || "object" != typeof obj) return obj;
    var copy = obj.constructor();
    for (var attr in obj) {
      if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
    }
    return copy;
  },

  /**
   * Curency formating function.
   *
   * @param {(number|string)} price - The price to format.
   * @param {object} options - The options for the currency formatter.
   * @param {boolean} [options.cents=false] - Wether or not the price is currently in cents.
   * @param {string} [options.locale=Shop locale or 'en-CA'] - Override the locale to format to.
   * @param {string} [options.currency=Shop currency or 'CAD'] - Override the currency to output.
   * @param {boolean} [options.no_zeros=false] - To output the price without decimal zeros.
   * @returns {(string|null)} The price formatted based on the locale and currency.
   * @memberof Utilities
   * @example
   * // Returns '$12.56'
   * theme.utils.toMoney(12.56)
   *
   * // Returns '12,56 $'
   * theme.utils.toMoney('12.56', { locale: 'fr-CA', currency: 'CAD' })
   *
   * // Returns '$12.56'
   * theme.utils.toMoney('1256', { cents: true })
   */
  toMoney: function(price, options) {
    if (!price) {
      console.error('Utilities.toMoney: Invalid price passed.', price);
      return null;
    }

    var price = new Number(price);
    var options = options || {};
    var currency_options = {
      style: 'currency'
    }

    // Assign defaults if no options are passed.
    options.cents = options.cents || false;
    options.locale = options.locale || (theme.shop ? theme.shop.locale : null) || 'en-CA';
    currency_options.currency = options.currency || (theme.shop ? theme.shop.currency : null) || 'CAD';

    // Convert to dollars if the amount passed is in cents
    if (options.cents) {
      price = price / 100;
    }

    // Check if there are decimals and no zero option
    if (options.no_zeros && !price.toString().split('.')[1]) {
      currency_options.minimumSignificantDigits = 2;
    }

    return price.toLocaleString(options.locale, currency_options);
  },

  translateOption: function(name) {
		if (theme.locale != 'fr') {
			switch(name) {
				case 'Très grand':
					return 'Extra Large';
				case 'Très petit':
          return 'Extra Small';
        default:
          return name;
			}
		} else {
      return name;
    }
  }
}

theme.dayMap = {
  "en": {
    "Sunday": "Sun",
    "Monday": "Mon",
    "Tuesday": "Tues",
    "Wednesday": "Wed",
    "Thursday": "Thurs",
    "Friday": "Fri",
    "Saturday": "Sat"
  },
  "fr": {
    "Sunday": "Dim",
    "Monday": "Lun",
    "Tuesday": "Mar",
    "Wednesday": "Mer",
    "Thursday": "Jeu",
    "Friday": "Ven",
    "Saturday": "Sam"
  }
}
$(function() {
    var $navItems = $(".main-nav__list-item");
    var $mainNavContainer = $navItems.parent(".main-nav__list");
    var $submenuNavItems = $(".dropdown_column", $navItems).filter(function() {
        return $(".subsubmenu", this).length > 0;
    });

    $navItems.on("touchstart", function(event) {
        var $thisNavItem = $(this);

        $mainNavContainer.addClass("touch-triggered");

        if($thisNavItem.hasClass("sub-menu-open")) {
            return true;
        }

        $navItems.removeClass("sub-menu-open");
        $submenuNavItems.removeClass("sub-menu-open");
        $thisNavItem.addClass("sub-menu-open");

        return false;
    });

    $submenuNavItems.on("touchstart", function() {
        var $thisNavItem = $(this);

        $mainNavContainer.addClass("touch-triggered");

        if($thisNavItem.hasClass("sub-menu-open")) {
            return true;
        }

        $submenuNavItems.removeClass("sub-menu-open");
        $thisNavItem.addClass("sub-menu-open");

        return false;
    });

    $mainNavContainer.on("mousemove", function() {
        $mainNavContainer.removeClass("touch-triggered");
        $navItems.removeClass("sub-menu-open");
        $submenuNavItems.removeClass("sub-menu-open");
    });
});

/*================ Slate ================*/
/**
 * A11y Helpers
 * -----------------------------------------------------------------------------
 * A collection of useful functions that help make your theme more accessible
 * to users with visual impairments.
 *
 *
 * @namespace a11y
 */

slate.a11y = {

  /**
   * For use when focus shifts to a container rather than a link
   * eg for In-page links, after scroll, focus shifts to content area so that
   * next `tab` is where user expects if focusing a link, just $link.focus();
   *
   * @param {JQuery} $element - The element to be acted upon
   */
  pageLinkFocus: function($element) {
    var focusClass = 'js-focus-hidden';

    $element.first()
      .attr('tabIndex', '-1')
      .focus()
      .addClass(focusClass)
      .one('blur', callback);

    function callback() {
      $element.first()
        .removeClass(focusClass)
        .removeAttr('tabindex');
    }
  },

  /**
   * If there's a hash in the url, focus the appropriate element
   */
  focusHash: function() {
    var hash = window.location.hash;

    // is there a hash in the url? is it an element on the page?
    if (hash && document.getElementById(hash.slice(1))) {
      this.pageLinkFocus($(hash));
    }
  },

  /**
   * When an in-page (url w/hash) link is clicked, focus the appropriate element
   */
  bindInPageLinks: function() {
    $('a[href*=#]').on('click', function(evt) {
      this.pageLinkFocus($(evt.currentTarget.hash));
    }.bind(this));
  },

  /**
   * Traps the focus in a particular container
   *
   * @param {object} options - Options to be used
   * @param {jQuery} options.$container - Container to trap focus within
   * @param {jQuery} options.$elementToFocus - Element to be focused when focus leaves container
   * @param {string} options.namespace - Namespace used for new focus event handler
   */
  trapFocus: function(options) {
    var eventName = options.namespace
      ? 'focusin.' + options.namespace
      : 'focusin';

    if (!options.$elementToFocus) {
      options.$elementToFocus = options.$container;
    }

    options.$container.attr('tabindex', '-1');
    options.$elementToFocus.focus();

    $(document).on(eventName, function(evt) {
      if (options.$container[0] !== evt.target && !options.$container.has(evt.target).length) {
        options.$container.focus();
      }
    });
  },

  /**
   * Removes the trap of focus in a particular container
   *
   * @param {object} options - Options to be used
   * @param {jQuery} options.$container - Container to trap focus within
   * @param {string} options.namespace - Namespace used for new focus event handler
   */
  removeTrapFocus: function(options) {
    var eventName = options.namespace
      ? 'focusin.' + options.namespace
      : 'focusin';

    if (options.$container && options.$container.length) {
      options.$container.removeAttr('tabindex');
    }

    $(document).off(eventName);
  }
};

/**
 * Cart Template Script
 * ------------------------------------------------------------------------------
 * A file that contains scripts highly couple code to the Cart template.
 *
 * @namespace cart
 */

slate.cart = {

	/**
	 * Browser cookies are required to use the cart. This function checks if
	 * cookies are enabled in the browser.
	 */
	cookiesEnabled: function() {
		var cookieEnabled = navigator.cookieEnabled;

		if (!cookieEnabled){
			document.cookie = 'testcookie';
			cookieEnabled = (document.cookie.indexOf('testcookie') !== -1);
		}
		return cookieEnabled;
	}
};

(function($) {

	var settings = {
		bagels: true,
		moneyFormat: window.theme.moneyFormat,
		freeShippingThreshold: window.theme.freeShippingThreshold,
		shippingRate: window.theme.shippingRate
	};

	var cart = new function() {
		function Cart(settings) {

			var lang = theme.cart_lang;
			var money_format = (lang === 'fr') ? "{{amount}} $" : "${{amount}}";

			this.defaults = {
				selectors : {
					cartOpen: ".js-cart-open",
					cartClose: ".js-cart-close",
					cartItems: ".js-cart-items",
					cartItemsCount: ".js-cart-item-count",
					cartItemDelete: ".js-item-delete",
					cartTotal: ".js-cart-total",
					cartFreeShippingDifference: ".js-cart-shipping-difference",
					cartItem: ".js-cart-item",
					cartItemMinus: ".js-cart-minus",
					cartItemQty: ".js-cart-qty",
					cartItemPlus: ".js-cart-plus",
					cartShippingCost: ".js-cart-shipping-cost",
					cartShippingRate: ".js-cart-shipping",
					cartStyleParent: "body", // Container to hold action styling
					cartFlyout: "#cart-flyout"
				},
				classes : {
					cartOpened: "cart__open",
					itemDisabled: "disabled",
					freeShipping: "cart__free-shipping",
					cartEmpty: "cart__empty"
				},
				itemData: {
					id: "item-id"
				},
				moneyFormat: money_format,
				freeShippingThreshold: 5000,
				shippingRate: 500
			}

			this.options = $.extend(this.defaults, settings);

			this.$cartStyleParent = $(this.options.selectors.cartStyleParent);
			this.$cartItemsCount = $(this.options.selectors.cartItemsCount);
			this.$cartItems = $(this.options.selectors.cartItems);
			this.$cartFlyout = $(this.options.selectors.cartFlyout);
			this.itemQuantities = {};

			this.initDynamicSelectors();

			this.init();
		}

		Cart.prototype.initDynamicSelectors = function() {
			this.$cartTotal = $(this.options.selectors.cartTotal);
			this.$cartShippingCost = $(this.options.selectors.cartShippingCost);
			this.$cartFreeShippingDifference = $(this.options.selectors.cartFreeShippingDifference);
			this.$cartShippingRate = $(this.options.selectors.cartShippingRate);
			this.$cartShippingRate.html(this.formatMoney(this.options.shippingRate));
		};

		Cart.prototype.init = function() {
			var _ = this;
			$(_.options.selectors.cartOpen).on('click', function(event) {
				event.preventDefault();
				_.openCart();
			});

			$(_.options.selectors.cartClose).on('click', function(event) {
				event.preventDefault();
				_.$cartStyleParent.removeClass(_.options.classes.cartOpened);
			});

			_.updateCartInfo();
			//_.refreshFlyout(document._shopify_cart.cart);
		};

		Cart.prototype.refreshFlyout = function(data, outerpromise) {
			var _ = this;
			var promise = outerpromise || $.Deferred();

			if (typeof(data) !== 'object') {
				data = JSON.parse(data);
			}

			data.items = data.items || data.line_items;

			$.get('/cart?view=flyout', function(resp) {
				_.$cartFlyout.html(resp);

				_.initDynamicSelectors();
				_.setSubTotal(data);

				_.itemQuantities = {};

				$('.cart__item').each(function() {
					var element = {
						id: $(this).data('item-id'),
						quantity: $(this).find('.js-cart-qty').val()
					}
					_.initItem($(this), element, false);
				});

				$(document).trigger('cart.qty_update');

				promise.resolve();
			}).fail(function () {
				promise.reject();
			});

			// Update empty cart state
			if(data.items.length > 0) {
				_.$cartStyleParent.removeClass(_.options.classes.cartEmpty);
			} else {
				_.$cartStyleParent.addClass(_.options.classes.cartEmpty);
			}

			var item_ids = document._shopify_cart.cart.line_items.map(function(item) {
				return item.product_id;
			});

			$('[data-cart-badge]').find('.badge-cart').remove();
			$.each(item_ids, function(index, id) {
				$('[data-cart-badge="' + id + '"]').append('<img class="product-badge badge-cart" src="' + window.theme.badges.cart + '">');
			});

			_.setCartCount(data);

			return promise;
		};

		Cart.prototype.updateCartInfo = function() {
			if (document._shopify_cart) {
				var promise = document._shopify_cart._refresh();
				document._shopify_cart._run_campaigns();
				return promise;
			} else {
				return false;
			}
		};

		Cart.prototype.setCartCount = function(data) {
			var _  = this;
			_.$cartItemsCount.html(data.item_count);
		};

		Cart.prototype.setSubTotal = function(data) {
			var _  = this;
			var shippingDifference = _.options.freeShippingThreshold - data.total_price;
			var shippingCost = _.options.shippingRate;
			if (data.line_items) {
				data.line_items.forEach(function (line_item) {
					if (line_item.properties && line_item.properties._discounted_shipping) {
						shippingCost = Math.max(Math.min(line_item.properties._discounted_shipping, shippingCost), 0);
					}
				});
			}

			if(shippingCost > 0 && shippingDifference > 0) {
				_.$cartStyleParent.removeClass(_.options.classes.freeShipping);
			} else {
				_.$cartStyleParent.addClass(_.options.classes.freeShipping);
				shippingCost = 0;
			}

			_.$cartShippingCost.html(_.formatMoney(shippingCost));
			_.$cartFreeShippingDifference.html(_.formatMoney(shippingDifference));
			_.$cartTotal.html(_.formatMoney(data.total_price + shippingCost));
		};

		Cart.prototype.setItemQty = function($qty, qty) {
			$qty.val(qty);
		};

		Cart.prototype.initItem = function(item, itemData, prepend) {
			var _ = this;

			var $item = $(item);
			var id = itemData.id;

			$(_.options.selectors.cartItemDelete, $item).on("click", function(event) {
				_.deleteItem(id, $item);
			});

			var $qty = $(_.options.selectors.cartItemQty, $item);

			_.setItemQty($qty, itemData.quantity);

			var min = parseInt($qty.attr('min'));

			$(_.options.selectors.cartItemPlus, $item).on("click", function(event) {
				_.updateQuantity(parseInt($qty.val()) + 1, id, $qty, $item, min);
			});

			$(_.options.selectors.cartItemMinus, $item).on("click", function(event) {
				_.updateQuantity(parseInt($qty.val()) - 1, id, $qty, $item, min);
			});

			$qty.on("blur", function(event) {
				_.updateQuantity(parseInt($qty.val()), id, $qty, $item, min);
			});

			var idValue = itemData.id.split(":")[0];

			if(_.itemQuantities[idValue]) {
				_.itemQuantities[idValue] += parseInt(itemData.quantity);
			} else {
				_.itemQuantities[idValue] = parseInt(itemData.quantity);
			}

			/* if(!prepend) {
				_.$cartItems.append($item);
			} else {
				_.$cartItems.prepend($item);
			} */
		};

		Cart.prototype.addItem = function(formData) {
			var _ = this;
			var result = $.Deferred();

			$.post("/cart/add.js", formData, undefined, "json")
				.then(function(element) {
					// Call cart to update pricing info
					return $.post("/cart.json");
				})
				.then(function(data) {
					return _.updateCartInfo(data);
				})
				.done(function() {
					result.resolve();
				})
				.fail(function(error) {
					result.reject(error);
				});

			return result;
		};

		Cart.prototype.deleteItem = function(itemId, $item) {
			var _ = this;

			$item.addClass(_.options.classes.itemDisabled);

			$.post("/cart/change.js", { id: itemId, quantity: 0 }, undefined, "json")
				.done(function(data) {
					$item.slideUp(function() {
						_.updateCartInfo(data);
					});
				})
				.fail(function() {
					$item.removeClass(_.options.classes.itemDisabled);
				});
		};

		Cart.prototype.updateQuantity = function(qty, itemId, $qty, $item, min) {
			var _ = this;

			if(min !== NaN) {
				qty = Math.max(qty, min);
			}

			$item.addClass(_.options.classes.itemDisabled);

			$.post("/cart/change.js", { id: itemId, quantity: qty }, undefined, "json")
				.done(function(data) {

					var items = data.items.filter(function(item) {
						return item.id === itemId;
					});

					if(items.length > 0) {
						_.itemQuantities[item[0].id] = item[0].quantity;
						$(document).trigger('cart.qty_update');
						_.setItemQty($qty, items[0].quantity);
					}

					_.updateCartInfo(data);
				})
				.always(function() {
					$item.removeClass(_.options.classes.itemDisabled);
				});
		};

		Cart.prototype.formatMoney = function(value) {
			var _ = this;
			var price = _.options.moneyFormat.replace("{{amount}}", (value / 100).toFixed(2));
			return (theme.cart_lang === 'fr') ? price.replace("$", '') + ' $' : price;
		};

		Cart.prototype.openCart = function () {
			var _ = this;
			_.$cartStyleParent.addClass(_.options.classes.cartOpened);
		};

		return Cart;
	};

	// Initialize the cart on load
	$(function() {
		window.Cart = window.Cart || new cart(settings);
	});
})(jQuery);
/**
 * Utility helpers
 * -----------------------------------------------------------------------------
 * A collection of useful functions for dealing with arrays and objects
 *
 * @namespace utils
 */

slate.utils = {

  /**
   * Return an object from an array of objects that matches the provided key and value
   *
   * @param {array} array - Array of objects
   * @param {string} key - Key to match the value against
   * @param {string} value - Value to get match of
   */
  findInstance: function(array, key, value) {
    for (var i = 0; i < array.length; i++) {
      if (array[i][key] === value) {
        return array[i];
      }
    }
  },

  /**
   * Remove an object from an array of objects by matching the provided key and value
   *
   * @param {array} array - Array of objects
   * @param {string} key - Key to match the value against
   * @param {string} value - Value to get match of
   */
  removeInstance: function(array, key, value) {
    var i = array.length;
    while(i--) {
      if (array[i][key] === value) {
        array.splice(i, 1);
        break;
      }
    }

    return array;
  },

  /**
   * _.compact from lodash
   * Remove empty/false items from array
   * Source: https://github.com/lodash/lodash/blob/master/compact.js
   *
   * @param {array} array
   */
  compact: function(array) {
    var index = -1;
    var length = array == null ? 0 : array.length;
    var resIndex = 0;
    var result = [];

    while (++index < length) {
      var value = array[index];
      if (value) {
        result[resIndex++] = value;
      }
    }
    return result;
  },

  /**
   * _.defaultTo from lodash
   * Checks `value` to determine whether a default value should be returned in
   * its place. The `defaultValue` is returned if `value` is `NaN`, `null`,
   * or `undefined`.
   * Source: https://github.com/lodash/lodash/blob/master/defaultTo.js
   *
   * @param {*} value - Value to check
   * @param {*} defaultValue - Default value
   * @returns {*} - Returns the resolved value
   */
  defaultTo: function(value, defaultValue) {
    return (value == null || value !== value) ? defaultValue : value
  }
};

/**
 * Rich Text Editor
 * -----------------------------------------------------------------------------
 * Wrap videos in div to force responsive layout.
 *
 * @namespace rte
 */

slate.rte = {

  wrapTable: function() {
    $('.rte table').wrap('<div class="rte__table-wrapper"></div>');
  },

  iframeReset: function() {
    var $iframeVideo = $('.rte iframe[src*="youtube.com/embed"], .rte iframe[src*="player.vimeo"]');
    var $iframeReset = $iframeVideo.add('.rte iframe#admin_bar_iframe');

    $iframeVideo.each(function() {
      // Add wrapper to make video responsive
      $(this).wrap('<div class="rte__video-wrapper"></div>');
    });

    $iframeReset.each(function() {
      // Re-set the src attribute on each iframe after page load
      // for Chrome's "incorrect iFrame content on 'back'" bug.
      // https://code.google.com/p/chromium/issues/detail?id=395791
      // Need to specifically target video and admin bar
      this.src = this.src;
    });
  }
};

slate.Sections = function Sections() {
  this.constructors = {};
  this.instances = [];

  $(document)
    .on('shopify:section:load', this._onSectionLoad.bind(this))
    .on('shopify:section:unload', this._onSectionUnload.bind(this))
    .on('shopify:section:select', this._onSelect.bind(this))
    .on('shopify:section:deselect', this._onDeselect.bind(this))
    .on('shopify:section:reorder', this._onReorder.bind(this))
    .on('shopify:block:select', this._onBlockSelect.bind(this))
    .on('shopify:block:deselect', this._onBlockDeselect.bind(this));
};

slate.Sections.prototype = $.extend({}, slate.Sections.prototype, {
  _createInstance: function(container, constructor) {
    var $container = $(container);
    var id = $container.attr('data-section-id');
    var type = $container.attr('data-section-type');

    constructor = constructor || this.constructors[type];

    if (typeof constructor === 'undefined') {
      return;
    }

    var instance = $.extend(new constructor(container), {
      id: id,
      type: type,
      container: container
    });

    this.instances.push(instance);
  },

  _onSectionLoad: function(evt) {
    var container = $('[data-section-id]', evt.target)[0];
    if (container) {
      this._createInstance(container);
    }
  },

  _onSectionUnload: function(evt) {
    var instance = slate.utils.findInstance(this.instances, 'id', evt.detail.sectionId);

    if (!instance) {
      return;
    }

    if (typeof instance.onUnload === 'function') {
      instance.onUnload(evt);
    }

    this.instances = slate.utils.removeInstance(this.instances, 'id', evt.detail.sectionId);
  },

  _onSelect: function(evt) {
    var instance = slate.utils.findInstance(this.instances, 'id', evt.detail.sectionId);

    if (instance && typeof instance.onSelect === 'function') {
      instance.onSelect(evt);
    }
  },

  _onDeselect: function(evt) {
    var instance = slate.utils.findInstance(this.instances, 'id', evt.detail.sectionId);

    if (instance && typeof instance.onDeselect === 'function') {
      instance.onDeselect(evt);
    }
  },

  _onReorder: function(evt) {
    var instance = slate.utils.findInstance(this.instances, 'id', evt.detail.sectionId);

    if (instance && typeof instance.onReorder === 'function') {
      instance.onReorder(evt);
    }
  },

  _onBlockSelect: function(evt) {
    var instance = slate.utils.findInstance(this.instances, 'id', evt.detail.sectionId);

    if (instance && typeof instance.onBlockSelect === 'function') {
      instance.onBlockSelect(evt);
    }
  },

  _onBlockDeselect: function(evt) {
    var instance = slate.utils.findInstance(this.instances, 'id', evt.detail.sectionId);

    if (instance && typeof instance.onBlockDeselect === 'function') {
      instance.onBlockDeselect(evt);
    }
  },

  register: function(type, constructor) {
    this.constructors[type] = constructor;

    $('[data-section-type=' + type + ']').each(function(index, container) {
      this._createInstance(container, constructor);
    }.bind(this));
  }
});

/**
 * Currency Helpers
 * -----------------------------------------------------------------------------
 * A collection of useful functions that help with currency formatting
 *
 * Current contents
 * - formatMoney - Takes an amount in cents and returns it as a formatted dollar value.
 *
 */

slate.Currency = (function() {
  var moneyFormat = '${{amount}}';

  /**
   * Format money values based on your shop currency settings
   * @param  {Number|string} cents - value in cents or dollar amount e.g. 300 cents
   * or 3.00 dollars
   * @param  {String} format - shop money_format setting
   * @return {String} value - formatted value
   */
  function formatMoney(cents, format) {
    if (typeof cents === 'string') {
      cents = cents.replace('.', '');
    }
    var value = '';
    var placeholderRegex = /\{\{\s*(\w+)\s*\}\}/;
    var formatString = (format || moneyFormat);

    function formatWithDelimiters(number, precision, thousands, decimal) {
      precision = slate.utils.defaultTo(precision, 2);
      thousands = slate.utils.defaultTo(thousands, ',');
      decimal = slate.utils.defaultTo(decimal, '.');

      if (isNaN(number) || number == null) {
        return 0;
      }

      number = (number / 100.0).toFixed(precision);

      var parts = number.split('.');
      var dollarsAmount = parts[0].replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1' + thousands);
      var centsAmount = parts[1] ? (decimal + parts[1]) : '';

      return dollarsAmount + centsAmount;
    }

    switch (formatString.match(placeholderRegex)[1]) {
      case 'amount':
        value = formatWithDelimiters(cents, 2);
        break;
      case 'amount_no_decimals':
        value = formatWithDelimiters(cents, 0);
        break;
      case 'amount_with_space_separator':
        value = formatWithDelimiters(cents, 2, ' ', '.');
        break;
      case 'amount_no_decimals_with_comma_separator':
        value = formatWithDelimiters(cents, 0, ',', '.');
        break;
      case 'amount_no_decimals_with_space_separator':
        value = formatWithDelimiters(cents, 0, ' ');
        break;
    }

    return formatString.replace(placeholderRegex, value);
  }

  return {
    formatMoney: formatMoney
  };
})();

/**
 * Image Helper Functions
 * -----------------------------------------------------------------------------
 * A collection of functions that help with basic image operations.
 *
 */

slate.Image = (function() {

  /**
   * Preloads an image in memory and uses the browsers cache to store it until needed.
   *
   * @param {Array} images - A list of image urls
   * @param {String} size - A shopify image size attribute
   */

  function preload(images, size) {
    if (typeof images === 'string') {
      images = [images];
    }

    for (var i = 0; i < images.length; i++) {
      var image = images[i];
      this.loadImage(this.getSizedImageUrl(image, size));
    }
  }

  /**
   * Loads and caches an image in the browsers cache.
   * @param {string} path - An image url
   */
  function loadImage(path) {
    new Image().src = path;
  }

  /**
   * Find the Shopify image attribute size
   *
   * @param {string} src
   * @returns {null}
   */
  function imageSize(src) {
    var match = src.match(/.+_((?:pico|icon|thumb|small|compact|medium|large|grande)|\d{1,4}x\d{0,4}|x\d{1,4})[_\.@]/);

    if (match) {
      return match[1];
    } else {
      return null;
    }
  }

  /**
   * Adds a Shopify size attribute to a URL
   *
   * @param src
   * @param size
   * @returns {*}
   */
  function getSizedImageUrl(src, size) {
    if (size === null) {
      return src;
    }

    if (size === 'master') {
      return this.removeProtocol(src);
    }

    var match = src.match(/\.(jpg|jpeg|gif|png|bmp|bitmap|tiff|tif)(\?v=\d+)?$/i);

    if (match) {
      var prefix = src.split(match[0]);
      var suffix = match[0];

      return this.removeProtocol(prefix[0] + '_' + size + suffix);
    } else {
      return null;
    }
  }

  function removeProtocol(path) {
    return path.replace(/http(s)?:/, '');
  }

  return {
    preload: preload,
    loadImage: loadImage,
    imageSize: imageSize,
    getSizedImageUrl: getSizedImageUrl,
    removeProtocol: removeProtocol
  };
})();

/**
 * Variant Selection scripts
 * ------------------------------------------------------------------------------
 *
 * Handles change events from the variant inputs in any `cart/add` forms that may
 * exist. Also updates the master select and triggers updates when the variants
 * price or image changes.
 *
 * @namespace variants
 */

slate.Variants = (function() {
  /**
   * Variant constructor
   *
   * @param {object} options - Settings from `product.js`
   */
  function Variants(options) {
    this.$container = options.$container;
    this.product = options.product;
    this.singleOptionSelector = options.singleOptionSelector;
    this.originalSelectorId = options.originalSelectorId;
    this.enableHistoryState = options.enableHistoryState;

    var selectedValues = this._getCurrentOptions();
    this.currentVariant = this._getVariantFromOptions(selectedValues);

    // If multiple variants could be selected, make sure
    // it's the open that was rendered in the DOM
    if (this.currentVariant.multipleVariantsFound) {
      var defaultVariant = this.product.variants.filter(function(element) {
        return element.id === options.defaultVariantId;
      });
      this.currentVariant = defaultVariant;
      this.currentVariant.multipleVariantsFound = true;
    }

    this.optionLabelUpdateSource = options.optionLabelUpdateSource;
    this.optionLabelUpdateTarget = options.optionLabelUpdateTarget;
    this.lowStockThreshold = $(options.lowStockThreshold, this.$container);
    this.lowStockThresholdValue = parseInt(this.lowStockThreshold.data(options.lowStockThreshold.replace(/\[data-|]/g, "")));
    this.$productRadios = $(options.productRadios, this.$container);
    this.sliderImage = $("#slider-image", this.$container);
    this.sliderThumb = $("#slider-thumbs", this.$container);
    this.$productQuantity = $(options.productQuantity, this.$container);

    this._updateRadioButtons(selectedValues, this.currentVariant);

    // Initialize slick and update slider images
    if(this.sliderImage) {
      this.sliderImage.find("li").zoom({ touch: false });

      this.sliderImage.slick();

      // Remove or add zoom functionality depending on breakpoint state
      this.sliderImage.on('breakpoint', function(event, slick, breakpoint) {
        if(breakpoint === null) {
          // Desktop, enable zoom
          slick.$slidesCache.zoom({ touch: false });
        } else {
          // Mobile, remove zoom
          slick.$slidesCache.trigger('zoom.destroy');
        }
      });

      this.sliderThumb.slick();
      this._updateImages(this._getSelectedColor(selectedValues));
    }

    $(this.singleOptionSelector, this.$container).on(
      "change",
      this._onSelectChange.bind(this)
    );

    $(this.optionLabelUpdateSource, this.$container).on(
      "change",
      { labelTarget: this.optionLabelUpdateTarget },
      function(event) {
        updateSource = $(this);
        updateTarget = updateSource
          .closest(".selector-wrapper")
          .find(event.data.labelTarget);

        updateTarget.html(updateSource.val());
      }
    );
  }

  Variants.prototype = $.extend({}, Variants.prototype, {
    /**
     * Get the currently selected options from add-to-cart form. Works with all
     * form input elements.
     *
     * @return {array} options - Values of currently selected variants
     */
    _getCurrentOptions: function() {
      var currentOptions = $.map(
        $(this.singleOptionSelector, this.$container),
        function(element) {
          var $element = $(element);
          var type = $element.attr("type");
          var currentOption = {};

          if (type === "radio" || type === "checkbox") {
            if ($element[0].checked) {
              currentOption.value = $element.val();
              currentOption.index = $element.data("index");

              return currentOption;
            } else {
              return false;
            }
          } else {
            currentOption.value = $element.val();
            currentOption.index = $element.data("index");

            return currentOption;
          }
        }
      );

      // remove any unchecked input values if using radio buttons or checkboxes
      currentOptions = slate.utils.compact(currentOptions);

      // Sort according to option index
      currentOptions = currentOptions.sort(function(a, b) {
        if (a.index < b.index) {
          return -1;
        }
        if (a.index > b.index) {
          return 1;
        }

        // names must be equal
        return 0;
      });

      return currentOptions;
    },

    /**
     * Find variant based on selected values.
     *
     * @param  {array} selectedValues - Values of variant inputs
     * @return {object || undefined} found - Variant object from product.variants
     */
    _getVariantFromOptions: function(selectedValues) {

      var variants = this.product.variants;
      var found = false;
      var multipleVariantsFound = false;

      variants.forEach(function(variant) {
        var satisfied = true;

        selectedValues.forEach(function(option) {
          if (satisfied) {
            satisfied = option.value === variant[option.index];
          }
        });

        // If the conditions are satisfied but we already found a variant,
        // then multiple variants satisfy the options
        if(found && satisfied) {
          multipleVariantsFound = true;
        }

        if (satisfied) {
          found = variant;
        }
      });

      if(found) {
        found.multipleVariantsFound = multipleVariantsFound;
      }

      return found || null;
    },

    /**
     * Get the selected color from the selected option values
     */
    _getSelectedColor: function(selectedValues) {
      var selectedColors = selectedValues.filter(function(element, index) {
        return element.index === "option1";
      });
      if(selectedColors.length > 0) {
        return selectedColors[0].value;
      }
    },
    /**
     * Event handler for when a variant input changes.
     */
    _onSelectChange: function() {
      var selectedValues = this._getCurrentOptions();
      var variant = this._getVariantFromOptions(selectedValues);
      var selectedColor = this._getSelectedColor(selectedValues);

      this._updateQuantitySelector(variant);

      this.$container.trigger({
        type: "variantChange",
        variant: variant
      });

      if (variant) {
        this._updateMasterSelect(variant);
        this._updatePrice(variant);
      }

      this._updateImages(selectedColor);
      this._updateRadioButtons(selectedValues, variant);

      this._updateLowStockIndicator(variant);
      this.currentVariant = variant;

      if (this.enableHistoryState) {
        this._updateHistoryState(variant);
      }
    },

    /**
     * Trigger event when variant image changes
     *
     * @param  {String} variantColor - Currently selected color
     */
    _updateImages: function(variantColor) {

      if(!variantColor) {
        return;
      }

      // Filter slick images
      var currentFilter = "[data-alt='" + theme.utils.handleize(variantColor) +"'], [data-alt='" + this.product.handle +"']";

      this.sliderImage.slick('slickUnfilter').slick("slickFilter", currentFilter);
      this.sliderThumb.slick('slickUnfilter').slick("slickFilter", currentFilter);

      // On desktop (when thumbs are visible) need to reset slick indices to
      // resolve a known bug with slick
      if(this.sliderImage.slick('getSlick').activeBreakpoint === null) {
        $('.slick-slide:not(.slick-cloned)', this.sliderImage).each( function(i) {
          $(this).attr('data-slick-index', i);
        });

        $('.slick-slide:not(.slick-cloned)', this.sliderThumb).each( function(i) {
          $(this).attr('data-slick-index', i);
        });
      }
    },

    /**
     * Trigger event when variant price changes.
     *
     * @param  {object} variant - Currently selected variant
     * @return {event} variantPriceChange
     */
    _updatePrice: function(variant) {
      if (
        this.currentVariant &&
        variant.price === this.currentVariant.price &&
        variant.compare_at_price === this.currentVariant.compare_at_price
      ) {
        return;
      }

      this.$container.trigger({
        type: "variantPriceChange",
        variant: variant
      });
    },

    /**
     * Update history state for product deeplinking
     *
     * @param  {variant} variant - Currently selected variant
     * @return {k}         [description]
     */
    _updateHistoryState: function(variant) {
      if (!history.replaceState || !variant) {
        return;
      }

      var newurl =
        window.location.protocol +
        "//" +
        window.location.host +
        window.location.pathname +
        "?variant=" +
        variant.id;
      window.history.replaceState({ path: newurl }, "", newurl);
    },

    /**
     * Update radio button states
     *
     */
    _updateRadioButtons: function(selectedValues, variant) {

      this.$productRadios
        .removeClass("active out-stock")
        .find("input")
        .prop("disabled", false);

      var variants = this.product.variants;
      var $productRadios = this.$productRadios;

      this.product.options.forEach(function(value, index) {
        var $optionRadios = $productRadios.filter(
          "[data-option='option" + (index + 1) + "']"
        );

        if (index > 0) {
          var selectedOptions = selectedValues.slice(0, index);

          var relevantVariants = variants.reduce(function(obj, item) {
            for (i = 0; i < selectedOptions.length; i++) {
              if (item.options[i] !== selectedOptions[i].value) {
                return obj;
              }
            }
            obj[item.options[index]] = item;
            return obj;
          }, {});

          $optionRadios
            .filter(function (i, radio) {
              var optionVariant = relevantVariants[$(radio).data('value')];
              return !optionVariant || optionVariant.inventory_quantity <= 0;
            })
            .addClass("out-stock")
            .find("input")
            .prop("disabled", true);
        }

        var selectedValue = selectedValues[index];
        if (selectedValue) {
          $optionRadios.filter('[data-value="' + selectedValue.value + '"]').addClass("active");
        }
      });
    },

    /**
     * Update low stock indicator message
     *
     * @param  {variant} variant - Currently selected variant
     */
    _updateLowStockIndicator: function(variant) {
      if(variant && variant.inventory_quantity <= this.lowStockThresholdValue && variant.inventory_quantity > 0 && !variant.multipleVariantsFound) {
        var $message = this.lowStockThreshold.find('.product__low-stock-message');
        var message = $message.text().replace(/[0-9]/g, variant.inventory_quantity);
        $message.text(message);
        this.lowStockThreshold.show();
      } else {
        this.lowStockThreshold.hide();
      }
    },

    /**
     * Update quantity selectors. Set max quantity and round down current quantity
     * if it is greater than the inventory of the newly selected variant
     *
     * @param {variant} variant = Currently selected variant
     */
    _updateQuantitySelector: function(variant) {

      if(!variant)
        return;

      var currentVal = parseInt(this.$productQuantity.val());
      var originalId = parseInt($(this.originalSelectorId, this.$container).first().find('[data-original-variant="' + variant.id + '"]').val());
      var cartInventory = window.Cart.itemQuantities[originalId];

      if($.isNumeric(variant.inventory_quantity) && variant.inventory_management == 'shopify') {
        var maxQty = 1;

        if($.isNumeric(cartInventory)) {
          maxQty = Math.max(1, variant.inventory_quantity - cartInventory);

          variant.allInCart = variant.inventory_quantity - cartInventory < 1;
        } else {
          maxQty = Math.max(1, variant.inventory_quantity);
          variant.allInCart = false;
        }

        this.$productQuantity.attr("max", maxQty);

        if(!isNaN(currentVal)) {
          this.$productQuantity.val(Math.min(currentVal, maxQty));
        }
      } else {
        this.$productQuantity.removeAttr("max");
        variant.allInCart = false;
      }
    },

    /**
     * Update hidden master select of variant change
     *
     * @param  {variant} variant - Currently selected variant
     */
    _updateMasterSelect: function(variant) {
      var $option = $(this.originalSelectorId, this.$container).first().find('[data-original-variant="' + variant.id + '"]');

      if ($option.length) {
        $option.prop('selected', true).trigger('change').trigger('variantSet');
      }
    }
  });

  return Variants;
})();


/*================ Sections ================*/
/**
 * Product Template Script
 * ------------------------------------------------------------------------------
 * A file that contains scripts highly couple code to the Product template.
 *
   * @namespace product
 */

theme.Product = (function() {
  var selectors = {
    addToCart: "[data-add-to-cart]",
    addToCartText: "[data-add-to-cart-text]",
    addToCartImage: "[data-add-to-cart-img]",
    comparePrice: "[data-compare-price]",
    comparePriceText: "[data-compare-text]",
    originalSelectorId: "[data-product-select]",
    priceWrapper: "[data-price-wrapper]",
    productFeaturedImage: "[data-product-featured-image]",
    productJson: "[data-product-json]",
    productPrice: "[data-variant-price]",
    productThumbs: "[data-product-single-thumbnail]",
    singleOptionSelector: "[data-single-option-selector]",
    optionLabelUpdateSource: "[data-single-option-update-label]",
    optionLabelUpdateTarget: "[data-single-option-label]",
    productLowStock: "[data-stock-threshold]",
    productRadios: ".product__radio",
    productDescription: "#product__description",
    productQuantity: "#Quantity",
    productQuantityPlus: "[data-product-quantity-plus]",
    productQuantityMinus: "[data-product-quantity-minus]",
    productForm: "[data-product-form]",
    defaultVariant: "[data-default-variant]"
  };

  /**
   * Product section constructor. Runs on page load as well as Theme Editor
   * `section:load` events.
   * @param {string} container - selector for the section container DOM element
   */
  function Product(container) {
    this.$container = $(container);

    // Stop parsing if we don't have the product json script tag when loading
    // section in the Theme Editor
    if (!$(selectors.productJson, this.$container).html()) {
      return;
    }

    var sectionId = this.$container.attr("data-section-id");
    this.productSingleObject = JSON.parse(
      $(selectors.productJson, this.$container).html()
    );

    if (this.productSingleObject.variants.length == 0) {
      $('#slider-image').slick();
      $('#slider-thumbs').slick();
      return false;
    }

    var options = {
      $container: this.$container,
      enableHistoryState: this.$container.data("enable-history-state") || false,
      singleOptionSelector: selectors.singleOptionSelector,
      originalSelectorId: selectors.originalSelectorId,
      optionLabelUpdateSource: selectors.optionLabelUpdateSource,
      optionLabelUpdateTarget: selectors.optionLabelUpdateTarget,
      lowStockThreshold: selectors.productLowStock,
      product: this.productSingleObject,
      productRadios: selectors.productRadios,
      productQuantity: selectors.productQuantity,
      defaultVariantId: parseInt($(selectors.defaultVariant).data('default-variant'))
    };

    this.settings = {};
    this.namespace = ".product";
    this.$featuredImage = $(selectors.productFeaturedImage, this.$container);

    this.variants = new slate.Variants(options);

    this.$container.on(
      "variantChange" + this.namespace,
      this.updateAddToCartState.bind(this)
    );
    this.$container.on(
      "variantPriceChange" + this.namespace,
      this.updateProductPrices.bind(this)
    );

    if (this.$featuredImage.length > 0) {
      this.settings.imageSize = slate.Image.imageSize(
        this.$featured_image.attr("src")
      );
      slate.Image.preload(
        this.productSingleObject.images,
        this.settings.imageSize
      );

      this.$container.on(
        "variantImageChange" + this.namespace,
        this.updateProductImage.bind(this)
      );
    }

    var descriptionContainer = $(selectors.productDescription, this.$container);

    if(descriptionContainer.length > 0) {
      descriptionContainer.easyResponsiveTabs({
        tabidentify: 'resp-element',
        closed: true,
        inactive_bg: '#fff',
        active_border_color: '#000'
      });
    }

    var $productQuantity = $(selectors.productQuantity, this.$container);

    $(selectors.productQuantityPlus, this.$container).on("click", { $quantity: $productQuantity }, function(event) {
      var $quantity = event.data.$quantity;
      var max = parseInt($quantity.attr('max'));
      var currentVal = parseInt($quantity.val());

      if(isNaN(currentVal)) {
        $quantity.val(1);
      } else {
        if(!isNaN(max)) {
          $quantity.val(Math.min(currentVal + 1, max));
        } else {
          $quantity.val(currentVal + 1);
        }
      }
    });

    $(selectors.productQuantityMinus, this.$container).on("click", { $quantity: $productQuantity }, function(event) {
      var $quantity = event.data.$quantity;
      var min = parseInt($quantity.attr('min'));
      var currentVal = parseInt($quantity.val());

      if(isNaN(currentVal)) {
        console.log("val is nan");
        if(!isNaN(min)) {
          $quantity.val(min);
        } else {
          $quantity.val(1);
        }
      } else {
        if(!isNaN(min)) {
          $quantity.val(Math.max(currentVal - 1, min));
        } else {
          $quantity.val(currentVal - 1);
        }
      }
    });

    $(document).on('cart.qty_update', {product: this }, function(event) {
      var variants = event.data.product.variants;
      variants._updateQuantitySelector(variants.currentVariant);
      event.data.product.updateAddToCartState({variant: variants.currentVariant});
    });

    var $productForm = $(selectors.productForm, this.$container);
    $productForm.submit(function(e) {
      e.preventDefault();

      var $form = $(this);

      var atc = $(selectors.addToCartText, this.$container);
      atc.html(theme.strings.adding);

      window.Cart.addItem($(this).serialize())
        .done(function() {
          window.Cart.openCart();

          atc.html(theme.strings.added);
        })
        .fail(function() {
          atc.html(theme.strings.addingFail);
          console.log("failed to add");
        })
        .always(function() {
          setTimeout(function() {
            // Trigger a quantity update to update atc button text
            $(document).trigger('cart.qty_update');
          }, 4000);
        });
    })
  }

  Product.prototype = $.extend({}, Product.prototype, {
    /**
     * Updates the DOM state of the add to cart button
     *
     * @param {boolean} enabled - Decides whether cart is enabled or disabled
     * @param {string} text - Updates the text notification content of the cart
     */
    updateAddToCartState: function(evt) {
      var variant = evt.variant;
      $(selectors.addToCartImage, this.$container).attr('src',theme.strings.bagIcon).hide();

      if (!variant) {
        $(selectors.addToCart, this.$container).prop("disabled", true);
        $(selectors.addToCartText, this.$container).html(
          theme.strings.unavailable
        );
        return;
      }

      if (variant.multipleVariantsFound) {
        return;
      } else if (variant.allInCart) {
        $(selectors.addToCart, this.$container).prop("disabled", true);
        $(selectors.addToCartText, this.$container).html(
          theme.strings.incart
        );
      } else if (variant.available) {
        $(selectors.addToCart, this.$container).prop("disabled", false);
        $(selectors.addToCartText, this.$container).html(
          theme.strings.addToCart);
        $(selectors.addToCartImage, this.$container).attr('src',theme.strings.bagIcon).show();
      } else {
        $(selectors.addToCart, this.$container).prop("disabled", true);
        $(selectors.addToCartText, this.$container).html(theme.strings.soldOut);
        $(selectors.addToCartImage, this.$container).attr('src',theme.strings.bagIcon).hide();
      }
    },

    /**
     * Updates the DOM with specified prices
     *
     * @param {string} productPrice - The current price of the product
     * @param {string} comparePrice - The original price of the product
     */
    updateProductPrices: function(evt) {
      var variant = evt.variant;
      var $comparePrice = $(selectors.comparePrice, this.$container);
      var $compareEls = $comparePrice.add(
        selectors.comparePriceText,
        this.$container
      );
      var $productPrice = $(selectors.productPrice, this.$container);

      $productPrice.html(
        slate.Currency.formatMoney(variant.price, theme.moneyFormat)
      );

      if (variant.compare_at_price > variant.price) {
        $comparePrice.html(
          slate.Currency.formatMoney(
            variant.compare_at_price,
            theme.moneyFormat
          )
        );
        $compareEls.removeClass("hide");
        $productPrice.addClass("product__discount");
      } else {
        $comparePrice.html("");
        $compareEls.addClass("hide");
        $productPrice.removeClass("product__discount");
      }
    },

    /**
     * Updates the DOM with the specified image URL
     *
     * @param {string} src - Image src URL
     */
    updateProductImage: function(evt) {
      var variant = evt.variant;
      var sizedImgUrl = slate.Image.getSizedImageUrl(
        variant.featured_image.src,
        this.settings.imageSize
      );

      this.$featured_image.attr("src", sizedImgUrl);
    },

    /**
     * Event callback for Theme Editor `section:unload` event
     */
    onUnload: function() {
      this.$container.off(this.namespace);
    }
  });

  return Product;
})();

theme.featuredCollection = (function() {

	$('.featured-collection-slick').slick({
		autoplay: false,
	    dots: true,
	    swipe: false,
	    touchMove:false,
	    slidesToShow: 5,
	    slidesToScroll: 5,
	    lazyLoad: 'ondemand',
	    focusOnSelect: false,
	    prevArrow: '<i class="fa fa-caret-left previous-arrow" aria-hidden="true"></i>',
	    nextArrow : '<i class="fa fa-caret-right next-arrow" aria-hidden="true"></i>',
	    responsive: [
	    	{
				breakpoint:  1024,
				settings: {
					draggable: false,
					swipeToSlide: false,
					swipe: false,
					slidesToShow: 2,
					slidesToScroll: 2,
					arrows: true,
					autoplay: false,
					infinite: true
				}
			},
			{
				breakpoint: 768,
				settings: {
					draggable: true,
					swipeToSlide: true,
					swipe: true,
					slidesToShow: 2,
					slidesToScroll: 2,
					arrows: false,
					autoplay: false,
					infinite: true
				}
			}
		]
	}).fadeIn(700);

	$('.curated-collection-slick').slick({
		autoplay: false,
	    dots: false,
	    swipe: false,
	    touchMove:false,
	    slidesToShow: 5,
	    lazyLoad: 'ondemand',
	    focusOnSelect: false,
	    arrows: false,
	    responsive: [
	    	{
				breakpoint: 990,
				settings: {
					draggable: false,
					swipeToSlide: false,
					swipe: false,
					slidesToShow: 3,
					arrows: false,
					autoplay: false,
					infinite: true,
					dots: true
				}
			},
			{
				breakpoint: 768,
				settings: {
					draggable: false,
					swipeToSlide: false,
					swipe: false,
					slidesToShow: 2,
					arrows: false,
					autoplay: false,
					infinite: true,
					dots: true
				}
			}
		]
	}).fadeIn(700);

	$('.super-collection__container').slick({
		autoplay: false,
	    dots: true,
	    swipe: true,
	    touchMove:false,
	    slidesToShow: 1,
	    lazyLoad: 'ondemand',
	    focusOnSelect: false,
	    arrows: false
	}).fadeIn(700);

	$('.sc-slick-block').slick({
		centerMode: true,
		autoplay: false,
	    dots: true,
	    swipe: true,
	    touchMove:true,
	    slidesToShow: 1,
	    lazyLoad: 'ondemand',
	    focusOnSelect: true,
	    centerPadding: '12.5%'
	}).fadeIn(700);

})();


// Refresh the sliders on section load while customizing the theme
$(document).on('shopify:section:load', function () {
	theme.featuredCollection();
});
slate.Collection = (function () {
	var old_hovered_img = '';
	var $tile_main_image = '';
	var $tile_altenative_image = '';
	var new_main_img = '';
	var new_alternative_img = '';
	var clicked = false;

	var $staticParents = $('.collection.wrapper--section-content.grid, .nosto_element, .search__content-output, .grid__element--collection.one-whole, .featured-collection-container');

	// ------------------------------
	// PRODUCT TILE
	// ------------------------------

	// Update tile image on click of a color swatch
	$staticParents.on('click', '.featured-collection__info .product__option-selector .radio-color', function () {
		clicked = true;
		var $this = $(this);
		var $tile_container = $this.closest('.featured-collection__info');

		$tile_main_image = $tile_container.find('.featured_collection__image');
		$tile_altenative_image = $tile_container.find('.alternate_collection__image');
		new_main_img = $this.data('variantImage');
		new_alternative_img = $this.data('variantAltImage');

		$tile_altenative_image.attr('src', new_alternative_img);

		if (!new_main_img) {
			return false;
		}

		$tile_main_image.attr('src', new_main_img);

	});

	// Update tile image on mouse enter of color swatch
	$staticParents.on('mouseenter', '.featured-collection__info .product__option-selector .radio-color', function () {
		var $this = $(this);
		var $tile_container = $this.closest('.featured-collection__info');

		$tile_altenative_image = $tile_container.find('.alternate_collection__image');
		new_alternative_img = $this.data('variantAltImage');
		old_hovered_img = $tile_altenative_image.attr('src');

		if (!new_alternative_img) {
			return false;
		}

		$tile_altenative_image.attr('src', new_alternative_img);
	});

	// Update tile image on mouse leave of color swatch
	$staticParents.on('mouseleave', '.featured-collection__info .product__option-selector .radio-color', function () {
		if (clicked) {
			clicked = false;
			return;
		}
		
		if (!old_hovered_img) {
			return false;
		}

		$tile_altenative_image.attr('src', old_hovered_img);
	});


	// ------------------------------
	// FILTERS
	// ------------------------------

	// Filter panels
	function openPanel($section) {
		closePanel($('.filter-section').not($section));
		$section.addClass('active');
		$section.find('.filter-expander').addClass('open');
		$section.find('.filter-options').slideDown(300);
	}
	function closePanel($section) {
		$section.removeClass('active');
		$section.find('.filter-expander').removeClass('open');
		$section.find('.filter-options').slideUp(300);
	}

	// Filter expand
	$('.js-filter-expand').click(function () {
		var $section = $(this).closest('.filter-section');

		if ($section.hasClass('active')) {
			closePanel($section);
		} else {
			openPanel($section);
		}
	});

	// Filters click-away
	$(document).click(function(event) {
		if (!$(event.target).closest('.filter-section').length) {
			closePanel($('.filter-section'));
		}
	});

	// Filters mobile collapse
	$('.js-filters-toggle').click(function() {
		$(this)
			.closest('.filters-container').toggleClass('active')
			.find('.filters').slideToggle();
	});

	$('#collection-list').on('change', '.swatch-radio', function () {
		var $optionKey = $(this).data('index');
		var $optionVal = $(this).val();
		$(this).closest('form').find($('[data-option-index=' + $optionKey + ']')).val($optionVal);
	});

	if (window.collection) {
		var page_number = window.collection.page || 0;
		var view = (getUrlVars()["view_mode"]);
		// Stops the loading of more pages until the last one is done
		var lock = false;
		var $view_all = $('.js-view-all');

		function scrollExecute() {
			if (page_number < window.collection.last_page && !lock) {
				if ($('#collection-list').height() + $('#collection-list').offset().top - 2000 < $(document).scrollTop() + $(window).height()) {
					lock = true;
					page_number += 1;
					scrollURL = window.collection.url + '?view=view-all&page=' + page_number;
					$('.ajax-spinner').show();
					$.ajax({
						type: 'GET',
						url: scrollURL,
						success: function (data) {
							var $new_page = $(data);
							$('.ajax-spinner').hide();
							$('#collection-list').append($new_page);
							$new_page.find('.featured-collection__info').each(function() {
								var themeProduct = new theme.Product($(this));
							})
							window.promoMessaging.update();
							lock = false;
						},
						error: function (error) {
							$('.ajax-spinner').hide();
						},
						dataType: "html"
					});
				}
			}
		}
		$view_all.click(function () {
			if ($(this).hasClass('active')) {
				window.location.replace(window.collection.url);
			}
			else {
				window.location.replace(window.collection.url + "?view_mode=all");
			}
		});

		if (view == 'all') {
			$view_all.toggleClass('active');
			$(window).scroll($.throttle(200, scrollExecute));
		}
		else {
			$('.pagination-container, .pagination-bottom').show();
			$view_all.removeClass('active');
		}

		$('.scroll-up').click(function () {
			$("html, body").animate({
				scrollTop: 0
			}, 1200, 'linear');
			return false;
		});
	}
})();
theme.Header = (function() {
	$(function () {
	  $('.show-center').click(function () {
	    $('.meganav').removeClass('meganav-show-left meganav-show-right').addClass('meganav-show-center');
	  });
	  $('.show-left').click(function () {
	    $('.meganav').removeClass('meganav-show-center meganav-show-right').toggleClass('meganav-show-left');
	  });
	  $('.show-right').click(function () {
	    $('.meganav').removeClass('meganav-show-left meganav-show-center').toggleClass('meganav-show-right');
	  });
	  $('.show-none, .show-center, .show-left, .show-right').click(function (e) {
	    e.stopPropagation();
	  });
	  $(document).on('click', function (e) {
	    var $chain = $(e.target).parents().addBack();
	    if (!$chain.is('.meganav-left, .meganav-right, .meganav-center')) {
	      $('.meganav').removeClass('meganav-show-left meganav-show-center meganav-show-right');
	       $('#nav-toggle').removeClass('active');
	    }
	  })
	});

	$(function() {
	    $(".mm-sublist").mouseover(function(){
	      var listName = $(this).data("menuitem");
	      $(".mm-sublist").removeClass('sub-menu-active')

	      //check if it is part of the shop menu
	        $(".mm-sublist.shop").addClass('sub-menu-inactive');

	      if($(this).hasClass('shop')){
	        $(this).removeClass('sub-menu-inactive');
	      }

	      $(this).addClass('sub-menu-active');
	      $(".inner-menu-list").removeClass("active-menu");
	      $(".inner-menu-container").find("ul ." + listName).toggleClass("active-menu");
	    });
	});

	$(function(){
	    //Header Height
	    var headerHeight = $("#navigation-header").outerHeight();
	    $(window).resize(function(){
	      headerHeight = $("#navigation-header").outerHeight();
	    })

	    $(window).scroll(function(){
	      var windowSize = $(window).width();
	      if(windowSize > 1024){
	        //Elements
	        var $header = $("#navigation-header");
	        var $pageContainer = $header.siblings(".container");
	        var $supportList = $header.find("#support-list-container");
	        var $cartList = $("#cart-item-container").find(".cart-li");
	        var $searchContainer = $header.find("#search-container");
	        var $logoContainer = $header.find("#logo-container");
	        //Window Variables
	        var windowTop = $(window).scrollTop();

	        //if windowTop > headerHeight make header fixed and shrink to smaller header
	        if(windowTop > headerHeight){
	          $pageContainer.css({ 'margin-top': headerHeight });
	          //Transition downwards if header isn't fixed
	          if(!$header.hasClass('scroll-fixed')){
	            $header.addClass('scroll-transition').delay(100).queue(function(){
	              $header.addClass('scroll-fixed').dequeue();
	              $header.removeClass('scroll-transition').dequeue();
	            });
	          }

	          //if > 1200
	          if (windowSize > 1200){
	            $supportList.addClass("scroll-support");
	            $cartList.addClass("scroll-cart");
	            $searchContainer.addClass("scroll-search");
	            $logoContainer.addClass('scroll-logo');
	          }
	        }else{
	          //if header was fixed
	          if($header.hasClass('scroll-fixed')){
	            $header.removeClass('scroll-transition');
	            $header.removeClass('scroll-fixed');
	            $pageContainer.css({ 'margin-top': '0' });

	            //1200px
	            $supportList.removeClass("scroll-support");
	            $cartList.removeClass("scroll-cart");
	            $searchContainer.removeClass("scroll-search");
	            $logoContainer.removeClass('scroll-logo');
	          }
	        }
	      }
	    });
    });

	$(function() {
		$('#nav-toggle, .container.meganav-page-content').on("click", function() {
			if($('#nav-toggle').hasClass('active')) {
				$('#nav-toggle').removeClass('active');
			} else {
				$('#nav-toggle').addClass('active');
			}
		});

		$('.mobile-nav__parent .mobile-nav__link').on("click", function() {
			var $accordion = $(this).parent().find('.mobile-nav__dropdown');
			$accordion.toggleClass('active__accordion').slideToggle();

			if($accordion.hasClass('active__accordion')) {
				$(this).find('.plus .fa').first().addClass('active');
			} else {
				$(this).find('.plus .fa').first().removeClass('active');
			}
		});

		$('.mobile-nav__child .mobile-nav__link-child').on("click", function() {
			var $accordion = $(this).parent().find('.panel').first();
			$accordion.toggleClass('active__accordion').slideToggle();

			if($accordion.hasClass('active__accordion')) {
				$(this).find('.plus .fa').first().addClass('active');
			} else {
				$(this).find('.plus .fa').first().removeClass('active');
			}
		});

		$('.mobile-nav__grandchild').on("click", function() {
			var $accordion = $(this).find('.panel').first();

			$accordion.toggleClass('active__accordion').slideToggle();

			if($accordion.hasClass('active__accordion')) {
				$(this).find('.plus .fa').first().addClass('active');
			} else {
				$(this).find('.plus .fa').first().removeClass('active');
			}
		});
	});

})();
theme.footer = (function() {
	$(function(){
		$('.site-footer__menu .menu__col').click(function(e){
			$(this).find('.col-menu__list-menu').toggleClass('active__accordion');
			if($(this).find('.col-menu__list-menu').hasClass('active__accordion')) {
				$(this).find('.col-menu__header .fa-plus').css({"transition":"0.4s", "transform":"rotate(135deg)"});
			} else {
				$(this).find('.col-menu__header .fa-plus').css({"transition":"0.4s", "transform":"rotate(270deg)"});
			}
		});
	});

	$(function(){
		
		// Hides the loading icon
		$('.product__find-store').find('.js-load-icon').hide();

		$('.store-search-form, .product__find-store').submit(function(e){
			$(this).find('.icon-search').hide();
			$(this).find('.js-load-icon').show();
			var address = '?address=' + $.trim($(this).find('.store-search-box,.product__find-store__input').val());
			var language = $('html').attr('lang');
			var find_store_page = '/pages/find-a-store';

			if (language == 'fr') {
				find_store_page = '/pages/trouver-une-boutique';
			}

			if (address.length) {
				e.preventDefault();
				window.location.href = find_store_page + address;
			}
			else {
				e.preventDefault();
				window.location.href = find_store_page;
			}

		});
	});
})();
$(function () {

	const cm_unit = 2.54;
	const inch_unit = 0.393701;
	var $general_table = $('.js-sc-tables.general-table');
	var $curve_appeal_table = $('.js-sc-tables.curve-appeal-table');
	var $tables = $('.js-sc-tables');
	var $category_inputs = $('.js-sc-category-inputs input');
	var $table_sections = $('.js-sc-table-section');
	var $products_types = $('.js-product-type');
	var $modal_close = $('.js-modal-close');
	var $modal_link = $('.js-modal-link');
	var $unit_form_inputs = $('.js-sc-unit-form input');

	$modal_link.click(function (e) {
		e.preventDefault();
		var $this = $(this);
		var target = $this.data('modal-target');
		var types = $products_types.data('product-types').split('-');
		var category = "";

		// Creates a list of all the existing categories
		var categories = $.map( $category_inputs, function( n ) {
			return $(n).val();
		});

		category = types.find(function(product_type) {
				return categories.indexOf(product_type) > -1; // Returns the matching category from the list
		});

		$('#sc-category-' + category).prop('checked', true).change(); // Triggers the category tabs
		$('.js-modal' + target).addClass('show');
	});

	$modal_close.click(function (e) {
		e.preventDefault();
		var $this = $(this);
		var target = $this.closest('.js-modal').removeClass('show');
	});

	$unit_form_inputs.change(function () {
		var unit_name = $(this).val();
		var title = $('.unit-converter__header-unit').data('sc-' + unit_name + '-title');

		$('.unit-converter__header-unit').text(title);
		$('.js-sc-unit').each(function () {
			sizeUnitConverter($(this), unit_name);
		});
	});

	$category_inputs.change(function () {
		var category = $(this).val();
		categorySwitcher(category);
	});

	// Switches when a category is selected
	// Filters the needed tables for that category
	function categorySwitcher(category) {
		$table_sections.each(function () {
			var $table = $(this);
			var $table_body = $table.find('tbody');
			var table_filters = $table.data('sc-section-filters').split(',');
			var $row_filters = $table.data('hide-row').split(',');

			// Resets the filter functionality
			$table.hide();
			$table_body.find('tr').show();

			table_filters.forEach(function (table_filter) {
				if (category == table_filter) {
					$table.show();
					
					$row_filters.forEach(function (row_filter) {
						var filter_array = row_filter.split(':');
						var category_name  = filter_array[0];
						var row_name = filter_array[1];

						// Hide data of a table depending on the category
						if (category == category_name) {
							$table_body.find('tr[data-row-name="' + row_name  + '"]').hide();
						}
					});
				}
			});
		});
	}

	// Converts the tables units' from inch to cm
	// and vice-versa
	function sizeUnitConverter($element, unit_name) {
		var table_data = $element.text().split('-');
		var new_data = '';

		table_data.forEach(function (data,i) {

			// Will not run if the data is not a number
			if ($.isNumeric(data)) {
				var unit = parseFloat(data); // table data
				var cm_to_inch = parseFloat(unit * inch_unit).toFixed(1).toString().split('.'); // Makes an array of integers
				var inch_to_cm = parseFloat(unit * cm_unit).toFixed(1).toString().split('.'); // Makes an array of integers
				var decimal_point = '.';
				var separator = '-'
				var new_unit = '';

				if (unit_name == 'inch') {

					// Remmoves trailing zeros from the number
					if (cm_to_inch[1] == 0) {
						cm_to_inch[1] = '';
						decimal_point = ''
					}

					new_unit = cm_to_inch[0] + decimal_point + cm_to_inch[1]
				} else if (unit_name == 'cm') {

					// Remmoves trailing zeros from the number
					if (inch_to_cm[1] == 0) {
						inch_to_cm[1] = '';
						decimal_point = '';
					}

					new_unit = inch_to_cm[0] + decimal_point + inch_to_cm[1];
				}

				if ( i >= table_data.length -1 ){
					separator = '';
				}
				new_data += new_unit + separator;
				$element.text(new_data);
			}
		});
	}

});
$(function() {
	/**
	 * The store locator object takes care of filtering,
	 * rendering the map, handling filters, geo location,
	 * geo coding, rendering store results, and pagination
	 */
	var Store_locator = new function() {
		function Store_locator() {
			var store_locator = this;

			this.key = "AIzaSyD9zUfv8hp6EUC9YezuOa6yd0OXMZ8b-ts";

			this.$container = $(".js-store-locator");
			this.$resultsCountContainer = $(".js-results-count-container");
			this.$resultsCount = $(".js-results-count", this.$resultsCountContainer);
			this.$storeListContainer = $(".js-store-list", this.$container);
			this.$radiusCopy = $(".js-radius-dropdown-copy", this.$container);
			this.$filterRadius = $(".js-filter-radius", this.$container);
			this.$geolocateButton = $(".js-geolocate", this.$container);
			this.$addressLookup = $(".js-search-input", this.$container);
			this.$addressSubmit = $(".js-search-submit", this.$container);
			this.$mapToggleButton = $(".js-map-toggle", this.$container);
			this.$mapContainer = $("#map_container", this.$container);
			this.$locationNearby = $('.js-location-near', this.$container);

			this.dropdowns = new Dropdowns(this.$container);
			this.pagination = new Pagination($('.js-pagination-container', this.$container), this);

			this.filtersManager = new FilterManager($('.js-filters-container', this.$container), $('.js-selected-filters', this.$container), this);

			this.previousWidth = window.outerWidth;
			this.fitBoundsPadding = 40;
			this.countPerPage = 6;
			this.markers = [];
			this.labels = [];
			this.storepicker = this.$container.hasClass("js-store__picker");
			this.defaultNearbyLocation = this.$locationNearby.data('default');

			this.$geolocateButton.on('click', function() {
				store_locator.geolocate();
			});

			this.$addressSubmit.submit(function() {
				store_locator.geocode();
				return false;
			});

			this.$mapToggleButton.on('click', function() {
				store_locator.$mapToggleButton.toggleClass('active');
				store_locator.toggleMap();
			});

			this.$filterRadius.on('change', function() {
				store_locator.$radiusCopy.text($(this).data('title'));
				store_locator.loadStores();
			});

			$(window).resize(function() {
				if(window.outerWidth >= 1024 && store_locator.previousWidth < 1024 && !store_locator.storepicker) {
					store_locator.$mapToggleButton.addClass('active');
					store_locator.toggleMap();
				} else if (window.outerWidth < 1024 && store_locator.previousWidth >= 1024 || store_locator.storepicker) {
					store_locator.$mapToggleButton.removeClass('active');
					store_locator.toggleMap();
				} else if(store_locator.storeBounds) {
					window.theme.map.fitBounds(store_locator.storeBounds, store_locator.fitBoundsPadding);
				}

				store_locator.previousWidth = window.outerWidth;
			});

			if (store_locator.storepicker) {
				var checkoutDestination = store_locator.$container.data('checkout-destination');

				store_locator.$storeListContainer.on('click', '.js-pick-store', function() {
					var id = this.getAttribute('data-store-id');
					var this_store = store_locator.stores[id];
					checkoutDestination += this_store.checkout_auto_fill;
					var store_brand = this_store.brand;
					var open_hours = this_store.open_hours;
					var store_address = this_store.address;
					var store_tel = this_store.phone;
					var store_full_name = store_address.name + " " + this_store.brand + " #" + this_store.store_code;
					var store_code = this_store.store_code;

					$.post('/cart/update.js', {
						attributes: {
							checkout_type: "store",
							open_hours: JSON.stringify(open_hours),
							store_address: JSON.stringify(store_address),
							store_tel: store_tel,
							store_brand: store_brand,
							store_name: store_address.name,
							store_code: store_code,
							store_full_name: store_full_name,
							store_formatted_hours: store_locator.processHours(open_hours).join('')
						}
					}, function() {
						window.location.href = checkoutDestination;
					}, "json");
				});
			}
			if(window.outerWidth >= 1024 && !store_locator.storepicker) {
				store_locator.$mapToggleButton.addClass('active');
			}

			// Geolocate and load stores
			this.geolocate();
		}

		/**
		 * The Dropdowns object keeps track of all the dropdowns and ensuring that
		 * atmost only one is open at a time.
		 * @param {Object} $container - Container holding dropdown elements
		 */
		function Dropdowns($container) {
			var dropdowns = this;
			dropdowns.$container = $container;
			dropdowns.$active = undefined;

			dropdowns.$dropdownsElements = $(".js-dropdown-toggle", this.$container);

			dropdowns.Toggle = function Toggle($trigger) {
				if(dropdowns.$active === $trigger) {
					dropdowns.$active = undefined;
					$trigger.deactivate();
				} else if (dropdowns.$active) {
					dropdowns.$active.deactivate(function() {
						$trigger.activate();
						dropdowns.$active = $trigger;
					});
				} else {
					$trigger.activate();
					dropdowns.$active = $trigger;
				}
			};

			this.dropdownObjects = $.map(dropdowns.$dropdownsElements, function(element) {
				return new Dropdown(element, dropdowns.$container, dropdowns.Toggle);
			});
		}

		/**
		 * The Dropdown object represents a single dropdown that can slide down and up
		 *
		 * @param {object} button - The dropdown's toggle button
		 * @param {object} $container - Container of dropdown elements
		 * @param {function} toggle - Funtion to call when the dropdown request a toggle
		 */
		function Dropdown(button, $container, toggle) {
			var dropdown = this;
			dropdown.$button = $(button);
			dropdown.toggle = toggle;
			dropdown.$dropdown = $(dropdown.$button.data('for'), $container);

			dropdown.$button.on('click', function() {
				dropdown.toggle(dropdown);
			});

			dropdown.deactivate = function(completed) {
				dropdown.$dropdown.slideUp(completed);
				dropdown.$button.removeClass('active');
			};

			dropdown.activate = function() {
				dropdown.$dropdown.slideDown();
				dropdown.$button.addClass('active');
			};
		}

		/**
		 * The Pagination object creates pagination dropdown elements,
		 * handles pagination GUI item click events, and keeps track of page
		 * changes notifying the store locator of which page to load
		 *
		 * @param {object} $container - Pagination elements container
		 * @param {Store_locator} store_locator - The main store locator
		 */
		function Pagination($container, store_locator) {
			var pagination = this;
			this.store_locator = store_locator;
			this.pages = 1;
			this.page = 1;

			this.$dropdown = $('.js-pagination-dropdown', $container);
			this.$previous = $('.js-pagination-prev', $container);
			this.$next = $('.js-pagination-next', $container);
			this.$lastpage = $('.js-pagination-last', $container);

			this.$next.on('click', function() {
				if(pagination.page < pagination.pages) {
					pagination.updatePagination(pagination.page + 1);
				}
			});

			this.$previous.on('click', function() {
				if(pagination.page > 1) {
					pagination.updatePagination(pagination.page - 1);
				}
			});

			this.$dropdown.on('change', function() {
				pagination.updatePagination(parseInt(pagination.$dropdown.val()));
			});

			// Create the pagination dropdown elements
			this.setupPagination = function(count) {
				pagination.pages = Math.ceil(count / store_locator.countPerPage);

				pagination.$lastpage.html(pagination.pages);

				var dropdownOptions = [];
				for(var i = 1; i <= pagination.pages; i++) {
					var index = i.toString();
					dropdownOptions.push('<option value="');
					dropdownOptions.push(index);
					dropdownOptions.push('">');
					dropdownOptions.push(index);
					dropdownOptions.push('</option>')
				}
				pagination.$dropdown.html(dropdownOptions.join(''));
			}

			// Update page information and load the requested page
			this.updatePagination = function(page) {
				pagination.$previous.css('visibility', page === 1 ? 'hidden' : 'visible');
				pagination.$next.css('visibility', page === pagination.pages ? 'hidden' : 'visible');
				pagination.$dropdown.val(page);
				pagination.page = page;
				pagination.store_locator.renderPage(page);
			}
		}

		/**
		 * The Filter Manager adds and removes filters, keeps track of active filters
		 * and enables the clearing of all filters;
		 *
		 * @param {Object} $filtersContainer - Container holding the filters
		 * @param {Object} $selectedFiltersContainer - Container holding the bar were selected filters are displayed
		 */
		function FilterManager($filtersContainer, $selectedFiltersContainer, store_locator) {
			var manager = this;

			manager.$filterTags = $(".js-filter-tag", $filtersContainer);
			manager.$selectedFilterLists = $(".js-filters-list", $selectedFiltersContainer);
			manager.$clearAll = $('.js-clear-all-filters', $selectedFiltersContainer)
			manager.selectedFilters = new Set();

			// Create the filters
			manager.filters = $.map(manager.$filterTags, function(element) {
				return new Filter($(element), manager);
			});

			// Add a filer
			manager.attach = function(filter) {
				manager.$selectedFilterLists.append(filter.$element);
				manager.selectedFilters.add(filter);
				manager.update();
			};

			// Remove a filter
			manager.detach = function(filter) {
				filter.deactive();
				manager.selectedFilters.delete(filter);
				manager.update();
			};

			manager.$clearAll.on('click', function(e) {
				e.preventDefault();

				manager.selectedFilters.forEach(function(filter) {
					filter.deactive();
				});
				manager.selectedFilters.clear();
				manager.update();
			});

			// Show or hide the filters bar
			manager.update = function() {
				if(manager.selectedFilters.size > 0) {
					$selectedFiltersContainer.show();
				} else {
					$selectedFiltersContainer.hide();
				}

				store_locator.loadStores();
			}
		}

		/**
		 * A Filter object keeps track of a filter's checkbox and
		 * badge that appears in the selected filters bar
		 *
		 * @param {object} $filter - The filter's input element
		 * @param {FilterManager} manager - The Filter manage
		 */
		function Filter($filter, manager) {
			var filter = this;
			filter.$input = $filter;
			filter.tagGroup = $filter.data('tag');
			filter.filterValue = $filter.val();
			filter.open_now = filter.tagGroup === "open_now";
			filter.open_till_9 = filter.tagGroup === "open_till_9";
			filter.open_sundays = filter.tagGroup === "open_sundays";

			// Construct the filter's badge with a button to remove the filter
			filter.$element = $(
				'<div class="store__selected-filter">' +
					filter.filterValue +
				'</div>'
			);
			filter.$remove = $('<button class="store__selected-filter-close"></button>');
			filter.$remove.appendTo(filter.$element);

			// Notify the manager of request to add or remove the filer
			filter.$input.on('change', function() {
				if(filter.$input[0].checked) {
					manager.attach(filter);
				} else {
					manager.detach(filter);
				}
			});
			filter.$remove.on('click', function() {
				manager.detach(filter);
			});

			// When removing the filter ensure to uncheck the input
			filter.deactive = function() {
				filter.$input.prop('checked', false);
				filter.$element.detach();
			};

			// Modify filter settings based on the state of this filter
			filter.addSetting = function(settings) {
				if(filter.$input[0].checked) {
					var tag = filter.tagGroup;
					if(filter.open_now) {
						settings["open_now"] = true;
					} else if (filter.open_till_9 || filter.open_sundays) {
						var openingDate;

						// Parse the existing time or create a new one
						if(settings.open_at) {
							openingDate = new Date(Date.parse(settings.open_at));
						} else {
							openingDate = new Date();
							openingDate.setHours(12);
							openingDate.setMinutes(0);
							openingDate.setSeconds(0);
						}

						// Set the time to 8:59 for open till 9pm
						// or add the number of days to make the date Sunday for open sunday
						if(filter.open_till_9) {
							openingDate.setHours(20);
							openingDate.setMinutes(59);
							openingDate.setSeconds(0);
						} else {
							var daysToAdd = 7 - openingDate.getDay();
							openingDate.setDate(openingDate.getDate() + daysToAdd);
						}

						settings.open_at = openingDate.toUTCString();
					} else if(tag) {
						// Check if we need to initialize the array for this tag
						if(!settings[tag]) {
							settings[tag] = [];
						}
						settings[tag].push(filter.filterValue);
					}
				}

				return settings;
			}
		}

		Store_locator.prototype.refreshMap = function() {
			var store_locator = this;

			// Trigger the resize event so that google maps will recalculate bounds
			google.maps.event.trigger(window.theme.map,'resize');
			if(store_locator.storeBounds) {
				window.theme.map.fitBounds(store_locator.storeBounds, store_locator.fitBoundsPadding);
			}
		};

		// Toggle the visibility of the map
		Store_locator.prototype.toggleMap = function() {
			var store_locator = this;

			if(this.$mapToggleButton.hasClass('active')) {
				store_locator.$container.removeClass("map_stowed");
				this.$mapContainer.slideDown(function() {
					store_locator.refreshMap();
				});
			} else {
				this.$mapContainer.slideUp(function() {
					store_locator.$container.addClass("map_stowed");
				});
			}
		};

		Store_locator.prototype.getRadius = function() {
			return this.$filterRadius.filter(':checked')[0];
		};

		// Get a list of selected filters
		Store_locator.prototype.gatherFilters = function() {
			return this.filtersManager.filters.reduce(function(accumulator, currentValue) {
				return currentValue.addSetting(accumulator);
			}, {});
		};

		// Request a geo location from the browser's geolocation feature
		Store_locator.prototype.geolocate = function() {
			var _ = this;
			this.$locationNearby.text(this.defaultNearbyLocation);

			if (theme.cart_json.attributes.location) {
				try {
					var customer_location = JSON.parse(theme.cart_json.attributes.location);

					this.geolocateResponse(customer_location);
				} catch(e) {
				}
			} else {

				// Maxmind's API call
				geoip2.city(
					function (geolocation) {
						$.post('/cart/update.js', { 'attributes[location]': JSON.stringify(geolocation) }, 
						function(cart) {}, 'json')
						.done(function(cart) {
							theme.cart_json = cart;
							_.geolocateResponse(geolocation);
						});
					},
					function (error) {
						/*
							Fixes the Firefox update issue
							https://blog.mozilla.org/firefox/how-to-block-fingerprinting-with-firefox/
						*/

						if (error.code == 'HTTP_ERROR') {
							_.loadStores();
						}
						console.log(error);
					}
				);
			}
		};

		// Set the position based on the browser's geolocation response
		Store_locator.prototype.geolocateResponse = function(position) {
			this.position = {
				latitude: position.location.latitude || null,
				longitude: position.location.longitude || null 
			}

			this.loadStores();
		};

		// Set the position based on Google's geocoding response
		Store_locator.prototype.geocodeResponse = function(response) {

			if(response.status !== "OK") {
				this.$resultsCount.text(0);
				this.$resultsCountContainer.show();
				this.pagination.setupPagination(0);
				this.pagination.updatePagination(0);
				return;
			}

			this.position = {
				latitude: response.results[0].geometry.location.lat,
				longitude: response.results[0].geometry.location.lng
			}

			this.loadStores();
		};

		// Use Google geocoding to convert the user's search
		// query into coordinates
		Store_locator.prototype.geocode = function() {
			var store_locator = this;

			var params = {
				address: store_locator.$addressLookup.val(),
				key: store_locator.key,
				region: "ca"
			}

			store_locator.$locationNearby.text(params.address);

			var settings = {
				async: true,
				url: "https://maps.googleapis.com/maps/api/geocode/json?" + $.param(params),
				method: "GET",
				dataType: "json"
			}

			return $.ajax(settings).done(function (response) {
				store_locator.geocodeResponse(response);
			});
		}

		Store_locator.prototype.filterStores = function(stores) {
			return stores.filter(function(store) {
				return store.support_in_store_pickup;
			});
		}

		// Load a list of stores from the SI endpoint
		Store_locator.prototype.loadStores = function() {
			var store_locator = this;
			var params = {};
			var address = theme.utils.getURLParams()['address'] || '';

			if (address.length) {
				store_locator.$addressLookup.val(address);
				theme.utils.clearURLParams();
				store_locator.$addressSubmit.submit();
				store_locator.$geolocateButton.click();
			} else if (store_locator.position) {
				params.location = {};

				params.location.longitude = store_locator.position.longitude;
				params.location.latitude = store_locator.position.latitude;

				var radius = store_locator.getRadius();

				if (radius) {

					params.location.radius = radius.value;
					params.location.units = 'km';
				}
			}

			$.extend(params, store_locator.gatherFilters());

			var settings = {
				'async': true,
				'url': '/apps/api/v1/stores?' + $.param(params),
				'method': 'GET',
				'cache': false
			};

			return $.ajax(settings)
				.done(function(response) {
					if (store_locator.storepicker) {
						store_locator.stores = store_locator.filterStores(response.stores);
					} else {
						store_locator.stores = response.stores;
					}

					$(document).trigger('stores_loaded.StoreLocator', [store_locator.stores]);

					store_locator.$resultsCount.html(store_locator.stores.length);
					store_locator.$resultsCountContainer.show();
					store_locator.pagination.setupPagination(store_locator.stores.length);
					store_locator.pagination.updatePagination(1);
				})
				.fail(function() {
					// Return no stores on fail
					$(document).trigger('stores_loaded.StoreLocator', []);
				});
		};

		// Render a page of stores
		Store_locator.prototype.renderPage = function(page) {
			var store_locator = this;

			// Remove all markers and labels
			store_locator.removeMapObjects(store_locator.markers);
			store_locator.removeMapObjects(store_locator.labels);

			if(!store_locator.stores) {
				this.$storeListContainer.empty();
				return;
			}

			store_locator.markers = [];
			store_locator.labels = [];

			var storesObject = [];
			var storeBounds = new google.maps.LatLngBounds();
			var pageindex = (page - 1) * store_locator.countPerPage;

			store_locator.stores.slice(pageindex, pageindex + store_locator.countPerPage).forEach(function(store, index) {

				var storeId = pageindex + index;

				// Add a marker at the store's position on the map
				var marker = new google.maps.Marker({
					position: {lat: store.address.latitude, lng: store.address.longitude},
					title: store.address.name.trim(),
					map: window.theme.map,
					icon: {
						url: window.theme.store_view_oval,
						size: new google.maps.Size(17, 20),
						origin: new google.maps.Point(0, 0),
						anchor: new google.maps.Point(8, 20)
					},
					label: {
						text: (index + 1).toString(),
						color: "white",
						fontSize: "10px",
						fontWeight: "600",
						fontFamily: "Montserrat"
					}
				});

				store_locator.markers.push(marker);

				// Add a label for the store's name on the map
				var label = new MapLabel({
					align: "left",
					position: new google.maps.LatLng({lat: store.address.latitude, lng: store.address.longitude}),
					map: window.theme.map,
					text: store.address.name.trim(),
					fontColor: "black",
					fontSize: "10px",
					fontFamily: "Montserrat",
					strokeWeight: 0
				});

				store_locator.labels.push(label);

				// Add the store coordinates to the bounds
				storeBounds.extend({lat: store.address.latitude, lng: store.address.longitude});

				storesObject.push('<div class="store__list"><div class="store__list-inner">');

				store.checkout_auto_fill = "&locale=" + window.theme.locale + '&step=contact_information';

				storesObject.push('<div class="store__list-upper">');
					storesObject.push('<div class="store__list-address-cell">');
						storesObject.push('<span class="store__list-oval" data-index="');
						storesObject.push(index + 1);
						storesObject.push('"><img src="');
						storesObject.push(window.theme.store_view_oval);
						storesObject.push('"/></span>');

						storesObject.push('<h3 class="store__list-title">');
						storesObject.push(store.brand);
						storesObject.push('</h3>');
						storesObject.push('<h3 class="store__list-title store__list-title--name">');
						storesObject.push(store.address.name.trim());
						storesObject.push('</h3>');

						storesObject.push('<div class="store__list-address">');
							storesObject.push('<div>');
								storesObject.push(store.address.line1);
								storesObject.push('<br />');
								storesObject.push(store.address.line2);
							storesObject.push('</div>');
							storesObject.push('<div>');
								storesObject.push(store.address.city);
								storesObject.push(', ');
								storesObject.push(store.address.state_code);
								storesObject.push(' ');
								storesObject.push(store.address.zip);
							storesObject.push('</div>');
						storesObject.push('</div>');
					storesObject.push('</div>');

					if(store_locator.storepicker) {
						storesObject.push('<div class="store__list-pick-container">');
						storesObject = storesObject.concat(store_locator.generatePickButton(storeId));
						storesObject.push('</div>');
					}

				storesObject.push('</div>');

				storesObject.push('<div class="store__list-lower">');
					storesObject.push('<div class="store__list-hours-container">');
						storesObject.push('<h4 class="store__list-hours-title">');
						storesObject.push(window.theme.strings.store_locator.store_hours);
						storesObject.push('</h4>');
						storesObject.push('<div class="store__list-hours">');
							storesObject = storesObject.concat(store_locator.processHours(store.open_hours));
						storesObject.push('</div>');
					storesObject.push('</div>');

					storesObject.push('<div class="store__list-links">');
						storesObject.push('<div class="store__list-phone">');
							if(store.phone) {
								storesObject.push('<a href="tel:+1');
								storesObject.push(store.phone.replace(/\D/g,''));
								storesObject.push('">');
									storesObject.push(store.phone);
								storesObject.push('</a>');
							}
						storesObject.push('</div>');

						storesObject.push('<div class="store__list-directions">');
							storesObject.push('<a href="https://www.google.com/maps/dir/?api=1&')
							storesObject.push($.param({
								destination: [store.address.line1, store.address.line2, store.address.city, store.address.zip].join(' ')
							}));
							storesObject.push('" target="_blank">');
							storesObject.push(window.theme.strings.store_locator.directions);
							storesObject.push('</a>');
						storesObject.push('</div>');
					storesObject.push('</div>');
				storesObject.push('</div>');

				storesObject.push('</div><div class="store__list-pick-container--mobile">');
				storesObject = storesObject.concat(store_locator.generatePickButton(storeId));
				storesObject.push('</div></div>');
			});

			if(store_locator.stores.length > 0) {
				// Set the map to show the stores
				store_locator.storeBounds = storeBounds;

				if(store_locator.stores.length > 1) {
					window.theme.map.fitBounds(storeBounds, store_locator.fitBoundsPadding);
				} else {
					window.theme.map.panTo(storeBounds.getCenter());
					window.theme.map.setZoom(17);
				}
			}

			// Populate the store list container with the stores
			store_locator.$storeListContainer.html(storesObject.join(''));
		}

		/**
		 * Converts a list of opening hours to html elements that are grouped by days with the same
		 * opening hours
		 *
		 * @param {object[]} hours - A list of opening and closing hours for each day of the week
		 * @returns {object[]} A list of strings representing the constructed DOM elements
		 */
		Store_locator.prototype.processHours = function(hours) {
			var store_locator = this;
			var hourGrouping = [];
			var hourOutput = [];
			var opening_hours;

			// The endpoint from SI starts on Sunday, move that day
			// to the end so that we start on Monday
			if (hours[0].day === 'Sunday') {
				opening_hours = hours.slice(1);
				opening_hours.push(hours[0]);
			} else {
				opening_hours = hours;
			}

			// Iterate through each day of the week
			opening_hours.forEach(function(day, index) {
				if(index === 0) {
					hourGrouping.push({ start: day });
					return;
				}

				// Get the previously inserted day
				var lastDay = hourGrouping[hourGrouping.length - 1];

				// If the previous day has the same opening hours, set it ending today,
				// else create a new time range starting today
				if(lastDay.start.open_time === day.open_time && lastDay.start.close_time === day.close_time) {
					lastDay.end = day;
				} else {
					hourGrouping.push({ start: day });
				}
			});

			// Construct the DOM elements for the time ranges
			hourGrouping.forEach(function(day) {
				hourOutput.push('<div>');
				hourOutput.push(window.theme.dayMap[window.theme.locale][day.start.day]);

				if(day.end) {
					hourOutput.push(' - ');
					hourOutput.push(window.theme.dayMap[window.theme.locale][day.end.day]);
				}

				hourOutput.push(': ');
				hourOutput = hourOutput.concat(store_locator.processAMPM(day.start.open_time));
				hourOutput.push(' - ');
				hourOutput = hourOutput.concat(store_locator.processAMPM(day.start.close_time));
				hourOutput.push('</div>');
			});

			return hourOutput;
		}

		/**
		 * Convert 24 hour time to DOM elements in am/pm format for
		 * english
		 *
		 * @param {number} time - 24 hour time
		 * @returns {string[]} list of constructed DOM strings
		 */
		Store_locator.prototype.processAMPM = function(time) {
			var ampmOutput = [];

			if (!time) {
				return;
			}

			var times = time.split(':');

			if(window.theme.locale === "en") {
				var hour = parseInt(times[0]);

				if(hour > 12) {
					ampmOutput.push(hour - 12);
				} else if (hour === 0) {
					ampmOutput.push('12');
				} else {
					ampmOutput.push(hour);
				}

				if(parseInt(times[1]) > 0) {
					ampmOutput.push(':');
					ampmOutput.push(times[1]);
				}

				ampmOutput.push('<sub>');
				if(hour < 12 || hour === 24) {
					ampmOutput.push('am');
				} else {
					ampmOutput.push('pm');
				}
				ampmOutput.push('</sub>');
			} else {
				ampmOutput.push(parseInt(times[0]));
				ampmOutput.push('h');

				if(parseInt(times[1]) > 0) {
					ampmOutput.push(times[1]);
				}
			}

			return ampmOutput;
		}

		/**
		 * Removes the Map from a list of Google Map objects
		 *
		 * @param {object} objects - A list of Google Map objects
		 */
		Store_locator.prototype.removeMapObjects = function(objects) {
			objects.forEach(function(element) {
				element.setMap(null);
			})
		}

		Store_locator.prototype.generatePickButton = function(store_id) {
			var buttonOutput = [];
			buttonOutput.push('<button class="store__list-pick-button js-pick-store" data-store-id="');
			buttonOutput.push(store_id);
			buttonOutput.push('">');
			buttonOutput.push(window.theme.strings.store_locator.pick_up);
			buttonOutput.push('</button>');

			return buttonOutput;
		}

		return Store_locator;
	};

	if (window.store_locator_loader) {
		window.store_locator_loader.done(function() {
			window.loadMapLabels();
			window.store_locator = new Store_locator();
		});
	}
})
!function($) {
	// Wrap any iFrame in the description to be responsive
	$('#toggle-product__description').find('iframe').filter(function(i, element) {
		return !$(element).parent().hasClass('video-container');
	}).wrap('<div class="video-container"></div>');
}(jQuery);

(function($) {
	$(function() {
		var $videos = $('.js-video');

		// Play/pause on click (desktop only)
		$videos.each(function() {
			var $video = $(this);
			var video = $video.get(0);

			if (!video.paused) {
				$video.parent().addClass('paused');
			} else if (video.paused && $video.attr('autoplay')) {
				var playPromise = video.play();

				if (playPromise !== undefined) {
					playPromise
						.catch(function(error) {
							$video.parent().addClass('paused');
						});
				}
			}

			$video.on('click', function() {
				if (video.paused) {
					video.play();
					$video.parent().removeClass('paused');
				} else {
					video.pause();
					$video.parent().addClass('paused');
				}
			});
		});
	});
})(jQuery);
// Newsletter email input trigger
$(function() {

	var $newsletter_wrapper = $('.js-newsletter');
	var $newsletter_buttons = $('.js-newsletter-button');
	var $newsletter_input = $newsletter_wrapper.find('.js-newsletter-email');
	var url = $newsletter_wrapper.data('newsletter-url');

	
	if (window.KlaviyoSubscribe) {
		KlaviyoSubscribe.attachToModalForm('#k_id_modal', {
			delay_seconds: 1
		});
	}

	// Form will be submitted when the enter is pressed
	$newsletter_input.keyup(function(e) {
		e.preventDefault();
		// Code for enter key
		if (e.keyCode === 13) {
			var newsletter_wrapper = $(this).closest('.js-newsletter');
			$newsletter_buttons.trigger('newsletter-submit', newsletter_wrapper);
		}
	});

	$newsletter_buttons.click(function(e){
		var newsletter_wrapper = $(this).closest('.js-newsletter');
		$(document).trigger('newsletter-submit', newsletter_wrapper);
	});

	$(document).on('newsletter-submit', function(e, newsletter_target){
		e.preventDefault();

		var $newsletter_form = $(newsletter_target).find('.js-newsletter-form');
		var $email = $newsletter_form.find('.js-newsletter-email');
		var $page_source = $newsletter_form.find('.js-newsletter-source');

		var data = {
			attributes: {
				"klaviyo_email": $email.val() || '',
				"klaviyo_source": $page_source.val() || ''
			}
		}

		$.post('/cart/update.json', data)
			.done(function() {
				KlaviyoSubscribe.setClosedModalCookie();
				window.location.href = url;
			});
	});

});

// Email form validation and form submission
$(function() {
	if (window.KlaviyoSubscribe) {
		KlaviyoSubscribe.attachToForms('#klaviyo-sign-up-form');

		var $klaviyo_form = $('.js-sign-up-form');
		var $legal_checbox = $('.js-email-sign-up-legal');
		var $submit_form_button = $('.js-email-sign-up-submit');

		$legal_checbox.on('change',function(e) {
			$submit_form_button
				.toggleClass('form-submit-button--disabled')
				.prop('disabled', !$(this).prop('checked'));
		});

		$klaviyo_form.on("submit", function(e) {
			e.preventDefault();

			var $first_name = $('[name="fname"]');
			var $last_name = $('[name="lname"]');
			var $email = $('[name="email"]');
			var $birthday = $('[name="birthday"]');
			var $zip_code = $('[name="zipcode"]');
			var $errors = $('.js-form-errors');
			var $success_link =  $('.js-success-link');

			$errors.empty();
			$errors.hide();

			if(!$email.val().trim()) {
				erroDisplay($errors, $email);
			}

			if(!$first_name.val().trim()) {
				erroDisplay($errors, $first_name);
			}

			if(!$last_name.val().trim()) {
				erroDisplay($errors, $last_name);
			}

			if(!$zip_code.val().trim() || !isValidPostalCode($zip_code.val().trim())) {
				erroDisplay($errors, $zip_code);
			}

			if($birthday.val().trim() && !isValidDate($birthday.val().trim())) {
				erroDisplay($errors, $birthday);
			}
			
			$errors.show();
			var error_count = $errors.find('.form-error');	

			if (error_count.length > 0 ) {
				return;
			}

			var settings = {
				'async': true,
				'crossDomain': true,
				'url': 'https://manage.kmail-lists.com/ajax/subscriptions/subscribe',
				'method': 'POST',
				'headers': {
					'content-type': 'application/x-www-form-urlencoded',
					'cache-control': 'no-cache'
				},
				'data': $(this).serialize()
			};

			$.ajax(settings)
				.done(function (resp) {
					if (resp.success) {
						$errors.hide();
						$klaviyo_form.trigger("reset");
						window.location.replace($success_link.data('success-link'));
					} else {
						$errors.show();
					}
			});

			function isValidDate(date_string) {
				var date_regex = /^(?:(?:31(-)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(-)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)\d{2})$|^(?:29(-)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(-)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)\d{2})$/;
				return date_string.match(date_regex) != null;
			}

			function isValidPostalCode(postal_code_string) {
				postal_code_string = postal_code_string.toUpperCase();
				var postal_code_regex = /^[ABCEGHJ-NPRSTVXY]\d[ABCEGHJ-NPRSTV-Z][ -]?\d[ABCEGHJ-NPRSTV-Z]\d$/;
				var zip_code_regex = /^[0-9]{5}(?:-[0-9]{4})?$/;

				return postal_code_string.match(postal_code_regex) != null || postal_code_string.match(zip_code_regex) != null;
			}


			function erroDisplay($error_container, $error_field) {
				var $list_item = $('<li>', {class:'form-error'});
				$error_container.append($list_item.text($error_field.data('formError')));
			}
		});
	}
});

// Calendar input selector
$(function() {

	var lang = '';
	var $calendar_wrapper = $('.js-sign-up-calendar');
	var $calendar_button = $calendar_wrapper.find('.js-sign-up-calendar-button');
	var calendar_format =  $calendar_wrapper.data('date-format')
	var $calendar = $calendar_wrapper.find('[data-toggle="datepicker"]');
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1;
	var yyyy = today.getFullYear();

	if (window.theme.locale == 'en') {
		lang = 'en-US'
	} else if (window.theme.locale == 'fr') {
		lang = 'fr-FR'
	}

	$calendar.datepicker({
		language: lang,
		format: calendar_format,
		autoHide: true,
		trigger: $calendar_button,
		date: new Date(1980, 1, 1),
		endDate: new Date(yyyy,mm,dd)
	});
});
$(document).ready(function() {
	// Update language selection as cart attribute
	$('.js-language-option').click(function() {
		var url_redirect = $(this).data('lang-url');

		$.ajax({
			type: 'POST',
			url: '/cart/update.js',
			data: { 'attributes[language]': $(this).data('value') },
			dataType: 'json'
		}).done(function() {
			if (url_redirect != undefined) {
				window.location.href = url_redirect;
			} else {
				window.location.reload();
			}
		});
	});
});


/*================ Templates ================*/
! function(promoMessaging, $) {
	var prod_messaging = {};
	var campaigns = window.CAMPAIGN_MESSAGING || [];

	var getMessages = function() {
		return $('[data-promo-message]');
	}
	// Promo messaging object
	var promoMessage = function(config) {
		var promoMessage = this;
		var config = config || {};

		var setPromo = function() {
			for (var i = 0; i < campaigns.length; i++) {
				var campaign = campaigns[i];

				if (
					campaign.hasOwnProperty('include') &&
					campaign.include.hasOwnProperty('collection_includes') &&
					campaign.hasOwnProperty('display_messaging') &&
					!campaign.include.hasOwnProperty('discount_code') &&
					campaign.display_messaging
				) {
					var collections = campaign.include.collection_includes || [];
					var exclusions = (campaign.exclude && campaign.exclude.collection_includes) ? campaign.exclude.collection_includes : [];
					var included = false;
					var excluded = false;

					// Get inclusions
					for (var j = 0; j < collections.length; j++) {
						var collection = collections[j];
						if (promoMessage.collections.indexOf(collection) >= 0) {
							included = true;
							break;
						}
					}

					// Get exclusions
					for (var j = 0; j < exclusions.length; j++) {
						var exclusion = exclusions[j];
						if (promoMessage.collections.indexOf(exclusion) >= 0) {
							excluded = true;
							break;
						}
					}

					if (included && !excluded) {
						var promo_object = {
							regular_priced: campaign.regular_priced_only,
							messaging: campaign.message,
							type: campaign.type,
							colors: campaign.colors,
							calculate_price: campaign.calculate_price
						}

						if (campaign.discount) {
							var amount = campaign.discount.match(/\(([\d|\.]+)\)/)[1];
							promo_object.discount = {
								amount: parseFloat(amount),
								type: campaign.discount.split('Discount')[0].toLowerCase()
							}
						}

						return promo_object;
					}
				}
			}
			return false;
		}

		promoMessage.collections = config.collections || [];
		promoMessage.id = config.id;
		promoMessage.sale = config.sale || false;
		promoMessage.promo = setPromo();
	}


	// Update promo message based on element
	promoMessaging.showMessages = function() {
		$.each(prod_messaging, function(id, promoMessage) {
			if (promoMessage.promo && !(promoMessage.sale && promoMessage.promo.regular_priced)) {
				var $message = $('.messaging-' + promoMessage.id);
				$message.html('<span class="promo-message" ' + (promoMessage.promo.colors ? ('style="background-color: ' + promoMessage.promo.colors.background + ';color: ' + promoMessage.promo.colors.text + '"') : '') + '>' + promoMessage.promo.messaging + '</span>');

				if (promoMessage.promo.calculate_price) {
					var $price = $message.closest('.js-product').find('.js-product-price');
					var price = $price.data('product-price');

					// Convert the price if it's a string
					if (!!price && typeof(price) !== 'number') {
						price = price.match(/\d+/g).join('');
					}

					try {
						var old_price = parseFloat(price / 100);
						var new_price = old_price;

						switch(promoMessage.promo.discount.type) {
							case 'percentage':
								new_price = old_price - (old_price * (promoMessage.promo.discount.amount / 100));
								break;
							case 'money':
								new_price = old_price - promoMessage.promo.discount.amount;
								break;
							case 'price':
								new_price = promoMessage.promo.discount.amount;
								break;
						}

						if (!$price.find('.discounted').length && old_price > new_price) {
							$price.html('<span class="sale-wrapper">' + $price.html() + '<span class="discounted">' + theme.utils.toMoney(new_price) + '</span></span>');
						}
					} catch (e) {}
				}
			}
		});
	}

	promoMessaging.addMessage = function($element, config) {
		if (!prod_messaging[config.id]) {
			prod_messaging[config.id] = new promoMessage(config);
		}
		// Add class for selection
		$element.addClass('messaging-' + config.id);
	}

	// Update the list of products
	promoMessaging.update = function() {
		var deferred = [];

		getMessages().each(function() {
			var $this = $(this);
			var config = $this.data('promo-message');
			config.id = config.product || Math.floor(Math.random() * 1000000);

			if (config && config.collections !== undefined) {
				promoMessaging.addMessage($this, config);
			} else if (config.handle || config.url) {
				var handle = !!config.handle ? config.handle :
					config.url.split('products/')[1].split('?')[0];

				// Fetch the product data
				if (handle && !prod_messaging[config.product]) {
					deferred.push(
						promoMessaging.getPromo(handle)
							.then(function(resp) {
								// Test to make sure the proper data is sent back
								var regEx = /^\n<!doctype html>/;

								if (regEx.test(resp)) {
									return false;
								}

								try {
									var object = JSON.parse(resp);
									promoMessaging.addMessage($this, object);
								} catch(e) {
									return false
								}
							}, function() {
								return $.when();
							})
				);
			} else {
					promoMessaging.addMessage($this, config);
				}
			}
		});

		// Show messages on elements
		$.when.apply($, deferred).then(function() {
			promoMessaging.showMessages();
			});
	}
	promoMessaging.getProductMessages = function() {
		return prod_messaging;
		}

	promoMessaging.getPromo = function(handle) {
		return $.get('/products/' + handle + '?view=collections');
	}

	$(function() {
		promoMessaging.update();
	});
}(window.promoMessaging = window.promoMessaging || {}, jQuery);
(function(theme, algolia) {
	//
	var Algolia = function(algolia) {
		this.client = algolia('ZEBX1N6H3H', 'c69f81e70a4ff1c73a93b832922bde3c');
		this.index = this.client.initIndex('suzy_products');
	}

	/**
	 * Converts Algolia hits to product objects to render.
	 *
	 * @param {object[]} hits - Array of hits returned by Algolia.
	 * @returns Array of formatted product objects.
	 */
	Algolia.prototype.hitsToProducts = function(hits) {
		if (!hits) {
			return [];
		}

		var product_map = {};
		var products = [];

		for (var h in hits) {
			var product = hits[h];

			// Group products (variants) by their product id
			if(!product_map[product.id]) {
				product_map[product.id] = {};
				product_map[product.id].products = [product];
			} else {
				product_map[product.id].products.push(product);
			}
		}

		for (product_id in product_map) {
			// Remap product options and images to match tile renderer format

			product_map[product_id].options = $.map(product_map[product_id].products[0].option_names, function(element, index) {
				return { "name": element, "values": new Set(), "position": index + 1 };
			});

			$.each(product_map[product_id].products, function(var_index, variant) {
				$.each(Object.keys(variant.options), function(opt_index, optionKey) {
					product_map[product_id].options[opt_index].values.add(variant.options[optionKey]);
				});

				variant.options = Object.keys(variant.options).map(function(e) {
					return variant.options[e];
				});

				variant.featured_image = variant.image;
				variant.price *= 100;
				variant.compare_at_price *= 100;
				variant.product_id = variant.id;
				variant.id = variant.objectID;
			});

			$.each(product_map[product_id].options, function(index, option) {
				option.old_values = option.values;
				// Covert the Set to an Array with foreach since Array.from is not supported in IE11
				option.values = [];
				option.old_values.forEach(function(element) {
					option.values.push(element);
				});
			});

			var product = theme.utils.clone(product_map[product_id].products[0]);
			product.variants = product_map[product_id].products;
			product.old_options = product.options;
			product.options = product_map[product_id].options;
			product.id = parseInt(product.product_id);

			products.push(product);
		}

		return products;
	}

	/**
	 * Converts Shopify's sorting order to Algolia's.
	 *
	 * @param {string} sort - The Shopify sorting order to convert.
	 * @returns The Algolia-readable sort order. Defaults to best selling.
	 */
	Algolia.prototype.convertSort = function(sort) {
		switch(sort) {
			case 'alpha-ascending':
				return 'title_asc'
			case 'alpha-descending':
				return 'title_desc'
			case 'price-ascending':
				return 'price_asc'
			case 'price-descending':
				return 'price_desc'
			case 'created-ascending':
				return 'published_at_asc'
			case 'created-descending':
				return 'published_at_desc'
			default:
				return 'recently_ordered_count_desc';
		}
	}

	// Initialize the filters if on a collection page.
	theme.algolia = new Algolia(algolia);

})(window.theme = window.theme || {}, algoliasearch);
/**
 * Customer Addresses Script
 * ------------------------------------------------------------------------------
 * A file that contains scripts highly couple code to the Customer Addresses
 * template.
 *
 * @namespace customerAddresses
 */

theme.customerAddresses = (function() {
  var $newAddressForm = $('#AddressNewForm');

  if (!$newAddressForm.length) {
    return;
  }

  // Initialize observers on address selectors, defined in shopify_common.js
  if (Shopify) {
    new Shopify.CountryProvinceSelector('AddressCountryNew', 'AddressProvinceNew', {
      hideElement: 'AddressProvinceContainerNew'
    });
  }

  // Initialize each edit form's country/province selector
  $('.address-country-option').each(function() {
    var formId = $(this).data('form-id');
    var countrySelector = 'AddressCountry_' + formId;
    var provinceSelector = 'AddressProvince_' + formId;
    var containerSelector = 'AddressProvinceContainer_' + formId;

    new Shopify.CountryProvinceSelector(countrySelector, provinceSelector, {
      hideElement: containerSelector
    });
  });

  // Toggle new/edit address forms
  $('.address-new-toggle').on('click', function() {
    $newAddressForm.toggleClass('hide');
  });

  $('.address-edit-toggle').on('click', function() {
    var formId = $(this).data('form-id');
    $('#EditAddress_' + formId).toggleClass('hide');
  });

  $('.address-delete').on('click', function() {
    var $el = $(this);
    var formId = $el.data('form-id');
    var confirmMessage = $el.data('confirm-message');
    if (confirm(confirmMessage || 'Are you sure you wish to delete this address?')) {
      Shopify.postLink('/account/addresses/' + formId, {parameters: {_method: 'delete'}});
    }
  });
})();

/**
 * Customer Account Script
 * ------------------------------------------------------------------------------
 * A file that contains scripts highly couple code to the Customer Account
 * template.
 *
 * @namespace customerAccount
 */

theme.customerAccount = (function() {

  var $newAddressForm = $('#NewAddressForm');

  if (!$newAddressForm.length) {
    return;
  }
  
  // Toggle new/edit address forms.
  $('.address-new-toggle').on('click', function() {
    $newAddressForm.toggleClass('hide');
    $('.new-address-title').toggleClass('hide');
  });

  $('.address-edit-toggle').on('click', function() {
    var formId = $(this).data('form-id');
    $('#AddressEdit_' + formId).toggleClass('hide');
  });

  $('.prestige-card__form .button--edit').on('click', function() {
    // Hide the Edit button, and show the Save button.
    $(this).hide();
    $('.prestige-card__form .button--save').show();
    // Make the prestige number input editable.
    $('input[name=card_number]').prop('readonly', false); 
  });
  
})();

function updatePrestigeNumber(prestigeNumber) {
  var data = {
      attributes: {
          "prestige_number": prestigeNumber
      }
  }
    
  return $.post('/cart/update.json', data);
}
/**
 * Password Template Script
 * ------------------------------------------------------------------------------
 * A file that contains scripts highly couple code to the Password template.
 *
 * @namespace password
 */

theme.customerLogin = (function() {
  var config = {
    recoverPasswordForm: '#RecoverPassword',
    hideRecoverPasswordLink: '#HideRecoverPasswordLink'
  };

  if (!$(config.recoverPasswordForm).length) {
    return;
  }

  checkUrlHash();
  resetPasswordSuccess();

  $(config.recoverPasswordForm).on('click', onShowHidePasswordForm);
  $(config.hideRecoverPasswordLink).on('click', onShowHidePasswordForm);

  function onShowHidePasswordForm(evt) {
    evt.preventDefault();
    toggleRecoverPasswordForm();
  }

  function checkUrlHash() {
    var hash = window.location.hash;

    // Allow deep linking to recover password form
    if (hash === '#recover') {
      toggleRecoverPasswordForm();
    }
  }

  /**
   *  Show/Hide recover password form
   */
  function toggleRecoverPasswordForm() {
    $('#RecoverPasswordForm').toggleClass('hide');
    $('#CustomerLoginForm').toggleClass('hide');
  }

  /**
   *  Show reset password success message
   */
  function resetPasswordSuccess() {
    var $formState = $('.reset-password-success');

    // check if reset password form was successfully submited.
    if (!$formState.length) {
      return;
    }

    // show success message
    $('#ResetSuccess').removeClass('hide');
  }
})();

/**
 * Register Template Script
 * ------------------------------------------------------------------------------
 * A file that contains scripts highly couple code to the Register template.
 *
 */

function validatePassword() {
    if ($('#CreatePassword').val() != $('#ConfirmPassword').val()) {
        $('#ConfirmPassword')[0].setCustomValidity("Passwords must match.");
        console.log("no match");
        return false;
    }
    else {
        $('#ConfirmPassword')[0].setCustomValidity("");
        console.log("match");
    }
    return true;
}

$('form#create_customer').submit(function(e) {
    if (!validatePassword())
        e.preventDefault();
});

$('#ConfirmPassword').keyup(validatePassword);
$(".expandable-tab").click(function () {
    $tab = $(this);
    $content = $tab.next();
    $content.slideToggle(500);
    $tab.toggleClass('toggled');
});
// Form submission handling for the gift card balance checker page
$(function() {
	var $submitBtn = $("#card-balance-form-submit");
	var $reloadBtn = $("#card-balance-form-reload");
	var $cardForm = $('#card-balance-form');
	var $responseContainer = $('#gift-card-message');

	$cardForm.submit(function(e) {
		e.preventDefault();

		var VERIFY_CARD_URL = '/apps/api/v1/check_gift_card_balance';
		$submitBtn.prop('disabled', true);
		$responseContainer.hide().empty().removeClass('text-danger');

		$.post({
		  url: VERIFY_CARD_URL,
		  data: $(this).serialize(),
		  dataType: 'json'
		})
		.done(function(r) {
			$responseContainer.slideDown().html('Gift Card balance / Solde de la carte-cadeau : ' +  slate.Currency.formatMoney(r.amount, theme.moneyFormat));
			$cardForm.slideUp();
			$reloadBtn.slideDown();
		})
		.fail(function(r) {
			var message = "";
			if(r.responseJSON) {
				message = giftCardMessageTranslation (r.responseJSON.message.split("-")[0].replace('gift_card_code', 'card number').replace('pin_number', 'pin number'));
				$responseContainer.slideDown().html(message).addClass('text-danger');
			} else if (r.status === 404) {
				mesage = "Error connecting to server";
				$responseContainer.slideDown().html(giftCardMessageTranslation(message).addClass('text-danger'));
			}
		})
		.always(function() {
			$submitBtn.prop('disabled', false);
		});
	});

	$reloadBtn.click(function(e) {
		$cardForm[0].reset();
		$cardForm.slideDown();
		$reloadBtn.slideUp();
		$responseContainer.slideUp();
	});

	$submitBtn.prop('disabled', false);
});

// Generate the captcha on the gift card page
$(function() {
	if (theme && theme.template && theme.template.suffix == 'gift-card-checker') {
		var $gift_card_submit = $('#card-balance-form-submit');

		var actCallback = function() {
			$gift_card_submit.prop('disabled', false);
		};

		var expCallback = function() {
			$gift_card_submit.prop('disabled', true);
		};

		window.onloadCallback = function () {
			window.grecaptcha.render(document.getElementById('re-captcha'), {
				'sitekey' : '6LdHZ38UAAAAALuzueyGTz2xytJ0TLpTZwppJE_4', // Sitekey, Retrieving from reCaptcha
				'theme': 'light',
				'callback' : actCallback,
				'expired-callback': expCallback,
			});
		}

		window.addEventListener('load', function (event) {
			document.getElementById("card-balance-form-submit").disabled = true;
			function enableBtn(){
				document.getElementById("card-balance-form-submit").disabled = false;
			}
		}, false);
	}
});

function giftCardMessageTranslation (en_message) {
	var fr_message = "";

	if ( $("html").attr("lang") == "fr" ) {
		if ( en_message == "Missing parameters: card number" ){
			return fr_message = "Paramètre manquant: numéro de carte";
		} else if (en_message == "Missing parameters: pin number" ) {
			return fr_message = "Paramètre manquant: numéro de nip";
		} else if ( en_message == "Missing parameters: card number, pin number" ){
			return fr_message = "Paramètres manquants: numéro de carte et numéro de nip";
		} else if ( en_message == "Invalid Card #"){
			return fr_message = "Numéro de Carte Invalide";
		} else if (en_message = "Error connecting to server") {
			return fr_message = "Erreur durant la connection vers le serveur";
		}
		else {
			return en_message;
		}
	} else {
		return en_message;
	}
}
// Form submission handling for the gift card balance checker page
$(function() {
	var $submitBtn = $('#card-activate-form-submit');
	var $reloadBtn = $('#card-balance-form-reload');
	var $cardForm = $('#card-activate-form');
	var $responseContainer = $('#gift-card-activate-message');

	$cardForm.submit(function(e) {

		e.preventDefault();
		$responseContainer.hide().empty().removeClass('text-danger');
		var card_number = +$('#giftCardActivateInput').val(); // Converts input string into a number 

		$submitBtn.prop('disabled', true);
		var ACTIVATE_CARD_URL = '/apps/api/v1/activate_suzy_gift_card';

		$.post({
			url: ACTIVATE_CARD_URL,
			data: $(this).serialize(),
			dataType: 'json'
		})
		.done(function(r) {
			$responseContainer.slideDown().html(r.message);
			$cardForm.slideUp();
			$reloadBtn.slideDown();
			})
		.fail(function(r) {
			var message = "";
			if(r.responseJSON) {
				message = activateCardMessageTranslation(r.responseJSON.message.split("-")[0].replace('gift_card_code', 'card number').replace('pin_number', 'pin number'));
				$responseContainer.slideDown().html(message).addClass('text-danger');
			} else if (r.status === 404) {
				mesage = "Error connecting to server";
				$responseContainer.slideDown().html(activateCardMessageTranslation(message)).addClass('text-danger');
			}
		})
		.always(function() {
			$submitBtn.prop('disabled', false);
		});
});

	$reloadBtn.click(function(e) {
		$cardForm[0].reset();
		$cardForm.slideDown();
		$reloadBtn.slideUp();
		$responseContainer.slideUp();
	});

	$submitBtn.prop('disabled', false);
});

function activateCardMessageTranslation (en_message) {
	var fr_message = "";

	if ( $("html").attr("lang") == "fr" ) {
		if ( en_message == "Missing parameters: card number" ){
			return fr_message = "Paramètre manquant: numéro de carte";
		} else if ( en_message == "Invalid Card #"){
			return fr_message = "Numéro de Carte Invalide";
		} else if (en_message = "Error connecting to server") {
			return fr_message = "Erreur durant la connection vers le serveur";
		}
		else {
			return en_message;
		}
	} else {
		return en_message;
	}
}
!function(tile, $) {

	var badges = $('.badge-list').data('badge-types') || '';
	var badges_list = badges.split(',');
	// Returns HTML of a product tile
	tile.render = function(product, classes) {
		if (!product) return false;

		var productObject = [];
		var colorOption = product.options.filter(function(option) {
			var optionName = option.name.toLowerCase();
			return optionName == 'color' || optionName == 'couleur';
		})[0];
		var lower_tags = product.tags ? product.tags.map(function(tag) {
			return tag.toLowerCase();
		}) : false;
		var cart_ids = document._shopify_cart.cart.line_items.map(function(item) {
			return item.product_id;
		});
		var english_product = product.tags.indexOf('language_en') > -1 ? product.handle :
			(product.meta && product.meta.translate && product.meta.translate['en-CA']) ? product.meta.translate['en-CA'] : false;

		var promo_data = {
			product: product.id,
			sale: product.compare_at_price && product.compare_at_price > product.price,
			handle: product.handle
		}
		var $promo_message = $('<div class="promo-message-container" data-promo-message></div>').data('promo-message', promo_data);

		var badges_tile = badges_list.filter(function(badge) {
			var badge_name = 'badge::' + badge;
			return lower_tags.indexOf(badge_name) > 0
		}).reduce(function (accumulator, badge) {
			return accumulator + '<div class="badge badge--' + theme.utils.handleize(badge) + '"></div>';
		}, '');

		// Define the product URL with its handle if unavailable
		product.url = product.url || '/products/' + product.handle;

		// Default product tile classes
		if (!classes) {
			classes = "grid__item one-fifth medium--one-quarter small--one-half product-list__item";
		}

		productObject.push('' +
			'<li data-product="' + product.id + '" class="' + classes + '">' +
				'<div class="featured-collection__info js-product-tile js-product">' +
					'<div class="product-tile-image" data-cart-badge="' + product.id + '">' +
					'<div class="wishlist"><div class="gift-reggie-container"' +
					'data-wl-variant-id="' + product.variants[0].id + '"></div></div>');

		// TODO: Perhaps we will need to implement an image flip for Algolia results. Added .js-image-flip for the time being.
		productObject.push('' +
						'<a href="' + (window.theme && window.theme.template && window.theme.template == 'collection' ? window.template.location.pathname : '') + product.url + '" alt="' + product.title + '">' +
							'<img class="js-image-flip" src="' + slate.Image.getSizedImageUrl(product.featured_image || window.theme.placeholder, '270x400_crop_center') + '" alt="' + product.title + '">' +
						'</a>');


		if (cart_ids.indexOf(product.id) > -1 && window.theme.badges && window.theme.badges.cart) {
			productObject.push('<img class="product-badge badge-cart" src="' + window.theme.badges.cart + '">');
		}

		productObject.push('' +
					'</div>' +
					(english_product ? ('<div class="featured-collection__quick-shop-anchor">' +
						'<div class="quick-add js-quick-add" data-quick-add="' + english_product + '" data-quick-add-display="' + product.handle + '"></div>' +
					'</div>') : '') +
					'<a class="featured-collection__link" href="' + (window.theme && window.theme.template && window.theme.template == 'collection' ? window.template.location.pathname : '') + product.url + '" alt="' + product.title + '">' +
						'<h3 class="featured-collection__product-title">' + product.title + '</h3>' +
					'</a>' +
					'<div class="featured-collection__badges">' +
						badges_tile +
					'</div>' +
					'<p class="featured-collection__product-price js-product-price" data-product-price="' + product.price + '">' +
						(product.compare_at_price && product.compare_at_price > product.price ?
							(
								'<span class="grid-item-compare">' + slate.Currency.formatMoney(product.compare_at_price) + '</span>' +
								'<span class="grid-item-sale">' + slate.Currency.formatMoney(product.price) + '</span>'
							) :
							(
								(product.price_varies ?
									window.theme.strings.product.from.replace('{{ price }}', slate.Currency.formatMoney(product.price)) :
									('<span class="grid-item-price">' + slate.Currency.formatMoney(product.price) + '</span>')
								) +
								(product.available == false ? '<span class="grid-item-sold-out">' + window.theme.strings.product.sold_out + '</span>' : '')
							)
						) +
					'</p>');

		productObject.push('' +
					'<div class="selector-wrapper js product__option-selector">'
		);

		if (product.meta && product.meta.color_filters && product.meta.color_filters.images) {
			var colors = product.meta.color_filters.images.split(',')
				.map(function(color) {
					var color_split = color.split('%%%');
					return {
						name: (color_split[0] && color_split[0]) == '' ? false : color_split[0],
						image: (color_split[1] && color_split[1]) == '' ? false : color_split[1]
					}
				})
				.filter(function(color) {
					return color.name && color.image;
				});

			$.each(colors, function(index, color) {
				var color_handle = window.theme.utils.handleize(color.name);
				var inputId = product.id + '-' + color_handle + '-' + window.getNextCounter();
				productObject.push('' +
					'<input class="swatch-radio js-quick-add-color" type="radio" id="' + inputId + '" name="' + product.id + '-color" value="' + color.name + '" data-index="option' + colorOption.position + '"' + (color.name == product['option' + colorOption.position] ? ' checked' : '') + '>' +
					'<label title="' + color.name + '" class="product__radio radio-color swatch-' + color_handle + '" for="' + inputId + '" data-variant-image="' + slate.Image.getSizedImageUrl(color.image || window.theme.placeholder, '270x400_crop_center') + '"></label>');
			});
		}

		productObject.push('' +
					'</div>' +
				'</div>' +
			'</li>'
		);

		var $product_tile = $(productObject.join(''));
		$product_tile.find('.featured-collection__badges')
			.after($promo_message);

		// Initialize the quick add functionality
		new theme.QuickAdd($product_tile);

		return $product_tile;
	}
}(window.tile = window.tile || {}, jQuery);
(function(filters, $) {
	/*
	 * ------------------------------
	 * FILTER OPTION
	 * ------------------------------
	 */

	/**
	 * Creates a new filter option object.
	 *
	 * @param {string} value - The filter value.
	 * @param {object} [options] - Options to pass to the filter.
	 * @param {boolean} options.swatch - To display a swatch filter.
	 */
	var FilterOption = function(value, options) {
		if (!value) {
			return false;
		}

		this.elements = {};

		this.options = options || {};

		this.value = value;
		this.name = this.value;

		if (this.options.format == 'price') {
			var price_range = this.name.split(':');
			this.name = theme.utils.toMoney(price_range[0], { no_zeros: true }) + ' - ' + theme.utils.toMoney(price_range[1], { no_zeros: true });
		}

		// 03.28.2018 - Hotfix to rename French sizes coming through on English products
		// TODO: remove once product data is fixed
		this.name = theme.utils.translateOption(this.name);

		// Generate a 5-digit id
		this.id = 'filter-' + (this.options.id || Math.floor(Math.random() * 90000) + 10000);
		this.build();
	}

	FilterOption.prototype.buildSwatch = function() {
		return this.options.swatch ?
			$('<span class="product__radio radio-color swatch-' + theme.utils.handleize(this.value) + '"></span>') :
			'';
	}

	/**
	 * Creates the DOM elements for the filter inputs.
	 *
	 * @memberof FilterOption
	 */
	FilterOption.prototype.build = function() {
		var _ = this;

		_.elements.filter = $('<li class="filter-option"></li>');
		_.elements.input = $('<input type="checkbox" id="' + _.id + '" name="filter-' + theme.utils.handleize(_.value) + '" value="' + _.value + '">')
			.appendTo(_.elements.filter)
			.change(function() {
				// Trigger a global event for filter changes
				$(document).trigger('filter_change', _.value);
			});
		_.elements.label = $('<label for="' + _.id + '">' + _.name + '</label>')
			.prepend(_.buildSwatch())
			.appendTo(_.elements.filter);
	}

	FilterOption.prototype.active = function() {
		return this.elements.input.prop('checked');
	}

	FilterOption.prototype.disable = function() {
		return this.elements.input.prop('checked', false);
	}

	FilterOption.prototype.enable = function() {
		return this.elements.input.prop('checked', true);
	}

	FilterOption.prototype.breadcrumb = function() {
		var _ = this;
		var $breadcrumb = $('<button class="filter-breadcrumb" type="button">' + _.name + '</button>')
			.click(function() {
				_.remove();
			});

		$breadcrumb.prepend(_.buildSwatch());

		return $breadcrumb;
	}

	FilterOption.prototype.remove = function() {
		this.elements.input.prop('checked', false).trigger('change');
	}

	/*
	 * ------------------------------
	 * FILTER GROUP
	 * ------------------------------
	 */

	/**
	 * Filter group object. The object is tied to a DOM element, and gets its
	 * options from the element's data attribute.
	 *
	 * @param {jQuery element} $element - The jQuery element for the filter group
	 */
	var FilterGroup = function($element) {
		if (!$element || !$element.length) {
			return false;
		}

		var _ = this;

		this.elements = {
			group: $element,
			list: $element.find('ul')
		}
		this.options = this.elements.group.data('filter-options');
		this.filters = [];

		// If there are no options to define the element, exit out.
		if (!this.options) {
			console.error('FilterGroup: Initialization failed. Empty or missing options data-attribute: ', this.options);
			return false;
		}

		switch(this.options.type) {
			case 'option':
				this.facet_key = 'options.' + this.options.value;
				break;
			case 'price':
				this.facet_key = 'price_range';
				break;
		}

		this.facet = theme.filters.facets[this.facet_key];
		if (this.facet) {
			this.values = Object.keys(this.facet);

			this.color_group = ['color', 'colour', 'couleur'].indexOf(this.options.value) > -1;
			this.size_group = ['size', 'taille', 'grandeur'].indexOf(this.options.value) > -1;
			this.price_group = this.options.type == 'price';

			// Sort the facets
			if (this.size_group && theme.sorting) {
				var sorted_sizes = [];

				$.each(theme.sorting, function(index, size) {
					var value_index = _.values.indexOf(size);
					if (value_index > -1) {
						// Add to the sorted array
						sorted_sizes.push(size);
						// Remove from the values already found
						_.values.splice(value_index, 1)
					}
				});

				this.values = sorted_sizes.concat(this.values);
			} else {
				this.values.sort();
			}

			this.group_name = this.options.value || this.options.type;

			this.build();
		} else {
			this.destroy();
		}
	}

	FilterGroup.prototype.destroy = function() {
		this.elements.group.remove();
	}

	/**
	 * Builds the filter group options based on facets.
	 *
	 * @memberof FilterGroup
	 */
	FilterGroup.prototype.build = function() {
		var _ = this;
		_.filters = [];
		_.elements.filters = [];

		// Local function to create a single filter for the group
		function createFilter(value, options) {
			var options = options || {};
			options.id = theme.utils.handleize(_.group_name + '-' + value);
			var filter = new FilterOption(value, options);
			_.filters.push(filter);
			_.elements.filters.push(filter.elements.filter);
			_.elements.list.append(filter.elements.filter);
		}

		// If this is a colour group, only output parent colors
		if (this.color_group) {
			$.each(window.colorGrouping, function(parent_color, object) {
				var available_colors = _.values.filter(function(color) {
					return object.colors.indexOf(color) > -1;
				});

				if (available_colors.length) {
					createFilter(parent_color, { swatch: true });
				}
			});
		} else {
			$.each(_.values, function(index, value) {
				if (_.price_group) {
					var options = { format: 'price' }
				}
				createFilter(value, options);
			});
		}

		_.elements.list.appendTo(_.elements.group);
	}

	/**
	 * Builds the query from the filters and builds the active breadkcrumbs.
	 *
	 * @returns Algolia query string
	 * @memberof FilterGroup
	 */
	FilterGroup.prototype.buildQuery = function() {
		var _ = this;
		var active_filters = _.filters.filter(function(filter) {
			return filter.active();
		});

		// Create the breadcrumb elements
		_.filter_ids = [];
		_.elements.breadcrumbs = $();
		$.each(active_filters, function(index, filter) {
			_.filter_ids.push(filter.id);
			_.elements.breadcrumbs = _.elements.breadcrumbs.add(filter.breadcrumb());
		});

		return active_filters
			.map(function(filter) {
				if (_.color_group) {
					return _.buildGroupQuery(filter.value);
				} else {
					return _.facet_key + ':"' + filter.value + '"';
				}
			}).join(' OR ');
	}

	/**
	 * Creates the query for all the colors in that parent color.
	 *
	 * @param {string} group_parent - The parent color
	 * @returns Algolia query string
	 * @memberof FilterGroup
	 */
	FilterGroup.prototype.buildGroupQuery = function(group_parent) {
		var _ = this;
		var color_group = window.colorGrouping[group_parent];

		if (color_group && color_group.colors) {
			return color_group.colors
				.map(function(color) {
					return _.facet_key + ':"' + color + '"';
				}).join(' OR ');
		} else {
			return '';
		}
	}

	FilterGroup.prototype.clear_all = function() {
		$.each(this.filters, function(idnex, filter) {
			filter.disable();
		});
	}

	/*
	 * ------------------------------
	 * FILTERS LOGIC
	 * ------------------------------
	 */

	var Filters = function(collection) {
		var _ = this;
		_.params = theme.utils.getURLParams();

		if (!collection) {
			return false;
		}

		_.elements = {
			products: $('.js-filter-products'),
			breadcrumb_container: $('.js-filter-breadcrumbs'),
			sort_options: $('.js-filter-sort'),
			pagination: $('.js-filter-paginate'),
			html: $('html')
		}
		_.collection = collection;
		_.page = _.params.view_all ? 0 : (parseInt(_.params.page || 1) - 1);
		_.default_sort = theme.algolia.convertSort(theme.current_object.sort_order);
		_.sort = _.params.sort || _.default_sort;
		_.setIndex();
		_.products_per_page = theme.current_object.products_per_page | 48;
		_.pagination = 'page';
		_.scroll_lock = false;
		_.initial_query = true;

		// Initial query to setup pagination and filters
		_.search()
			.then(function() {
				_.initializeGroups();
			});

		// Event handlers
		$(document).on('filter_change', function() {
			_.page = 0;
			_.filter();
		});

		_.elements.sort_options.change(function() {
			_.sortBy($(this).val());
		});

		// Build the breadcrumb container
		_.elements.breadcrumb_container.hide().append('<span class="filters-selected">' + theme.strings.filters.selected + '</span>');
		_.elements.breadcrumb_target = $('<span class="filter-breadcrumbs-target"></span>').appendTo(	_.elements.breadcrumb_container);
		_.elements.clear = $('<button class="filters-clear">' + theme.strings.filters.clear_all + '</button>')
			.click(function() {
				_.clear_all();
			})
			.appendTo(_.elements.breadcrumb_container);
	}

	Filters.prototype.sortBy = function(sort) {
		this.sort = sort;
		this.page = 0;
		this.setIndex();
		this.filter();
	}

	Filters.prototype.setIndex = function() {
		this.index = theme.algolia.client.initIndex('suzy_products_' + this.sort);
	}

	Filters.prototype.clear_all = function() {
		$.each(this.groups, function(index, group) {
			group.clear_all();
		});

		this.filter();
	}

	/**
	 * Creates the filter group objects.
	 */
	Filters.prototype.initializeGroups = function() {
		var groups = [];

		$('.js-filter-group').each(function() {
			groups.push(new FilterGroup($(this)));
		});

		this.groups = groups;

		// Filter based on URL params if necessary
		if (this.params) {
			var refilter = false;

			if (this.params.sort) {
				$('#sort-' + this.params.sort).prop('checked', true);
				refilter = true;
			}
			if (this.params.view_all) {
				this.pagination = 'infinite';
				this.viewAllScroll();
				refilter = true;
			}
			if (this.params.filters) {
				var filter_selectors = this.params.filters.split(',')
					.map(function(selector) {
						return '#' + selector;
					}).join(', ');
				$(filter_selectors).prop('checked', true);
				refilter = true;
			}

			if (refilter) {
				this.filter();
			}
		}
	}

	Filters.prototype.buildQuery = function() {
		if (!this.groups) {
			return '';
		}

		var _ = this;
		this.filter_ids = [];

		return this.groups
			.map(function(filter_group) {
				var query = filter_group.buildQuery();
				_.filter_ids = _.filter_ids.concat(filter_group.filter_ids);
				return query.length ? ('(' + query + ')') : '';
			})
			.filter(function(filter_group) {
				return filter_group.length;
			})
			.join(' AND ');
	}

	Filters.prototype.destroy = function() {
		this.groups = [];
		$('.filters-container').empty();
	}

	Filters.prototype.search = function(hits_per_page, filters) {
		var _ = this;
		var hits_per_page = hits_per_page || this.products_per_page;

		return this.index.search({
			facets: ['*'],
			page: _.page,
			hitsPerPage: hits_per_page,
			filters: 'meta.collection_filter.collections:' + this.collection.id  + ' AND inventory_quantity > 0' + ((filters && filters.length) ? (' AND ' + filters) : '')
		})
			.then(function(response) {
				if (_.initial_query && response.nbHits == 0) {
					// Destroy the filters
					_.destroy();
				} else {
					_.facets = response.facets;
					_.page = response.page;
					_.filter_results = {
						products: theme.algolia.hitsToProducts(response.hits),
						count: response.nbHits,
						pages: response.nbPages
					};
					_.buildPagination();
				}

				_.initial_query = false;
			});
	}

	Filters.prototype.updateBreadcrumbs = function() {
		var _ = this;
		_.elements.breadcrumbs = $();
		$.each(_.groups, function(index, group) {
			_.elements.breadcrumbs = _.elements.breadcrumbs.add(group.elements.breadcrumbs);
		});

		if (_.elements.breadcrumbs.length) {
			_.elements.breadcrumb_container.show();
		} else {
			_.elements.breadcrumb_container.hide();
		}
		_.elements.breadcrumb_target.empty().append(_.elements.breadcrumbs);
	}

	Filters.prototype.filter = function() {
		var _ = this;

		this.search(null, this.buildQuery()).then(function(response) {
			var language = $('html').attr('lang');

			if (language == 'en') {
				var $products = '<p class="filter-empty">No products matched your selection.</p>';
			}
			else if (language == 'fr') {
				var $products = '<p class="filter-empty">Aucun produit ne correspond à votre sélection.</p>';
			}
			
			// Create the product tiles.
			$.each(_.filter_results.products, function(index, product) {
				if (index == 0) {
					$products = window.tile.render(product, 'grid__item one-fifth medium--one-quarter small--one-half product-list__item');
				} else {
					$products = $products.add(window.tile.render(product, 'grid__item one-fifth medium--one-quarter small--one-half product-list__item'));
				}
			});

			// Empty the page if we're in pagination mode and this is not a new query
			if (_.pagination == 'infinite' && _.page != 0) {
				_.scroll_lock = false;
			} else {
				_.elements.products.empty();
			}
			_.elements.products.append($products);

			// Add promotional messaging
			window.promoMessaging.update();

			_.updateBreadcrumbs();

			if (theme.cart.customer){
				window.wishlistStart();
			}

			// Update URL parameters
			var filter_settings = {};
			if (_.page !== 0) {
				filter_settings.page = _.page + 1;
			}
			if (_.sort !== _.default_sort) {
				filter_settings.sort = _.sort;
			}
			if (_.pagination == 'infinite') {
				filter_settings.view_all = true;
			}
			if (_.filter_ids && _.filter_ids.length) {
				filter_settings.filters = _.filter_ids;
			}
			theme.utils.clearURLParams(filter_settings);

		});
	}

	Filters.prototype.scrollToTop = function() {
		var _ = this;
		_.elements.html.animate({
			scrollTop: _.elements.products.offset().top - 140
		}, 700);
	}

	// Infinite scroll function
	Filters.prototype.viewAllScroll = function() {
		var _ = this;

		$(window).on('scroll.view_all', $.throttle(200, function() {
			if (
				_.page < (_.filter_results.pages - 1) &&
				!_.scroll_lock &&
				(_.elements.products.height() + _.elements.products.offset().top - 1000) < ($(document).scrollTop() + $(window).height())
			) {
				_.page = Math.min(_.filter_results.pages, ++_.page);
				_.scroll_lock = true;
				_.filter();
			}
		}));
	}

	Filters.prototype.buildPagination = function() {
		var _ = this;
		var $pagination = $('<div class="filter-pagination"></div>');

		if (_.pagination == 'infinite') {
			$('<button class="pagination-view-all">' + theme.strings.pagination.paginate + '</button>').click(function() {
				_.pagination = 'page';
				_.page = 0;
				$(window).off('scroll.view_all');
				_.filter();
			}).prependTo($pagination);
		} else {
			if (_.filter_results.pages > 1) {
				// View all functionality
				$('<button class="pagination-view-all">' + theme.strings.pagination.view_all + '</button>').click(function() {
					_.pagination = 'infinite';
					_.page = 0;

					if ($(this).closest('.pagination').hasClass('pagination-bottom')) {
						_.scrollToTop();
					};

					_.viewAllScroll();

					_.filter();
				}).prependTo($pagination);

				// Previous
				if (_.page > 0) {
					$('<a class="pagination-button pagination-button-prev" href="#"><span class="pagination-text">' + (theme.strings.pagination.previous == '' ? '<i class="fa fa-caret-left" aria-hidden="true"></i>' : theme.strings.pagination.previous) + '</span></a>').appendTo($pagination).click(function(event) {
						event.preventDefault();
						_.page = Math.max(0, --_.page);

						if ($(this).closest('.pagination').hasClass('pagination-bottom')) {
							_.scrollToTop();
						};

						_.filter();
					});
				}

				$pagination.append('<span class="pagination-text-page">' + theme.strings.pagination.page + '</span>');

				var $paginationSelect = $('<select class="pagination-dropdown"></select>').change(function() {
					_.page = parseInt($(this).val()) - 1;

					if ($(this).closest('.pagination').hasClass('pagination-bottom')) {
						_.scrollToTop();
					};

					_.filter();
				}).appendTo($pagination);

				for (var i = 1; i <= _.filter_results.pages; i++) {
					$paginationSelect.append('<option value="' + i + '"' + (i == (_.page + 1) ? ' selected': '') + '>' + i + '</option>');
				}

				// Of max number of pages
				$pagination.append('<span class="pagination-text-page">' + theme.strings.pagination.of + ' ' + _.filter_results.pages + '</span>');

				// Next
				if (_.page < (_.filter_results.pages - 1)) {
					$('<a class="pagination-button pagination-button-next" href="#"><span class="pagination-text">' + (theme.strings.pagination.next == '' ? '<i class="fa fa-caret-right" aria-hidden="true"></i>' : theme.strings.pagination.next) + '</span></a>').appendTo($pagination).click(function(event) {
						event.preventDefault();
						_.page = Math.min(_.filter_results.pages, ++_.page);

						if ($(this).closest('.pagination').hasClass('pagination-bottom')) {
							_.scrollToTop();
						};

						_.filter();
					});
				}
			} else {
				$pagination.append('<span class="pagination-text-page">' + theme.strings.pagination.page + ' <span class="pagination-button">' + _.filter_results.pages + '</span> ' + theme.strings.pagination.of + ' ' + _.filter_results.pages + '</span>');
			}
		}

		_.elements.pagination.html($pagination);
	}

	// Initialize the filters if on a collection page.
	if (theme.template && theme.template.name === 'collection' && theme.current_object) {
		theme.filters = new Filters(theme.current_object);
	}
})(window.theme = window.theme || {}, jQuery);
$(function() {
	var query = "";

	function showSearch() {
		if($searchPage.length === 0)
		{
			$main.slideUp();
			$searchContainer.fadeIn();
		}

		openStickySearch();
		$closeButtons.show();
		$searchButtons.hide();
		$closeBoxClose.css('display', 'inline-block');
	}

	function hideSearch() {
		if($searchPage.length === 0) {
			$main.slideDown();
		}

		closeStickySearch()
		$searchContainer.fadeOut();
		$closeButtons.hide();
		$searchButtons.show();
	}

	function resetSearch() {
		page = 0;
		numPages = 0;
		$searchOutput.empty();
	}

	function formatBrand(str) {
		return str
			.toLowerCase()
			.split('-')
			.map(function(word) {
				return word[0].toUpperCase() + word.substr(1);
			})
			.join(' ');
	}

	function search(query, promise, reset) {
		paused = true;

		index.search(
			{
				query: query,
				hitsPerPage: 12,
				distinct: 50,
				page: page,
				filters: filters
			},
			function searchDone(err, content) {
				if (err) {
					console.error(err);
					promise.reject();
					return;
				}

				var productMap = {};

				for (var h in content.hits) {
					var product = content.hits[h];

					// Group products (variants) by their product id
					if(!productMap[product.id]) {
						productMap[product.id] = {};
						productMap[product.id].products = [product];
					} else {
						productMap[product.id].products.push(product);
					}
				}

				if (reset) {
					resetSearch();
				}

				var products = [];

				for (productId in productMap) {
					// Remap product options and images to match tile renderer format

					productMap[productId].options = $.map(productMap[productId].products[0].option_names, function(element, index) {
						return { "name": element, "values": new Set(), "position": index + 1 };
					});

					$.each(productMap[productId].products, function(var_index, variant) {
						$.each(Object.keys(variant.options), function(opt_index, optionKey) {
							productMap[productId].options[opt_index].values.add(variant.options[optionKey]);
						});

						variant.options = Object.keys(variant.options).map(function(e) {
							return variant.options[e];
						});

						variant.featured_image = variant.image;
						variant.price *= 100;
						variant.compare_at_price *= 100;
						variant.product_id = variant.id;
						variant.id = variant.objectID;
					});

					$.each(productMap[productId].options, function(index, option) {
						option.old_values = option.values;
						// Covert the Set to an Array with foreach since Array.from is not supported in IE11
						option.values = [];
						option.old_values.forEach(function(element) {
							option.values.push(element);
						});
					});

					var product = theme.utils.clone(productMap[productId].products[0]);
					product.variants = productMap[productId].products;
					product.old_options = product.options;
					product.options = productMap[productId].options;
					product.id = product.product_id;

					products.push(product);
					$searchOutput.append(window.tile.render(product, "product-list__item grid__item one-sixth medium--one-quarter small--one-half"));
				}

				// Add promotional messaging
				window.promoMessaging.update();

				// Set wishlist on the search filter
				if (theme.cart.customer){
					window.wishlistStart();
				}

				if (content.nbHits === 0) {
					var query_string = window.theme.strings.search.no_results.replace("{{ terms }}", content.query);
					var $content = $("<div>").attr("class", "grid__item small--one-whole search-no-results")
					.text(query_string);
					$searchOutput.append($content);
				}

				numPages = content.nbPages;
				$searchCount.html(content.nbHits);
				promise.resolve();
				paused = false;
			}
		);
	}

	function updateSearch(reset) {
		// Add a search action to the search queue and update the queue
		searchQueue = searchQueue.then(function() { return setupSearch(reset); });
	}

	function setupSearch(reset) {
		var promise = new $.Deferred();

		if(currentQuery === query && currentPage === page) {
			promise.resolve();
			return promise;
		}

		var pageLink = "/pages/search";

		if(query) {
			pageLink += "?q=" + query;
			showSearch();

			search(query, promise, reset);
		} else {
			if(reset) {
				resetSearch();
			}
			hideSearch();
			promise.resolve();
		}

		currentQuery = query;
		currentPage = page;

		$searchLink.attr('href', pageLink);

		return promise;
	}

	function getParameterByName(name, url) {
		if (!url) url = window.location.href;
		name = name.replace(/[\[\]]/g, '\\$&');
		var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
			results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		return decodeURIComponent(results[2].replace(/\+/g, ' '));
	}

	// Search Toggle
	var $toggle = $('[data-search-toggle]');

	function toggleStickySearch() {
		$toggle.closest('form').find('.js-search-box').slideToggle();
	}
	function openStickySearch() {
		$toggle.closest('form').find('.js-search-box').slideDown();
	}
	function closeStickySearch() {
		$toggle.closest('form').find('.js-search-box').slideUp();
	}

	// Handle toggling of the search input
	$toggle.click(function() {
		toggleStickySearch();
	});

	var index = theme.algolia.index;
	var page = 0;
	var currentPage = 0;
	var numPages = 0;
	var paused = false;
	var filters = "";
	var distanceFromBottom = 400;
	var currentQuery = "";
	var searchQueue = new $.Deferred();

	var $searchOutput = $('.search__content-output');
	var $main = $('main');
	var $searchContainer = $('.search__container');
	var $searchBox = $(".js-search-box");
	var $closeButtons = $('.search-box-close, .search__clear');
	var $closeBoxClose = $('.search-box-close');
	var $searchButtons = $('.search-form .icon-search');
	var $searchCount = $('.js-search-count');
	var $searchPage = $("#search-output");
	var $searchLink = $(".search__view-all");

	// Resolve the initial search queue
	searchQueue.resolve();

	$searchBox.on('change keyup', function() {

		query = $(this).val();
		// Update all search forms to have the same value
		$searchBox.val(query);
		updateSearch(true);
	});

	$closeButtons.on('click', function(e) {
		e.preventDefault();
		$searchBox.val('');
		query = '';
		updateSearch(true);
	});

	$(".search-form").submit(function(e) {
		e.preventDefault();
		$searchBox.focus();
		updateSearch(true);
	});

	if($searchPage.length > 0) {
		$searchContainer.fadeOut();
		closeStickySearch();
		$searchOutput = $('.search-page__content-output');

		var queryIndex = getParameterByName('q');
		if(queryIndex) {
			$searchBox.val(queryIndex);
			updateSearch(true);
		}

		$(window).scroll(function() {
			// Go to the next page when scrolling to the bottom
			if (!paused && page < numPages && ($(document).scrollTop() + $(window).height() > ($(document).height() - ($("footer").height() + distanceFromBottom)))) {
				page++;
				updateSearch(false);
			}
		});
	}

	filters = 'tags:language_' + window.theme.locale + ' AND inventory_quantity>0';

	var query = getParameterByName('q');
	if (query) {
		$searchBox.val(query);
		updateSearch(true);
	}
});

! function(CountryPopup, $) {
  // DOM elements
  var $countryCover = $('.language-popup__cover');
  var $countryContainer = $('.language-popup__container');
  var $popup = $('.language-popup');

  // Returns wether or not to display the popup
  var canDisplay = function() {
    return !theme.cart_lang && !localStorage.countryPopup;
  }
  var setRegionLang = function(regionLang) {
    if (!regionLang || (theme.cart_json.attributes.region_language && theme.cart_json.attributes.region_language == regionLang)) {
      return false;
    }

    $.ajax({
      type: 'POST',
      url: '/cart/update.js',
      data: { 'attributes[region_language]': regionLang },
      dataType: 'json'
    });
  }

  CountryPopup.getRegionLang = function(callback) {
    function convertRegionLang(lang) {
      var regionLang = (lang && lang.region_code == 'QC') ? 'fr' : 'en';
      if (typeof(callback) === 'function') callback(regionLang);
    }

    function getLocation() {
      geoip2.city(
        function (geolocation) {
          $.post('/cart/update.js', { 'attributes[location]': JSON.stringify(geolocation) }, 
          function(cart) {}, 'json')
          .done(function(cart) {
            theme.cart_json = cart;
            convertRegionLang(geolocation);
          });
        },
        function (error) {
          if (typeof(callback) === 'function') callback(false);
          $('.language-popup__radio #english').prop('checked', true);
        }
      );
    }

    if (theme.cart_json.attributes.location) {
      try {
        var customer_location = JSON.parse(theme.cart_json.attributes.location);
        convertRegionLang(customer_location);
      } catch(e) {
        getLocation();
      }
    } else {
      getLocation();
    }
  }
  CountryPopup.show = function() {
    $countryCover.fadeTo(500, 0.75);
    $countryContainer.fadeIn(1000);
  }
  CountryPopup.hide = function() {
    $countryCover.fadeOut(1000);
    $countryContainer.fadeOut(1000);
  }
  CountryPopup.setLang = function(lang) {
    if (!lang) return false;

    $.ajax({
      type: 'POST',
      url: '/cart/update.js',
      data: { 'attributes[language]': lang },
      dataType: 'json',
      success: function(data) {
        // reload only if there's a change in language
        if (theme.locale != lang) window.location.reload();
        // set the local storage to viewed popup
        localStorage.countryPopup = true;
        CountryPopup.hide();
      }
    });
  }

  // Event handlers
  $('[data-lang-popup-close]').click(function() {
    CountryPopup.hide();
    // set language to default
    CountryPopup.setLang(theme.locale);
  });
  $('.language-popup__form').submit(function(e) {
    e.preventDefault();
    CountryPopup.setLang($('input[name=language-popup]:checked').val());
  });


  // On document ready
  $(function() {
    CountryPopup.getRegionLang(function(regionLang) {
      if (regionLang == 'fr') {
        setRegionLang('fr');
        $('.language-popup__radio #french').prop('checked', true);
      } else {
        setRegionLang('en');
        $('.language-popup__radio #english').prop('checked', true);
      }
    });
    if (canDisplay()) CountryPopup.show();

    if (theme.cart_json && !theme.cart_json.attributes.language && theme.locale) {
      CountryPopup.setLang(theme.locale);
    }
    
  });
}(window.CountryPopup = window.CountryPopup || {}, jQuery);
$(".reorder__move-up, .reorder__move-down").click(function(){
	var row = $(this).parents("tr:first");
	if ($(this).is(".reorder__move-up")) {
		row.insertBefore(row.prev());
	} else {
		row.insertAfter(row.next());
	}
});

$(".giftreggie-permalink").focus(function() {
	var $this = $(this);
	$this.select();

	// Work around Chrome's little problem
	$this.mouseup(function() {
		// Prevent further mouseup intervention
		$this.unbind("mouseup");
		return false;
	});
});
// account registration
$(function() {
  function formValidation($form, message) {
    if (!$form || !message) return false;

    $form.find('.validation').slideUp(function() {
      $(this).remove();
    });

    $('<span style="display:none" class="validation">' + message + '</span>').insertAfter($form.find('input[type="submit"]').first()).slideDown();
  }

	$('#create_customer, #customer_login').on('submit', function(e) {
		e.preventDefault();
		var $form = $(this);
    var customerQuery = {
      email: $form.find('[name="customer[email]"]').val(),
      email_body: '',
      language: 'lang_' + window.theme.locale
    };

    $.post('/apps/api/v1/query_import_customer', customerQuery, function(resp) {
      if (resp.imported_customer == 'true') {
        formValidation($form, window.theme.strings.account.account_already_exists);
        return false;
      } else {
        $form.off().submit();
      }
    })
    .fail(function(resp) {
      $form.off().submit();
    });
	});
});
! function(sticky, $) {
  // DOM elements
  var elements = [];
  var $window = $(window);
  var window_width = $window.width();

  var element = function($elem, options) {
    var _ = this;
    var options = options || {};

    // Calculate if disabled based on min and max breakpoints to stick
    var isDisabled = function() {
      return options.max && options.max < window_width || options.min && options.min > window_width;
    }

    _.selector = $elem;
    _.offset = Math.max($elem.offset().top - $(document).scrollTop(), 0);
    _.stuck = false;
    _.disabled = isDisabled();

    _.stick = function() {
      _.stuck = true;
      _.selector.addClass('sticky');
    };
    _.unstick = function() {
      _.stuck = false;
      _.selector.removeClass('sticky');
    };

    _.update = function() {
      if (_.disabled) return false;
      $(document).scrollTop() > _.offset ? _.stick() : _.unstick();
    }

    // Refresh values after window resize and update
    _.refresh = function() {
      _.offset = Math.max($elem.offset().top - $(document).scrollTop(), 0);
      _.disabled = isDisabled();
      if (_.disabled) _.unstick();
      _.update();
    }
  }

  sticky.refresh = function(element) {
    $.each(elements, function(index, element) {
      element.refresh();
    });
  }

  sticky.update = function(element) {
    $.each(elements, function(index, element) {
      element.update();
    });
  }

  sticky.getElems = function() {
    return elements;
  }

  // Initialize
  $('[data-sticky]').each(function() {
    elements.push(new element($(this), $(this).data('sticky')));
  });

  $(document).scroll(function() {
    sticky.update(elements);
  });

  $(window).resize(function() {
    window_width = $window.width();
    sticky.refresh(elements);
  });

  sticky.update(elements);
}(window.sticky = window.sticky || {}, jQuery);
(function(theme) {
	var QuickAdd = function($element) {
		if (!$element) {
			return false;
		}

		$element = $element.is('[data-quick-add]') ? $element : $element.find('[data-quick-add]').first();

		var _ = this;

		this.elements = {
			container: $element,
			tile: $element.closest('.js-product-tile')
		}
		this.elements.form = this.elements.tile.find('form[action="/cart/add"]');
		this.product_handle = $element.data('quick-add');
		this.display_product_handle = $element.data('quick-add-display');
		this.separate_display_product = this.display_product_handle && this.display_product_handle !== this.product_handle;

		this.loaded = false;

		// Build the element
		this.elements.button = $('<button class="quick-add-button" type="button"><span>' + theme.strings.quickview.label + '</span><span class="quick-add-icon">+</span></button>')
			.click(function() {
				_.open();
			})
			.appendTo(this.elements.container);

		// Save color selector and bind change event
		this.elements.colors = this.elements.tile.find('.js-quick-add-color')
			.change(function() {
				var $this = $(this);
				_.selected_color = {
					value: $this.val(),
					name: $this.data('name')
				};
				if (_.loaded) {
					_.updateOptions();
				}
			});

		// Return to the quickview button 5 seconds after mouse out
		this.elements.tile.on('mouseleave', function() {
			_.closing = true;

			setTimeout(function() {
				if (_.closing) {
					_.close();
				}
			}, 5000);
		});
		// If a mouse enter occurs in the mean time, cancel closing
		this.elements.tile.on('mouseenter', function() {
			_.closing = false;
		});
	};

	// Returns an array of the selected variants, matching the passed options as an object
	QuickAdd.prototype.selectedVariants = function(options) {
		return this.product.variants.filter(function(variant) {
			return (options.option1 ? (variant.option1 == options.option1) : true) &&
				(options.option2 ? (variant.option2 == options.option2) : true) &&
				(options.option3 ? (variant.option3 == options.option3) : true);
		});
	}

	// Returns the matching variant option between the display and added product
	// Example, if option1 is the color, pass "1" and "Blanc" to get "White" for the English product
	QuickAdd.prototype.getMatchingOption = function(option_position, option_value) {
		var _  = this;
		var variant_sku = _.display_product.variants.filter(function(variant) {
				return variant['option' + option_position] == option_value;
			})[0].sku;
		return _.product.variants.filter(function(variant) {
				return variant.sku == variant_sku;
			})[0]['option' + option_position];
	}

	// Build the form, bind all elements
	QuickAdd.prototype.buildOptions = function() {
		var _ = this;

		this.elements.form = $('<form action="/cart/add" method="post" enctype="multipart/form-data" class="quick-add-atc"><span class="quick-add-label">' + theme.strings.quickview.atc + '</span></form>')
			.submit(function(event) {
				event.preventDefault();
				if (_.elements.atc_error) {
					_.elements.atc_error.slideUp(function() {
						$(this).remove();
					});
				}
				var form_data = $(this).serialize();
				window.Cart.addItem(form_data)
					.done(function(response) {
						window.Cart.openCart();
					})
					.fail(function(response) {
						console.warn('QuickAdd: Failed to add to cart.', response);
						_.elements.atc_error = $('<span class="quick-add-error" style="display:none">' + theme.strings.addingFail + '</span>')
							.appendTo(_.elements.form)
							.slideDown();
					});
			});

		// Add the hidden options for add to cart
		var $hidden_inputs = $();
		this.elements.id = $('<input type="hidden" name="id">');
		$.each(this.display_product.options, function(index, option) {
			if (option) {
				var option_key = 'option' + option.position;
				_.elements[option_key + '_title'] = $('<input type="hidden" name="properties[_option_title' + option.position + ']" value="' + option.name + '">');
				_.elements[option_key + '_value'] = $('<input type="hidden" name="properties[_' + option_key + ']">');
				$hidden_inputs = $hidden_inputs.add(_.elements[option_key + '_title']).add(_.elements[option_key + '_value']);
			}
		});

		$hidden_inputs
			.add(this.elements.id)
			.add($('<input type="hidden" name="properties[_url]" value="' + this.display_product.url + '">'))
			.add($('<input type="hidden" name="properties[_title]" value="' + this.display_product.title + '">'))
			.add($('<input type="hidden" value="Tile" name="properties[_added_from]">'))
			.add($('<input type="hidden" value="' + promoMessaging.getProductMessages()[this.display_product.id].collections.join(',') + '" name="properties[_collection_ids]">'))
			.appendTo(this.elements.form);

		this.elements.option_container = $('<div class="quick-add-options"></div>').appendTo(this.elements.form);
		this.elements.options = $();

		// Create an element for each size
		$.each(this.size_option.values, function(index, size_name) {
			var size_value = _.separate_display_product ? _.getMatchingOption(_.size_option.position, size_name) : size_name;

			// 03.28.2018 - Hotfix to rename French sizes coming through on English products
			// TODO: remove once product data is fixed
			size_name = theme.utils.translateOption(size_name);

			// Create the input
			var $option = $('<button type="button" class="quick-add-size" value="' + size_value + '" data-name="' + size_name +'">' + size_name + '</button>')
				.click(function() {
					// Build the variant options
					var selected_options = {};
					var $this = $(this);
					var size = $this.val();
					var name = $this.data('name');
					if (_.color_option) {
						selected_options['option' + _.color_option.position] = _.selected_color.value;
					}
					if (_.size_option) {
						selected_options['option' + _.size_option.position] = $this.val();
					}

					// Find the variant
					var variant = _.selectedVariants(selected_options)[0];

					// Set the hidden inputs on the form
					_.elements.id.val(variant.id);
					// Color
					if (_.color_option) {
						_.elements['option' + _.color_option.position + '_value'].val(_.selected_color.name);
					}
					// Size
					if (_.size_option) {
						_.elements['option' + _.size_option.position + '_value'].val(name);
					}

					_.elements.form.submit();
				});

				_.elements.options = _.elements.options.add($option);
		});
		this.elements.options.appendTo(this.elements.option_container);

		// Add the quick add panel to the tile
		this.elements.container.append(this.elements.form);

		// Update the options with available sizes
		_.updateOptions();
	};

	QuickAdd.prototype.setDisplayProduct = function(product) {
		if (!product) {
			return false;
		}

		this.display_product = product;

		// Set the size option
		var size_names = ['size', 'grandeur'];
		this.size_option = this.display_product.options.filter(function(option) {
			return size_names.indexOf(theme.utils.handleize(option.name)) !== -1;
		})[0];

		// Set the color option
		var color_names = ['color', 'couleur', 'colour'];
		this.color_option = this.display_product.options.filter(function(option) {
			return color_names.indexOf(theme.utils.handleize(option.name)) !== -1;
		})[0];
	}

	QuickAdd.prototype.setProduct = function(product) {
		if (!product) {
			return false;
		}

		this.product = product;
	}

	QuickAdd.prototype.load = function() {
		var _ = this;
		var deferred_calls = [];
		this.elements.button.find('.quick-add-icon').replaceWith('<i class="quick-add-load fa fa-spinner fa-spin" aria-hidden="true"></i>');

		// Display another product but add an English product to cart
		if (_.separate_display_product) {
			deferred_calls.push(
				$.get('/products/' + _.display_product_handle + '.js', function(product) {
					_.setDisplayProduct(product);
				}, 'json')
			);
		}

		deferred_calls.push(
			$.get('/products/' + _.product_handle + '.js', function(product) {
				_.setProduct(product);
				if (!_.separate_display_product) {
					_.setDisplayProduct(product);
				}
			}, 'json')
		);

		// Return when the AJAX chain is complete.
		// Multilingual has multiple products to AJAX.
		return $.when.apply(undefined, deferred_calls).then(function() {

			_.elements.colors.each(function() {
				var $this = $(this);
				var color = $this.val();

				// Update the color values to match with English
				if (_.separate_display_product) {
					var new_color = _.getMatchingOption(_.color_option.position, color);
					$this.val(new_color)
				}

				$this.data('name', color);
			});

			var $selected_color = _.elements.colors.filter(':checked');
			_.selected_color = {
				value: $selected_color.val(),
				name: $selected_color.data('name')
			};

			_.buildOptions();

			// Mark product as loaded
			_.elements.button.find('.quick-add-load').replaceWith('<span class="quick-add-icon">+</span></button>');
			_.loaded = true;
		});
	};

	QuickAdd.prototype.open = function() {
		var _ = this;

		if (this.loaded) {
			this.elements.container.addClass('quick-add-open');
		} else {
			this.load().then(function() {
				_.elements.container.addClass('quick-add-open');
			});
		}
	};

	QuickAdd.prototype.close = function() {
		this.elements.container.removeClass('quick-add-open');
		if (this.elements.atc_error) {
			this.elements.atc_error.slideUp(function() {
				$(this).remove();
			});
		}
	};

	// Updates size options
	QuickAdd.prototype.updateOptions = function() {
		var _ = this;

		// Generate the options
		var selected_options = {};
		if (this.color_option) {
			selected_options['option' + this.color_option.position] = this.selected_color.value;
		}

		// Find matching variants
		var selected_variants = this.selectedVariants(selected_options);
		// Create an array of available sizes only
		var available_sizes = selected_variants.filter(function(variant) {
				return variant.available && variant.featured_image;
			}).map(function(variant) {
				return variant['option' + _.size_option.position];
			});

		// Disable options that are not in the list of available sizes
		this.elements.options.each(function() {
			if (available_sizes.indexOf($(this).val()) > -1) {
				$(this).prop('disabled', false);
			} else {
				$(this).prop('disabled', true);
			}
		});
	};

	// Initialize all quick adds on the page
	$(function() {
		$('.js-quick-add').each(function() {
			new QuickAdd($(this));
		});
	});

	theme.QuickAdd = QuickAdd;
})(window.theme = window.theme || {}, jQuery);
(function($) {
	var nostojs = window.nostojs || false;

	if (!nostojs) {
		var name="nostojs";
		window[name]=window[name]||function(cb){(window[name].q=window[name].q||[]).push(cb);};
		nostojs = window.nostojs;
	}

	function updateNostoPromos() {
		nostojs(function(api){
			api.listen('postrender', function(nostoPostRenderEvent) {
				if (!!promoMessaging) {
					promoMessaging.update();
				}
			});
		});
	}

	$(function() {
		if (!!nostojs) {
			updateNostoPromos();
		}
	});

})(jQuery);

$(document).ready(function() {
  var sections = new slate.Sections();
  sections.register("product", theme.Product);
  sections.register("cart", theme.Cart);

  // Common a11y fixes
  slate.a11y.pageLinkFocus($(window.location.hash));

  $(".in-page-link").on("click", function(evt) {
    slate.a11y.pageLinkFocus($(evt.currentTarget.hash));
  });

  // Wrap videos in div to force responsive layout.
  slate.rte.wrapTable();
  slate.rte.iframeReset();

  // Apply a specific class to the html element for browser support of cookies.
  if (slate.cart.cookiesEnabled()) {
    document.documentElement.className = document.documentElement.className.replace(
      "supports-no-cookies",
      "supports-cookies"
    );
  }

  //Custom dropdown functionality
  $('.dropdown').click(function() {
    $(this).toggleClass('active');
    $('.dropdown__option').click(function() {
      $('.dropdown__button').html($(this).data('value'));
    });
  });

  //Set custom dropdown default value
  if ($('.dropdown__button').text() == "") {
    var $defaultOption = $('.dropdown')
      .find('.dropdown__option')
      .first()
      .data('value');
    $('.dropdown__button').html($defaultOption);
  }

  //Close dropdown if clicking anywhere else on the page
  window.onclick = function(event) {
    if (!$(event.target).is('.dropdown, .dropdown *') && $('.dropdown').hasClass('active')) {
      $('.dropdown').removeClass('active');
    }
  }

  // Initialize collection grid masonry layout
  $(".collection-grid").masonry({
    itemSelector: '.collection-grid__item-outer',
    columnWidth: '.grid-sizer',
    gutter: '.gutter-sizer',
    percentPosition: true
  });
});

//
// Replay Queued jQuery
//
$.each(window.__jqReady || [], function (index, f) {
  if (typeof f === 'function') {
      $(f);
  } else {
      console.warn('non function jquery call before jquery loaded', f);
  }
});

// Load all sliders, remap and masonry
$(function() {
  var initialize = function() {
    $('.slick').slick();
  }

  initialize();

  $(document).on('shopify:section:load', function () {
    initialize();
  });

  // Use class 'remap' to responsively map images
	$('.remap').rwdImageMaps();
});

function getUrlVars() {
  var vars = [], hash;
  var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
  for(var i = 0; i < hashes.length; i++) {
      hash = hashes[i].split('=');
      vars.push(hash[0]);
      vars[hash[0]] = hash[1];
  }
  return vars;
}

/* HC - - Tabs for contact form - 02 Apr '19 */
$( ".tab-toggle" ).on( "click", function() {
  jQuery(this).parent().toggleClass('open');
});
