"use strict";

/**
 * Gift Card Template Script
 * ------------------------------------------------------------------------------
 * A file that contains scripts highly couple code to the Gift Card template.
 */

/* This function selects a range of text
*  it can be called like following on load:
*  selectText("GiftCardDigits");  
*/
function selectText(elementID) {
  var doc = document;
  var text = doc.getElementById(elementID);

  if (doc.body.createTextRange) {
    // ms
    var range = doc.body.createTextRange();
    range.moveToElementText(text);
    range.select();
  } else if (window.getSelection) {
    // moz, opera, webkit
    var selection = window.getSelection(),
        range = doc.createRange();
    range.selectNodeContents(text);
    selection.removeAllRanges();
    selection.addRange(range);
  }
}

(function () {
  var config = {
    qrCode: '#qr-code',
    giftCardCode: '.giftcard__code'
  };
  var $qrCode = $(config.qrCode);

  if ($qrCode.length) {
    //generate QRCode
    new QRCode($qrCode.get(0), {
      text: $qrCode.attr('data-identifier'),
      width: 100,
      height: 100
    });
  }
})();