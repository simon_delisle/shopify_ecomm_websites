/***
Spot SDK Documentation
======================

The Spot SDK is a single javascript file, which provides all spot functionality in an easy-to-digest package. The SDK consists of three separate classes,
briefly described below. Please read this entire section so you have some idea of the conventions; it'll only take a minute.

**If you're looking to install Spot on a free theme on Shopify, or a relatively standard theme**, it is recommended that you use the Spot "section" that
comes out of the box with Spot. It works by default with at least the following themes:

* Simple
* Minimal
* Brooklyn
* Narrative
* Supply
* Venture
* Boundless
* Debut

**If you are beginning work on a custom theme**, you'll likely want to take a look at the snippet anyway to get a feel for how the system is initialized. Or, if you're
feeling very adventurous, you can get started here with a few examples using the base SDK, provided with the [`SpotDefault`](#spotdefault-documentation) class.
If you are going to do a custom implementation, it's possible, depending on your implementation, that you won't need to touch `SpotAPI` or `SpotDOM` at all.
If you just want to dive in, start with the [implementation checklist](#implementation-checklist-sample-implementation).

You can retrieve the spot.js file [here](/docs/spot.js).

## [SpotAPI](#spotapi-documentation)

This class contains the **low-level API**. It lets you construct queries, send them to Spot, and get a JSON representation of the objects you want back. This
is the bare minimum you'll need to create a Spot implementation on a website.

## [SpotDOM](#spotdom-documentation)

This class contains a **higher-level API**, which keeps track of things like page state, and facet composition, allowing you to easily build things like a
pagination mechanism, a sorting dropdown, a faceting panel, a search bar, that sort of thing.

## [SpotDefault](#spotdefault-documentation)

This class contains a **sample high-level implementation** that uses the two above classes. Using this will allow you to create default-HTML versions of
various standard Spot components, such as a faceting panel, a paged paginator, and a search-as-you-type bar. This is called directly by the Spot section,
that works out-of-the-box with all free themes on Shopify.

## Conventions

### Casing

By default, the SpotSDK will *always* use **camelCase** when dealing with variables, methods, and most other data. It will use **TitleCase** when dealing with class names.

### No Modification

In an ideal world, there should be **no modification** of `spot.js`, as new versions of the file are pushed out regulary, and merging them can be onerous. If you
find you need to modify `spot.js` during what you would consider to be fairly normal operations, please contact us, and let us know what you're doing! We'd
be happy to add it to the API if it doesn't exist, or give clarification about how to perform a particular operation.

## Are there are sample implementations of Spot?

You can see https://merchman-filters.myshopify.com for a sample implementation of the system, or look at [SpotDefault](#spotdefault-documentation) below.

## Where can I go to get help?

You can contact spot@moddapps.com to get more infromation, or visit our SDK guide at https://spot.moddapps.com/docs, which will have a few more examples.

If you're internal to Modd, or to Diff, you can feel free to reach out to the #spot channel, on Slack.

## Found a bug?

If you've found a bug, please tell us at the address above! If you do file a bug report, please include the following information.

1. Your .myshopify.com URL.
2. The time at which the bug ocurred.
3. Whether or not the bug is consistently repeatable.

**If your bug is with the Spot API itself**, meaning that you are not getting the expected result from the search service, please also include:

1. The full request you're sending. (Including all Request Headers)
2. The full response you're receiving. (Including all Response Headers)

If you have a browser debugger active, this information can usually be found under the "Network" tab.

**If your bug is with the Spot SDK**, please ensure that you haven't modified `spot.js` (or if you have, that none of the modifications have any effect on the bug at hand). In this case, be sure to include the following information.

1. A link to preview the Shopify theme on the store where you're seeing the issue.
2. Any actions to take that will reproduce the problem.

## [Endpoint Details](#spot-endpoint-documentation)

In addition to this SDK, there are also a few details on the actual endpoint itself, that is directly queriable in HTTP, should you be in an environment that does not support JavaScript.

***/

/***

SpotAPI Documentation
=====================

## Overview

### What is this class?

This is the low-level query API for Spot. What this means is, this is how you talk to Spot directly, without any intervening modules.

### What are its depednencies?

There are currently no dependencies for the base API.

### When should I use it?

If you're trying to implement a collections page with various filters, you should not use this directly. You should use [SpotDom](#spotdom-documentation) which
provides a lot of time-saving functionality, built over top of this. If you need/want to talk to the shopify product index directly, this is what
you should use.

### How do I use it?

The first step to using this module is to instantiate it, as an object, with new. Like below:

	var spotAPI = new SpotAPI({{ shop.metafields.esafilters.clusters | json }});

You can treat this as a global object; you really only ever need to make one of these. The object keeps track of some global options
(stuff like which filters server to point at, whether to use JSONP, GET, or POST, etc..), you'd like to use for all your queries.
It has sensible defaults, so you don't really need to customize anything unless you really want to; it should just work.

Once you've made the object, you can start making queries. A query is also an object. To start a query, type:

	var query = spotAPI.s();

This will instantiate a query object. Queries record most of the stuff you want to communicate to filters, like what collection you'd
like to look up, what colors of things you want to see, the price range, the pagination, etc.., etc... To specify these things, you make
method calls on the query object, like you would a normal javascript object. There are a bunch of different fields to choose from; and you
can choose exactly what you want to query, by passing desired paramters to the query object. There are a number examples in the querying
section below.

The query object keeps track of all operations you perform on it. As you're constructing your query, no remote calls to the server have been made yet,
you're just assembling your request in javascript. Once you've finished querying things, and are ready to finish up, call e(), on the object.

	query.e()

This will make a query to the server. This function returns a jQuery-style deferred (you can also pass in a success function to it).
What this means is that you can then make things happen depending on what the result of the query is. So, instead of just calling e()
(which does nothing), you can tell it to do things after we get our data back from Spot.

	query.e().done(function(products) {
		console.log(products);
	});

This will dump all the products that matched your query to the console. You can do whatever you like, though, build HTML, amend the page,
whatever you like.

In summary, as an example:

	new SpotAPI({{ shop.metafields.esafilters.clusters | json }}).s().product_type("T-Shirt").e().done(function(products) {
		console.log(products);
	});

Will output a list of all products that have the product type of T-Shirt to the console.

## Querying Basics

### Filtering

Say for example, you want to get all products, that have the vendor "My Vendor" in the store. After instanting the query, you do:

	query = query.vendor("My Vendor");

or

        query = query.vendor("==", "My Vendor");

The operator can be changed to any of the following: ==, !=, ^, in, not_in.


This will add the requirement that you only want red products to the query. You can also chain queries together. Let's say you've made
the call above, and now you decide you also want to restrict things to those products under 100$, and only hats. So, you'd then do:

	query = query.price("<", 100).product_type("Hat");

The list of full properties that can be queried is as follows: "id", "price", "grams", "compare_at_price", "inventory_quantity", "priority", "title", "option1", "option2", "option3", "product_type", "vendor","tags", "sku", "product_metafield", "variant_metafield", "option", "available".

Metafields and options work a bit differently, as you have to specify the name of the option, or the namespace and key of the metafield , like so:

        query = query.option("Color", "==", "Red")

        query = query.product_metafield("myKey", "myNamspace", "<", 100)

In addition to the normal operators, you can also perform and and or queries:

        query = spotAPI.s().and(query1, query2)

        query = spotAPI.s().or(query1, query2)

### Searching

Searching works much like regular filtering; you can also narrow down *any* resultset with free-text searches.

        query = query.search("Red Dresses")

#### Spell Checking

By default you can also have your results spelling-corrected.

	query = query.search("Red Drasses")

This will likely automatically return "Red Dresses", if that exists in your catalog. The rules of the spell check are also returned under the `spellCheck` key in the options
arugment of the `done` function. Allowing you to easily construct "Did you mean?" dialogs.

This behaviour can be turned off by using `.autoCorrect(false)`.

You can also just use `.spellCheck(true).autoCorrect(false)` to get the results of the spellcheck, if you want to manage how that works yourself, and not have the user's search
be autoCorrected to a most likely correct search.

### Collections

You can also restrict searches to occur inside a particular collection with one of two operators.

        query = query.collection("my-collection-handle")

        query = query.in_collection("my-collection-handle")

The difference here is that collection should be used whenever you're on a particular collection's page, or if you need to operate in the context of that collection. It lets you do things like sort by the sort order of that collection, and a couple of other things. in_collection should be used when you need to use it as part of a query; or if you're creating collection unions.

### Sorting

#### Default Sort Orders

Sorting can be used in various ways. The normal Shopify sorting strings that exist on the default front-end of Shopify can be passed to the query object to change the sort order
of the object. As an example:

	query = query.sort("alpha-asc")

You can also, more intuitively pass an object to this function, and specify the sort direction independent of the sort field, like so:

	query = query.sort({ "asc": "alpha"})

#### Collection Sort Order

Using the sort order "manual" will sort the collection via the collection's specified manual ordering. Please note that this *only* applies when the query has a collection associated with it.

	query = query.sort("manual")

#### Search Sort Order

Using the sort order `"search"` will sort the resultset based on the most relevant results for a given search term. Please note that this *only* applies when a search term has been provided with `.search`

	query = query.search("My Search Terms")

The above query will automatically set the sort order to `"search"` if their has been no previous sort order set. You can reset a query's sort order back to `"search"` explicitly by doing:

	query = query.sort("search")

#### Priority Sort Order

Uses the priorities set in the spot back-end control panel to order the resultset.

	query = query.sort("priority")

#### Custom Sort Order

You can also specify a non-standard sort order; like sorting on a named option, or a metafield, if you've specified it in the Spot control panel.

	query = query.sort({"option":"Size"})

	query = query.sort({"product-metafield":{"namespace":"key"}})


### Variant-Level Querying

Variant-level functionality is a significant advantage that Spot has over other search engines on Shopify. From the get-go Spot was built to work
with Shopify's variant system, so you have a number of extra operators open to you that are specifically designed to make your life easier.

By default, resultsets are returned the same way Shopify normally returns things; as products. For example, if you ask for a "Red T-Shirt", and
there's a T-Shirt product, that has at least one variant that's red, we'll return the whole product, and all its variants. However, if you
use the allVariants property, like so, you'll receive a product cotaining ONLY the variants that match.

	query.option("Color", "Red").product_type("T-Shirt").allVariants(false).e().done(function(products) {
		console.log(products);
	});

If you want to narrow your search/query/filter to include ONLY products in which all variants match your query, you can do the following,
much as with and/or:

	spotAPI.s().all(query.option("Color", "Red").product_type("T-Shirt")).allVariants(false).e().done(function(products) {
		console.log(products);
	});

The above would only return you T-Shirts that have "Red" as the only color option.

In addition, Spot also lets you specify what's known as an "index-split", using the "split" property, like so.

	query.split("my-color-split").e().done(function(products) {
		console.log(products);
	});

By default, the split value is "none", which is always valid. Other splits must be pre-defined in the Spot back-end, but can do things like
break out the variants of each product by a named option, or a metafield. So, for example, if you set up a split on "Color" as an option, in
the back-end of Spot, and then activate it as above, what'll happen is your catalog will segment into one "product" per group of variants that have
a disctinct value of the particular option. For example, if you have a T-Shirt that is Red, Green, or Blue in Color, and has sizes from XS, S, M, L, XL,
for a total of 15 variants; specifying a color split will split this into 3 separate products, with 5 variants each.

You can also, simply split across all options, meaning that it'll split your catalog into one variant, one product; allowing you to see everything
at the variant-level.

## ResultSet Basics

### ResultSet Counting

To see the count included as part of the results, you can do:

        query = query.count(true)

By default, this will return the count how many products there are in a resultset. Normally, Spot will do an *approximate* count. This is significantly faster
that doing an actual count. For example, if your resultset is 10,000 products long; an approximate count allows us to really only grab the first hundred or so
products then extrapolate what an approximation of what the count is likely to be. This is usually pretty accurate, and very fast.

However, we recognize that under certain circumstances, it can be desirable to retrieve the *exact* amount of results. This takes longer, and necessitates more
computational power being devoted to your store (and a corresponding increase in cost), but it is possible, via the following call:

	query = query.count(true, "exact")

This will return the count as a second argument when you complete a query:

	query.e().done(function(products, count) {
		console.log(count);
	});


### ResultSet Faceting

#### Simple Faceting

This can be done via the .options query. In this, you can do something like:

        query = query.options(["product_type"])

Which will show you all the product types, as well as their counts. This information can be found in the third argument to "done", the full response body:

	query.e().done(function(products, count, resposne) {
		console.log(response);
	});

You can also facet on more than one facet at a time.

	query = query.options(["product_type", "vendor", {"option":"Size"}])

You can also be more specific, if you want only a subset of facets.

	query = query.options([{"product_type":["Dress", "Shoes"]}, "vendor", {"option":"Size"}])

#### Disjunctive Facet Counts

Now, when you get options back, usually you'll present these to the uesr as clickable facets. Frequently, you'll want to display counts next to the facets as well.
Normally, counts are simply the numbers of each facet in the resultset. However, there's usually an exception in the case of counts dispalyed for facets that are in the same category
as a facet that has been selected; that facet is usually stripped when computing the counts.

As an example, let's say you're faceting on Color and Product Type. For color we return the following result: `{"option":{"Color":{"Red":10,"Blue":"20","Green":15}}`.
If you're not clicking any of these facets, the counts are correct. Let's say the user then clicks on your "Red" facet. This of course reduces the resultset to the following: `{"option":{"Color":{"Red":10}}`.
This however, is not desirable. In traditional faceting schemes, the user is expecting the facet counts to be represented *as if there was no facet applied in the category in question*. The actual
result the user is expecting is still `{"option":{"Color":{"Red":10,"Blue":"20","Green":15}}`, despite having the facet selected.

This can be achieved by specifying the conditions that you apply are located under an `or` array, inside a `facets` array. For example, instead of the following to represent someone
clicking the "Red" facet:

	query.option("Color", "Red").options([{"option":"Color"}, "product_type"])

You want to do this:

	query.facets(spotAPI.s().option("Color", "Red"), spotAPI.s()).options([{"option":"Color"},"product_type"])

Likewise, if you wanted to use a product type facet, you'd do this:

	query.facets(spotAPI.s(), spotAPI.s().product_type("Jeans")).options([{"option":"Color"},"product_type"])

This is all handled for you if you use the SpotDOM SDK; but if you're writing your own DOM module, you'll need to wrap your options like that. Not only will this let spot know
that you explicitly want to have a particular query parameter be part of a facet, but it will also inform Spot's back-end analytics engine about your facet use.

#### Facet Grouping

In addition to faceting normally; you can also group facet values together, into a single facet. This is useful for stuff like
groups of product types, or colors. You can also do the same thing for groups of product prices.

##### Qualitative Groups

As an example for grouping together lists of items:

	query = query.options([
		{"product_type":[{"name":"Dresses","value":["Dress","Knit Dress","Jupe"]},{"name":"Coats","value":["Winter Jacket","Pea Coat"]}]}
		{"option":{"Color":[{"name":"Red","value":["Red","Magenta","Purple"]},{"name":"Blue","value":["Blue","Aqua","Cyan"]}]}}
	])

##### Quantitative Groups

Here's an example for grouping things together based on an item's price.

	query = query.options([
		{"type":"price","name":"Price","values":[
			{"name":"Under 10$","value":"-10"},
			{"name":"10-20 $","value":"10-20"},
			{"name":"20-30 $","value":"20-30"},
			{"name":"30-40 $","value":"30-40"},
			{"name":"40-50 $","value":"40-50"},
			{"name":"50-60 $","value":"50-60"},
			{"name":"Over 60$","value":"60+"}
		]}
	])

## Examples

All the following examples below can be run in the Chrome debugger console, on any page in a Shopify store which includes this file, and instantiates
the SpotAPI. Try them out, to get a feel of exactly what happens!

Gets all products.

	spotAPI.s().e(function(products, count) {
		console.log(products);
	});

Gets all products in collection "my-collection-handle", over 10$ and under 20$, paginated by 50, on page 2, sorted by vendor.

	spotAPI.s().collection("my-collection-handle").price(">=", 10).price("<", 20).paginate(50).page(2).sort("price").e().done(function(products, count) {
		console.log(products);
	});

All products in collection "my-collections-handle" NOT over 10$ and under 20$, all with the vendor "My Vendor" that are available for purchase.

	spotAPI.s().collection("my-collection-handle").vendor("My Vendor").or(spotAPI.s().price("<", 10), spotAPI.s().price(">=", 20)).available(true).paginate(50).page(2).e(function(products, count) {
		console.log(products);
	});

Gets the first 50 results of a search page, for "Gold" performing a spelling correction if no results were found.,

	spotAPI.s().search("Gold").autoCorrect(true).paginate(50).e(function(products, count, options) {
		console.log(products);
	});

Uses JSONP to get the first 5 products in the store, sorted by the store owner's preference.

	spotAPI.s().jsonp(1).sort("priority").paginate(5).e(function(products) {
		console.log(products);
	});

Set a default for all queries to be 48 products, and only show things that have the tag "Showing", and always sort by bestselling.

	spotAPI.defaultQuery(spotAPI.s().paginate(48).tag("Showing").sort("bestselling"));
	spotAPI.s().page(4).e(function(products) {
		console.log(products);
	});

Filters based on options

	spotAPI.s().option("Color", "Red").paginate(48).e(function(products) {
		console.log(products);
	});

****************************************************************/


var SpotAPI = SpotAPI || function(clusters, silenceErrors) {
	// ========== BEGIN INTERNALS =============
	// These are mostly internals, and can be ignored.
	var spotAPI = this;
	this._silenceErrors = silenceErrors || 0;
	this._defaultQuery = null;
	this._jsStyle = true; // set false to get Shopify backend object style
	this.clone = function(obj) {
		return new queryObject([], {}).merge(obj);
	};

	this.setCookie = function(cookieName, cookieValue, expDays) {
		var exdate=new Date();
		if (expDays != null)
			exdate.setDate(exdate.getDate() + expDays);
		var c_value=encodeURIComponent(cookieValue) + ((expDays==null) ? "" : "; expires="+exdate.toUTCString());
		c_value += "; path=/";
		document.cookie=cookieName + "=" + c_value;
	};
	this.deleteCookie = function(cookieName) {
		document.cookie = cookieName + '=; path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
	};
	this.getCookie = function(cookieName) {
		var c_value = document.cookie;
		var c_start = c_value.indexOf(" " + cookieName + "=");
		if (c_start == -1)
			c_start = c_value.indexOf(cookieName + "=");
		if (c_start == -1)
			c_value = null;
		else {
			c_start = c_value.indexOf("=", c_start) + 1;
			var c_end = c_value.indexOf(";", c_start);
			if (c_end == -1)
				c_end = c_value.length;
			c_value = decodeURIComponent(c_value.substring(c_start,c_end));
		}
		return c_value;
	}

	this.baseHostnames = [];
	this.successfulHostname = null;
	this.basePath = "/";
	this._sessionId = this.getCookie("spot_sessionid");


	// Customer identifier, can be cart token, or some other unique identifier. If no identifier is specified, the system will
	// attempt to use IP to disambiguage in a single session. This will allow for things like personalized recommendations, and the like.
	// By default this should probably be the Shopify session ID. If you do not specify one, you will be assigned one via a response header.
	// Generally, PII should not be transmitted here for privacy reasons, so this should be kept to things like IDs, and other non-identifiable
	// information.
	this.sessionId = function(sessionId) {
		this._sessionId = sessionId;
		this.setCookie("spot_sessionid", sessionId);
		return this;
	};

        this.clusters = function(clusters) {
		this.baseHostnames = clusters;
		return this.baseHostnames;
        };
        if (clusters)
		this.clusters(clusters);
        this.path = function(path) {
		this.basePath = path;
		return this.basePath;
	};


	this.encodeUrlVars = function(hash) {
		var query = "";
		var keys = Object.keys(hash).sort();
		for (var keyIdx = 0; keyIdx < keys.length; ++keyIdx) {
			var key = keys[keyIdx];
			if (typeof hash[key] == "object") {
				for (var i in hash[key])
					query += (query == "" ? "" : "&") + key + "=" + encodeURIComponent(hash[key][i]);
			}
			else
				query += (query == "" ? "" : "&") + key + "=" + encodeURIComponent(hash[key]);
		}
		return query;
	};

	var DeferredObject = function() {
		// true is success, false is fail, null is pending.
		var deferred = this;
		this.state = null;
		this.payload = null;

		this._doneHandlers = [];
		this._failHandlers = [];
		this._alwaysHandlers = [];

		this.resolve = function() {
			deferred.payload = arguments;
			deferred.state = true;
			while (deferred._doneHandlers.length > 0) {
				var handler = deferred._doneHandlers.shift();
				handler.apply(deferred, deferred.payload);
			}
			return deferred;
		};
		this.reject = function() {
			deferred.payload = arguments;
			deferred.state = false;
			while (deferred._failHandlers.length > 0) {
				var handler = deferred._failHandlers.shift();
				handler.apply(deferred, deferred.payload);
			}
			return deferred;
		};

		this.done = function(handler) {
			if (deferred.state === true)
				handler.apply(deferred, deferred.payload);
			else
				deferred._doneHandlers.push(handler);
			return deferred;
		};
		this.fail = function(handler) {
			if (this.state === false)
				handler.apply(deferred, deferred.payload);
			else
				deferred._failHandlers.push(handler);
			return deferred;
		};
		this.always = function(handler) {
			if (this.state != null) {
				handler.apply(deferred, deferred.payload);
			} else {
				deferred._doneHandlers.push(handler);
				deferred._failHandlers.push(handler);
			}
			return deferred;
		};
	};

	this.Deferred = function() {
		return new DeferredObject();
	};

	// Boiler-plate.
	this.ajax = function(url, options, baseHostnames, deferred) {
		if (!baseHostnames) {
			baseHostnames = this.baseHostnames;
			// Go through, and insert the successful hostname at the front, if we succeeded.
			if (this.successfulHostname) {
				for (var i = 1; i < baseHostnames.length; ++i) {
					if (baseHostnames[i] == this.successfulHostname) {
						var swap = baseHostnames[0];
						baseHostnames[0] = baseHostnames[i];
						baseHostnames[i] = swap;
						break;
					}
				}
			}
		}
		if (!options)
			options = {};
		if (!options['dataType'])
			options['dataType'] = 'json';
		if (options['data'] && options['contentType'] == 'json' && typeof(options.data) == "object" && options['type'] && options['type'] != "GET")
			options['data'] = JSON.stringify(options.data);
		// We are careful with this. If contentType is anything other than text/plan, it'll trigger a pre-flight request. So always specify a content-type of text. The server will know what we mean.
		options['contentType'] = 'text/plain';
		if (!options['headers'])
			options['headers'] = {}
		if (baseHostnames[0] != "/")
			options['url'] = "//" + baseHostnames[0] + this.basePath + url;
		else
			options['url'] = this.basePath + url;

		if (!options['headers']['Content-Type'] && options['contentType']) {
			if (options['contentType'] == "json") {
				options['headers']['Content-Type'] = "application/json; charset=UTF-8";
			} else {
				options['headers']['Content-Type'] = options['contentType'];
			}
		}

		if (!deferred)
			deferred = this.Deferred();

		var spotAPI = this;
		var xhttp = new XMLHttpRequest();
		var internalDeferred = this.Deferred();
		xhttp.onreadystatechange = function() {
			if (this.readyState == 4) {
				if (this.status >= 200 && this.status <= 299) {
					var sessionId = xhttp.getResponseHeader("X-Session-Id");
					if (sessionId)
						spotAPI.sessionId(sessionId);
					if (options['dataType'] == "json") {
						var data = null;
						try {
							data = JSON.parse(this.responseText)
						}
						catch (e) {
							internalDeferred.reject(this, "" + e, e);
						}
						internalDeferred.resolve(data);
					}
					else
						internalDeferred.resolve(this.responseText);
				} else {
					internalDeferred.reject(this, this.responseText, null);
				}
			}
		};
		var url = options['url'];
		if ((options['type'] || "GET") == "GET") {
			if (url.indexOf("?") == url.length - 1)
				url += this.encodeUrlVars(options['data']);
			else if (url.indexOf("?") != -1)
				url += "&" + this.encodeUrlVars(options['data']);
			else
				url += "?" + this.encodeUrlVars(options['data']);
		}
		xhttp.open(options['type'] || "GET", url, options['async'] == null ? true : options['async']);
		var headers = options['headers'];
		if (this._sessionId)
			headers['X-Session-Id'] = this._sessionId;
		for (var i in headers) {
			if (headers.hasOwnProperty(i))
				xhttp.setRequestHeader(i, options['headers'][i]);
		}
		internalDeferred.done(function() {
			successfulHostname = baseHostnames[0];
			deferred.resolve.apply(this, arguments);
		}).fail(function(xhr) {
			if (xhr.status >= 500) {
                                if (baseHostnames.length > 0) {
					baseHostnames.shift();
					spotAPI.ajax(url, options, baseHostnames, deferred);
                                } else {
					deferred.reject.apply(this, arguments);
                                }
			} else {
				deferred.reject.apply(this, arguments);
				successfulHostname = baseHostnames[0];
			}
		});

		xhttp.send(options['data']);

		return deferred;
	};

	this.map = function(items, func) {
		return Array.prototype.slice.call(items).map(func);
	};
	this.flatMap = function(items, func) {
		return Array.prototype.slice.call(items).flatMap(func);
	};
	this.forEach = function(items, func) {
		return Array.prototype.slice.call(items).forEach(func);
	};
	this.grep = function(items, func) {
		return Array.prototype.slice.call(items).filter(func);
	};
	// ============== END INTERNALS ==============


	// If false, will throw out errors as alerts. If true, will silently fail. Default is true.
	this.silenceErrors = function(silence) {
		this._silenceErrors = silence;
	};

	// This can be used to set the default query that comes out when you call .s() to start a query.
	this.defaultQuery = function(query) {
		if (arguments.length == 1)
			this._defaultQuery = this.clone(query);
		return this._defaultQuery;
	};


	// QUERY INTERFACE.
	function generateQualitativeQuery(field, operator, text) {
		if (!operator)
			operator = "==";
		var hash = {};
		if (field === 'available') {
			hash[operator] = text;
		} else {
			hash[field] = { };
			hash[field][operator] = text;
		}
		return new queryObject(hash);
	}
	function generateQuantitativeQuery(field, operator, text) {
		if (!operator)
			operator = "==";
		var hash = {};
		hash[field] = { };
		hash[field][operator] = text;
		return new queryObject(hash);
	}

	var queryObject = function(query, attributes) {
		var thisQuery = this;
		if (query && typeof(query) == "object" && !Array.isArray(query))
			query = [query];
		this.innerQuery = query || [];
		this.innerAttributes = attributes || {};
		this.innerJSONP = 0;
		this.innerHostname = null;
		this.innerMethod = "GET";
		this.innerCache = null;
		this.innerSessionId = null;

		// Used for analytics.
		this.category = function(category) {
			return this.merge(new queryObject(null, { category: category }));
		};

		// Internal.
		this.merge = function(otherQuery) {
			if (otherQuery.innerQuery)
				this.innerQuery = this.innerQuery.concat(otherQuery.innerQuery);
			for (var i in otherQuery.innerAttributes) {
				//if (this.innerAttributes[i] && !spotAPI._silenceErrors)
				//	alert("Query is overwriting " + i + " during merge.");
				this.innerAttributes[i] = otherQuery.innerAttributes[i];
			}
			this.innerJSONP = this.innerJSONP || otherQuery.innerJSONP;
			this.innerHostname = this.innerHostname || otherQuery.innerHostname;
			if (otherQuery.innerCache != null)
				this.innerCache = otherQuery.innerCache;
			return this;
		};

		var quantitativeFields = ["id", "price", "grams", "compare_at_price", "inventory_quantity", "priority"];
		var qualitativeFields = ["title", "option1", "option2", "option3", "product_type", "vendor", "tag", "tags", "sku"];
		var booleanFields = ["available"];

		// Performs a free-form text search; triggers spellcheck if autoCorrect or spellCheck has been set to true.
		this.search = function(text) {
			return this.merge(new queryObject(null, { search: { query: text } }));
		};

		// Puts this query into "collection mode", changing the query slightly. Will make it so that it'll only return products in that collection.
		// If given no sort order, will default the collection's default sort order. Can also give the extra, special sort order, "manual" to use
		// the collection's manual sorting, if present.
		this.collection = function(collection_handle) {
			return this.merge(new queryObject(null, { collection: collection_handle }));
		};

		// Acts as a filter; doesn't do anything with sort orders; simply helps pare down the resultset to things in a particular collection.
		// Allows you to do cool things, like collection unions/intersections.
                this.in_collection = function(operator, handle) {
                        if (arguments.length == 1) {
                                amount = operator;
                                operator = "==";
                        }
                        return this.merge(generateQualitativeQuery("collection", operator, amount));
                };


		this.isEmptyQuery = function() {
			return this.innerQuery.length == 0;
		};

		this.hostname = function(hostname) {
			this.innerHostname = hostname;
			return this;
		}
		this.cache = function(cache) {
			this.innerCache = cache;
			return this;
		}
		this.popular = function(popular) {
			this.innerPopular = popular;
			return this;
		};

		// operators are <, >, ==, !=, >=, <=, in, not_in. == is assumed by default.
		for (var i = 0; i < quantitativeFields.length; ++i) {
			var closure = function(i) {
				return function(operator, amount) {
					if (!spotAPI._silenceErrors && arguments.length < 1)
						alert("Requires an operator and operand, or just an operand if assuming ==.");
                                        if (arguments.length == 1) {
                                            operator = "==";
                                            operand = operator;
                                        }
					return this.merge(generateQuantitativeQuery(quantitativeFields[i], operator, amount));
				};
			};
			this[quantitativeFields[i]] = closure(i);
		}
		// operators are  ==, !=, in, =~, ^, $, sub, in, not_in
		for (var i = 0; i < qualitativeFields.length; ++i) {
			var closure = function(i) {
				return function(operator, amount) {
					if (arguments.length == 1) {
						amount = operator;
						operator = "==";
					}
					return this.merge(generateQualitativeQuery(qualitativeFields[i], operator, amount));
				};
			}
			this[qualitativeFields[i]] = closure(i);
		}

		// operators are ==, !=
		for (var i = 0; i < booleanFields.length; ++i) {
			var closure = function(i) {
				return function(operator, bool) {
					if (!spotAPI._silenceErrors && arguments.length < 1)
						alert("Requires an operator and operand, or just an operand if assuming ==.");
                                        if (arguments.length == 1) {
                                            bool = operator;
                                            operator = "==";
                                        }
                                        if (booleanFields[i] === 'available') {
																					operator = "or";
																					bool = [{"available": {"==":true}},
																									{"tag":				{"==":"badge::Coming-Soon"}}
																									]
																				}

					return this.merge(generateQualitativeQuery(booleanFields[i], operator, bool));
				};
			};
			this[booleanFields[i]] = closure(i);
		}

		this.option = function(name, operator, operand) {
			var hash = {};
			hash[name] = {};
			if (arguments.length == 2) {
				operand = operator;
				operator = "==";
			}
			if (!operator)
				operator = "==";
			hash[name][operator] = operand;
			return this.merge(new queryObject({ option: hash }));
		};

		this.product_metafield = function(namespace, key, operator, operand) {
			var hash = {};
			hash[namespace] = {};
                        hash[namespace][key] = {};
			if (arguments.length == 3) {
				operand = operator;
				operator = "==";
			}
			if (!operator)
				operator = "==";
			hash[namespace][key][operator] = operand;
			return this.merge(new queryObject({ "product-metafield": hash }));
		};

		this.variant_metafield = function(namespace, key, operator, operand) {
			var hash = {};
			hash[namespace] = {};
                        hash[namespace][key] = {};
			if (arguments.length == 3) {
				operand = operator;
				operator = "==";
			}
			if (!operator)
				operator = "==";
			hash[namespace][key][operator] = operand;
			return this.merge(new queryObject({ "variant-metafield": hash }));
		};

		// This is used internally by the faceting system; in terms of filtering, this is a no-op.
		this.facets = function() {
			if (arguments.length >= 1 && !Array.isArray(arguments[0])) {
				if (arguments.length > 1)
					return this.merge(new queryObject({ "facets": spotAPI.map(arguments, function (e) { return e.innerQuery; }) }));
				else
					return this.merge(new queryObject({ "facets": arguments[0].innerQuery }));
			}
			else if (Array.isArray(arguments[0])) {
				return this.facets.apply(this, arguments[0]);
			}
			return this;
		};

		// Possibly some more optimizations can take place here.
		this.or = function() {
			if (arguments.length >= 1 && !Array.isArray(arguments[0])) {
				if (arguments.length > 1)
					return this.merge(new queryObject({ "or": spotAPI.map(arguments, function (e) { return e.innerQuery; }).flat() }));
				else
					return this.merge(new queryObject({ "or": arguments[0].innerQuery }));
			}
			else if (Array.isArray(arguments[0])) {
				return this.or.apply(this, arguments[0]);
			}
			else if (arguments.length == 0) { // Now that we're specifying facets as part of the query sometimes we need to pass in empty ors instead of ignoring empty facet groups
				return this.merge(new queryObject({ "or": spotAPI.map(arguments, function (e) { return e.innerQuery; }) }));
			}
			return this;
		};

		this.and = function() {
			if (arguments.length >= 1) {
				if (arguments.length > 1)
					return this.merge(new queryObject({ "and": spotAPI.map(arguments, function (e) { return e.innerQuery; }) }));
				else
					return this.merge(arguments[0]);
			}
			else if (Array.isArray(arguments[0]))
				return this.and.apply(this, arguments[0]);
			return this;
		};


                // Makes it so that this subquery must match EVERY variant instead of at >= 1 variant.
                this.all = function() {
                        return this.merge(new queryObject({ "all": spotAPI.map(arguments, function (e) { return e.innerQuery; }) }));
		};

                // Valid fields for sorting are: "bestselling", "title", "price", "created", "priority".
		this.sort = function(field) {
			if (typeof(field) == "string") {
				var groups = /(\w+)\-(ascending|descending)/.exec(field);
				if (groups && groups.length > 1) {
					if (groups[2] == "descending") {
						field = { desc: groups[1] };
					} else {
						field = { asc: groups[1] };
					}
				}
			}
			return this.merge(new queryObject(null, { sort: field }));
		};

		// Valid values here are "none", "auto", or an explicitly created split in the admin panel.
		// "none" will return an unsplit resultset; "auto" will return a split if it's appropriate based
		// on which facets have been selected, and an explicit split will always return that split.
		this.split = function(splitHandle) {
			return this.merge(new queryObject(null, { split: splitHandle }));
		};

		// If set to true, will return the total count of objects in the resultset.
		// You can also set whether you want to get an exact count, or an approximate count.
		// Be warned, that setting an exact count can significantly slow down your search times,
		// as well as drive up your costs, as the server may have to do significantly more work
		// to determine the exact size of a resultset.
		this.count = function(field, behavior) {
			if (behavior)
				return this.merge(new queryObject(null, { count: field, countBehavior: behavior }));
			return this.merge(new queryObject(null, { count: field }));
		};

		// Sets how many results you'd like in your resulset. .rows and .limit are aliases of htis.
		this.paginate = function(pageAmount) {
			pageAmount = parseInt(pageAmount);
			if (pageAmount < 0 || pageAmount > 1000)
				alert("Must be a number between 0 and 1000");
			return this.merge(new queryObject(null, { rows: pageAmount }));
		};
                this.rows = this.paginate;
                this.limit = this.paginate;

                // Using this will skip over CORS headers for compatibility reasons; shouldn't really ever have to be used.
		this.jsonp = function(jsonp) {
			this.innerJSONP = jsonp;
			return this;
		}

		// Specifies which page you'd like; starts from 1; default 1.
		this.page = function(page) {
			return this.merge(new queryObject(null, { page: page }));
		};

		// Specifies which locale you'd like. If not specified, assumes the store's primary locale.
		this.locale = function(locale) {
			return this.merge(new queryObject(null, { locale: locale }));
		};

		// Specifies which currency you'd like. If not specified, assumes store default currency.
		// Note, this will *only* affect your input. (i.e. your desired price filters, and facets). It will
		// *not*, affect pricing. That should be converted after the fact to whatever is required.
		this.currency = function(currency) {
			return this.merge(new queryObject(null, { currency: currency }));
		};

		// If set to true, when a .search is made, will check to see if any results are found on the input string; and if not, will report
		// a better match in the "spellCheck" attribute of the returned object, based on a heuristic.
		this.spellCheck = function(spellCheck) {
			return this.merge(new queryObject(null, { spellCheck: spellCheck }));
		};

		// If set to true, when a .search is made, will check to see if any results are found on the input string; and if not, will select
		// a better match, as well as report this info in the "spellCheck" attribute based on a heuristic, and search on that instead.
		this.autoCorrect = function(autoCorrect) {
			return this.merge(new queryObject(null, { autoCorrect: autoCorrect }));
		};

		// This is kept in for legacy reasons; doesn't do anything.
		this.index = function(index) {
			return this.merge(new queryObject(null, { index: index }));
		};

		// By default this is true; if set to false will make it so that ONLY variants match the query will be returned, rather than the entire variantset of any matching product.
		this.allVariants = function(allVariants) {
			return this.merge(new queryObject(null, { allVariants: allVariants }));
		};


		// Options can be passed like this ["product_type", "vendor", {"option":"Color"}, {"option":"Size"}, {"price": ["-10","10-20","20-40","40+"]}, {"tag": ["tag1", "tag2"]}, {"tag":{"^":"beginwtithis:"}}]
		// Likewise, you can also specify if you want the counts to ignore conditions on their counts.
		// I.E. If you have a product type condition, and are looking at product type, setting ignoreOptionConditions
		// to true will cause the system to ignore that condition while calculating the counts.
		// Price will go from 0-10, 10-20, 30-40, 40+
		// Disjunctive, by default is true, and refers to whether or not the system should recalculate the resultset per faceting group, if relevant (i.e. if you've selected "Red" as a color, it shouldn't affect the other color options.
		// Behaviour, by default is "approximate".
		this.options = function(options, disjunctive, behaviour) {
			var hash = { options: options };
			if (behaviour != null)
				hash['optionBehavior'] = behaviour;
			if (disjunctive != null)
				hash['optionDisjunctive'] = disjunctive;
			return this.merge(new queryObject(null, hash));
		};

		// Can restrict fields in the following manner. ["id", "handle"]
		// Useful for reducing the amount of time spent downloading the search results, which can be quite a while.
		// Processing time will be longer, though.
		this.fields = function(fieldListing) {
			return this.merge(new queryObject(null, { fields: fieldListing }));
		}

		// Chooses whether or not to make a GET or a POST call. Sites with larger queries may have to set this to true.
		// Eventually you'll be able to supply "auto" here, which will be the default, which will have the system choose based
		// on payload length which to use.
		this.post = function(post) {
			if (post)
				this.innerMethod = "POST";
			else
				this.innerMethod = "GET";
		};

		// Convenience function to request 0 rows, and only get options.
		this.optionsOnly = function(doneFunc) {
			return this.paginate(0).end(doneFunc);
		}

		// When done constructing the query, this should be called to actually make the query; the result is contianed in the deferred.
		this.end = function(doneFunc) {
			var deferred = spotAPI.Deferred();
			if (doneFunc)
				deferred.done(doneFunc);

			if (this.innerPopular && this.innerAttributes.search)
				this.innerAttributes.search.popular_searches = this.innerPopular;
			var stringified = JSON.stringify({ query: this.innerQuery, attributes: this.innerAttributes });
			// If we're more than 8k, switch this to a spost, so that the server doesn't spew out a "header too large" error.
			var method = this.innerMethod != "GET" || (stringified.length >= 2*1024) ? "POST" : "GET";
			var hash = method == "POST" ? stringified : {"q": stringified };

			var queryHash = {};
			if (this.innerHostname)
				queryHash['hostname'] = this.innerHostname;
			else if(typeof esa_theme != 'undefined')
				queryHash['hostname'] = esa_theme.shop.permanent_domain
			else if(typeof theme != 'undefined')
				queryHash['hostname'] = theme.shop.permanent_domain
			if (this.innerCache)
				queryHash['cache'] = this.innerCache;
			var relevant = this.innerAttributes.allVariants && this.innerAttributes.allVariants == "flag";

			var query = spotAPI.map(Object.keys(queryHash), function(e) { return e + "=" + encodeURIComponent(queryHash[e]) }).join("&");;

			// Make this easier to flip over to POST.
			spotAPI.ajax((query || (!this.innerCache && this.innerCache != null) ? "?" + query : "") + (!this.innerCache && this.innerCache != null ? "cache=false" : ""), {
				type: method,
				dataType: (this.innerJSONP && method != "POST" ? "jsonp" : "json"),
				contentType: "application/json",
				data: hash
			}).done(function(results) {
				if (results['collections'])
					deferred.resolve((spotAPI._jsStyle ? spotAPI.map(results.collections, spotAPI.convertProduct ) : result.products), results.count, results);
				else
					deferred.resolve((spotAPI._jsStyle ? spotAPI.map(results.products, spotAPI.convertProduct ) : result.products), results.count, results);
			}).fail(function(xhr) {
				deferred.reject(xhr);
			});
			return deferred;
		};
		this.e = this.end;
	};

	this.start = function() {
		if (this._defaultQuery)
			return this.clone(this._defaultQuery);
		return new queryObject();
	};
	this.startEmpty = function() {
		return new queryObject();
	};
	this.s = function() {
		return this.start();
	};
	this.se = function() {
		return this.startEmpty();
	};

	this.spellCheck = function(phrase) {
		var deferred = spotAPI.Deferred();
		spotAPI.ajax("/spell-check", {
			type: "GET",
			dataType: "json",
			contentType: "json",
			data: { "phrase": phrase }
		}).done(function(result) {
			deferred.resolve(result.words, result.original, result.corrected);
		}).fail(function() {
			deferred.reject();
		});
	}

	this.getFeaturedImage = function(product) {
		if (product.images && product.images.length > 0) {
			var extantImages = {};
			var extantVariants = {};
			for (var i = 0; i < product.images.length; ++i)
				extantImages[product.images[i].id] = product.images[i];
			var relevant = false;
			for (var i = 0; i < product.variants.length; ++i) {
				if (product.variants[i].relevant)
					relevant = true;
			}

			for (var i = 0; i < product.variants.length; ++i) {
				if (!relevant || product.variants[i].relevant)
					extantVariants[product.variants[i].id] = product.variants[i];
			}
			var validFeatured = false;
			var presumablyAllVariants = true;
			for (var i = 0; i < product.images[0].variant_ids.length; ++i) {
				if (!extantVariants[product.images[0].variant_ids[i]]) {
					presumablyAllVariants = false;
				} else {
					validFeatured = true;
				}
			}
			if (!validFeatured) {
				for (var i = 0; i < product.variants.length; ++i) {
					if ((!relevant || product.variants[i].relevant) && extantImages[product.variants[i].image_id])
						return extantImages[product.variants[i].image_id];
				}
			}
			return product.images[0];
		}
		return null;
	};

	// Converts the incoming product to .js format from .json format.
	this.convertProduct = function(product) {

		let strip_image = function(img) {
			return !img.src ? null : img.src.split(":")[1];
		}

		let feproduct = {};
		feproduct['id'] = product.id ? product.id : product.variant[0].product_id ;
		feproduct['title'] = product.title;
		feproduct['handle'] = product.handle;
		feproduct['description'] = product.body_html;
		feproduct['published_at'] = product.published_at;
		feproduct['created_at'] = product.created_at;
		feproduct['vendor'] = product.vendor;
		feproduct['type'] = product.product_type;
		if (product.tags)
			feproduct['tags'] = product.tags.split(", ");
		feproduct['images'] = product.images ? spotAPI.map(product.images, function (e) { return { src: e.src, alt: e.alt }}) : [];
		feproduct['featured_image'] = spotAPI.getFeaturedImage(product);
		if (feproduct['featured_image'])
			feproduct['featured_image'] = feproduct['featured_image'].src;
		feproduct['options'] = null;
		if (product.images.length > 0)
			feproduct['url'] = product.handle ? ("/products/" + product.handle + (feproduct['featured_image'] != product.images[0].src ? "?variant=" + product.variants[0].id  : "")) : null;
		else
			feproduct['url'] = product.handle ? "/products/" + product.handle : null;
		feproduct['available'] = false;
		feproduct['price_min'] = null;
		feproduct['price_max'] = null;
		feproduct['compare_at_price_min'] = null;
		feproduct['compare_at_price_max'] = null;
		if (product.metafields) {
			feproduct['metafields'] = {};
			for(var i = 0; i < product.metafields.length; ++i){
				var metafield = product.metafields[i];
				if (!feproduct['metafields'][metafield.namespace])
					feproduct['metafields'][metafield.namespace] = {}
				feproduct['metafields'][metafield.namespace][metafield.key] = metafield.value;
			}
		}

		if (product.options) {
			feproduct['options'] = [];
			for (o in product.options) {
				feproduct['options'].push({
					"name": product.options[o].name,
					"position": product.options[o].position,
					"values": product.options[o].values
				});
			}
		}

		let varimages = {};
		if (product.images) {
			for (var i = 0; i < product.images.length; ++i)
				varimages[product.images[i].id] = product.images[i];
		}


		if (product.variants) {
			let variants = [];
			feproduct['variants'] = [];
			for (v in product.variants) {
				convert_price = function(s) {
					if (s == null)
						return null;
					return Number(s.replace(".", ""));
				}

				var myoptions = [];
				if (product.variants[v].option1) {
					myoptions.push(product.variants[v].option1);
				}
				if (product.variants[v].option2) {
					myoptions.push(product.variants[v].option2);
				}
				if (product.variants[v].option3) {
					myoptions.push(product.variants[v].option3);
				}

				var vprice = convert_price(product.variants[v].price);
				var vcompare_at = convert_price(product.variants[v].compare_at_price);

				var imageid = null;
				if (product.variants[v].image_id) {
					imageid = varimages[product.variants[v].image_id];
				}

				if (feproduct['price_min'] == null || feproduct['price_min'] > vprice) {
					feproduct['price_min'] = vprice;
				}

				if (feproduct['price_max']== null || feproduct['price_max']< vprice) {
					feproduct['price_max']= vprice;
				}
				if (feproduct['compare_at_price_min'] == null || feproduct['compare_at_price_min'] > vcompare_at) {
					feproduct['compare_at_price_min'] = vcompare_at;
				}

				if (feproduct['compare_at_price_max']== null || feproduct['compare_at_price_max'] < vcompare_at) {
					feproduct['compare_at_price_max'] = vcompare_at;
				}
				if (product.variants[v].inventory_policy == "continue" || product.variants[v].inventory_quantity > 0) {
					feproduct['available'] = true;
				}

				var variant = {
					"id": product.variants[v].id,
					"title": product.variants[v].title,
					"option1": product.variants[v].option1,
					"option2": product.variants[v].option2,
					"option3": product.variants[v].option3,
					"sku": product.variants[v].sku,
					"requires_shipping": product.variants[v].requires_shipping,
					"taxable": product.variants[v].taxable,
					"featured_image": imageid,
					"available": true,
					"name": product.title + " - " + product.variants[v].title,
					"public_title": product.variants[v].title,
					"options": myoptions,
					"price": vprice,
					"weight": product.variants[v].weight,
					"compare_at_price": vcompare_at,
					"inventory_quantity": product.variants[v].inventory_quantity,
					"inventory_management": product.variants[v].inventory_management,
					"inventory_policy": product.variants[v].inventory_policy,
					"barcode": product.variants[v].barcode
				};

				if (product.variants[v].relevant)
					variant['relevant'] = product.variants[v].relevant;

				variants.push(variant);
			} //endfor variants
			feproduct['variants'] = variants;
		}

		feproduct['compare_at_price_varies'] = true;
		if (feproduct['compare_at_price_max'] == feproduct['compare_at_price_min']) {
			feproduct['compare_at_price_varies'] = false;
		}

		feproduct['price_varies'] = true;
		if (feproduct['price_min'] == feproduct['price_max']) {
			feproduct['price_varies'] = false;
		}

		feproduct['price'] = feproduct['price_min'];
		feproduct['compare_at_price'] = feproduct['compare_at_price_min'];

		return feproduct;
	};

};

/***

SpotDOM Documentation
=====================

## Overview

### What does this class do, and why is it different from SpotAPI?

SpotDOM is a class that facilitates cleanly keeping *state* on a website using Spot. Whereas SpotAPI is a low-level querying interace that simply assists in constructing ajax requests,
SpotDOM helps assemble DOM elements, and maintain the state of what the user's looking at as they navigate through a product catalog. It intelligently keeps track of things like
page, active facets, current search, current sort order, stuff like that. It also provides easy ways to hook into it, and then render whatever DOM elements you think are appropriate.

### What are its dependencies?

SpotDOM currently has no dependencies.

### How do I use it? Give me a quickstart!

SpotDOM primarily uses the publisher-observer pattern to provide a layered architecutre. Ideally, most Spot-enhanced websites will have a 3-layered JS implementation. The first layer,
is the SpotAPI layer. As described above, this is the low level API. Above it, SpotDOM will sit, providing state, and helper functions. Above that, your own JS implementation will be sitting.
Primarily, what your implementation will do is register callback handlers with SpotDOM, which, depending on what's changing, will let you then render whatever HTML you'd like to render, or
affect the browser in whatever way you want. By default, SpotDOM does *not* affect the DOM, or the user's browser in *any* way; all this is done through callback handlers.

Essentially you'll want to instantiate this object like so:

	// Filters Integration
	window.spotAPI = window.spotAPI || new SpotAPI({{ shop.metafields.esafilters.clusters | json }});
	// Useful if multiple domains/domain switches occur.
	spotAPI.defaultQuery(spotAPI.s().hostname("{{ shop.permanent_domain }}"));
	window.spotDOM = window.spotDOM || new SpotDOM(spotAPI);

Below that, you can provide a number of options to the system; the easiest way to get started, is to ask for a basic pane, with the facets based on options that have been set up by the Spot back-end control panel.

	var options = {
		{% if template contains 'search' %}
			sections: {% if shop.metafields.esafilters.search-faceting %}{{ shop.metafields.esafilters.search-faceting | json }}{% else %}{% if shop.metafields.esafilters.general-faceting %}{{ shop.metafields.esafilters.general-faceting | json  }}{% else %}[]{% endif %}{% endif %}
		{% else %}
			sections: {% if collection and collection.metafields.esafilters.collections-faceting %}{{ collection.metafields.esafilters.collections-faceting | json  }}{% else %}{% if shop.metafields.esafilters.general-faceting %}{{ shop.metafields.esafilters.general-faceting | json  }}{% else %}[]{% endif %}{% endif %}
		{% endif %}
	};

	var samplePane = new window.spotDefault.Pane();
	for (var i = 0; i < sections.length; ++i) {
		if (sections[i].active)
			window.spotDOM.addFacet(new spotDOM.Facet(sections[i])).init();
	}
	document.body.appendChild(samplePane.element);

This will pop a pane onto your page, with generic HTML. You should inspect this function in this file if you really want to create your own pane with fancy functionality; this function doesn't do any magic, and only
uses the normal callback interface.

Alternatively, if you want to just test with how things COULD be, you can simply populates the facets with something static:

	options = {
		sections: [{"type":"tag","name":"Gender",values:[{"name":"Men","value":"Man"},{"name":"Women","value":"Woman"}]}, "Product Type", "Size",{"type":"vendor","name":"Vendor"},{"type":"price","name":"Price","values":[{"name":"Under 10$","value":"-10"},{"name":"10-20 $","value":"10-20"},{"name":"20-30 $","value":"20-30"},{"name":"30-40 $","value":"30-40"},{"name":"40-50 $","value":"40-50"},{"name":"50-60 $","value":"50-60"},{"name":"Over 60$","value":"60+"}]}]
	};

You also need to tell Spot how you'd want to render your products, so you'll want to register the 'products' callback handler. Here's a sample implementation.

	window.spotDOM.products(function(products) {
		$('.productgrid--items').empty();
		for (var i = 0; i < products.length; ++i) {
			var product = products[i];
			var productElement = $("<article class='productgrid--item imagestyle--natural productitem--emphasis' data-product-item tabindex='1'>\
			<div class='productitem' data-product-item-content>\
				<a class='productitem--image-link' href='" + product.url + "'>\
				<figure class='productitem--image'>\
				" + (product.featured_image ? "<img src='" + this.getSizedImage(product.featured_image, "275x275") + "'/>" : '<svg class="placeholder--image" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 525.5 525.5"><path d="M324.5 212.7H203c-1.6 0-2.8 1.3-2.8 2.8V308c0 1.6 1.3 2.8 2.8 2.8h121.6c1.6 0 2.8-1.3 2.8-2.8v-92.5c0-1.6-1.3-2.8-2.9-2.8zm1.1 95.3c0 .6-.5 1.1-1.1 1.1H203c-.6 0-1.1-.5-1.1-1.1v-92.5c0-.6.5-1.1 1.1-1.1h121.6c.6 0 1.1.5 1.1 1.1V308z"></path><path d="M210.4 299.5H240v.1s.1 0 .2-.1h75.2v-76.2h-105v76.2zm1.8-7.2l20-20c1.6-1.6 3.8-2.5 6.1-2.5s4.5.9 6.1 2.5l1.5 1.5 16.8 16.8c-12.9 3.3-20.7 6.3-22.8 7.2h-27.7v-5.5zm101.5-10.1c-20.1 1.7-36.7 4.8-49.1 7.9l-16.9-16.9 26.3-26.3c1.6-1.6 3.8-2.5 6.1-2.5s4.5.9 6.1 2.5l27.5 27.5v7.8zm-68.9 15.5c9.7-3.5 33.9-10.9 68.9-13.8v13.8h-68.9zm68.9-72.7v46.8l-26.2-26.2c-1.9-1.9-4.5-3-7.3-3s-5.4 1.1-7.3 3l-26.3 26.3-.9-.9c-1.9-1.9-4.5-3-7.3-3s-5.4 1.1-7.3 3l-18.8 18.8V225h101.4z"></path><path d="M232.8 254c4.6 0 8.3-3.7 8.3-8.3s-3.7-8.3-8.3-8.3-8.3 3.7-8.3 8.3 3.7 8.3 8.3 8.3zm0-14.9c3.6 0 6.6 2.9 6.6 6.6s-2.9 6.6-6.6 6.6-6.6-2.9-6.6-6.6 3-6.6 6.6-6.6z"></path></svg>') + "\
				</figure>\
			</a>\
			<div class='productitem--info'>\
				<div class='productitem--price'>\
				<div class='price--compare-at visible' data-price-compare-at=''>\
				<span class='price--spacer'></span>\
				</div>\
				<div class='price--main' data-price=''>\
				<span class='money' data-currency-usd='" + this.formatMoney(product.price/100) + "' data-currency='USD'>" + this.formatMoney(product.price/100) + "</span>\
				</div>\
				</div>\
				<h2 class='productitem--title'>\
				<a href='" + product.url + "' tabindex='1'>\
				" + (options['split'] && options['split'] == "none" ? product.title : this.getProductTitle(product)) + "\
				</a>\
				</h2>\
				<div class='productitem--description'>\
				<p>" + product.description + "</p>\
				<a href='" + product.url + "' class='productitem--link'>\
				View full details\
				</a>\
				</div>\
			</div>\
			</div>\
			</article>");
			$('.productgrid--items').append(productElement);
		}
	})

This function is called whenever a query is made. As you can see from the sample above, an array of products is given, and then turned into HTML. Speaking of, if you're using paging as your pagination mechanism, you'll
probably want change the pagination listing whenever query count/page changes. For that, you simply hook whenver the count is refreshed, and redraw pagination.

	window.spotDOM.count(function(count) {
		var elements = this.pagedPaginationHelper({ count: count });
		$('.pagination--inner').empty();
		for (var i = 0; i < elements.length; ++i)
			$('.pagination--inner').append(elements[i]);
	})

You can also write out a "we're searching for X" if you're using spellcheck; this can be rendered like so: (TODO: this regex shouldn't be necessary).

	window.spotDOM.spellCheck(function(spellCheck){
		if (spellCheck.original.replace(/(^\s+|\s+$|[^A-Za-z0-9]+)/g, "").toLowerCase() != spellCheck.corrected.replace(/(^\s+|\s+$|[^A-Za-z0-9]+)/g, "").toLowerCase()) {
			$('#spelling-correction').show();
			$('#spelling-correction').html("<p class='corrected'>Showing results for <a href='/search?q=" + encodeURIComponent(spellCheck.corrected) + "'>" + $.map(spellCheck.words, function(e) { if (e.corrected != e.original) { return "<b>" + e.corrected + "</b>"; } return e.corrected; }).join(" ") + "</a>.</p>");
		} else {
			$('#spelling-correction').hide();
		}
	})

You'll also probably want to keep the query string up to date so that it can be interpreted by the system. If you want to do that, all you need to do is the following:

	window.spotDOM.query(function() {
		window.spotDOM.updateQueryString();
	});

That will update the query string for you. Most implementation of Spot will also generally want to make a new query every time any propertry of the search changes, like sort, page, etc... In those cases, all you need
to do is do something like the following:

	window.spotDOM.sort(function(sortOrder) {
		this.query();
	}).collection(function(collection) {
		this.query();
	}).search(function(search) {
		this.query();
	}).split(function(split) {
		this.query();
	}).page(function(page) {
		this.query();
	}).paginate(function(resultsPerPage) {
		this.query();
	});

This will make it so whenever any property changes, we automatically re-perform a query. The automatic panel does this automatically by default, whenever you click a facet. However, if you wanted to make an "apply" button
where things would only apply when you clicked that button; you'd simply remove these declarations, and in your pane, also remove any handler that calls .query(), and you'd suddenly have a panel/page that only queries
when *you* want it to.

There are probably also a few other things you want to do, like hook up a select dropdown that handles sort order (can easily be done by something like):

	var sortDropdown = $('#product_grid_sort');
	sortDropdown.empty();
	window.spotDOM.addSortOrder(function(sortOrder) {
		sortDropdown.append("<option id='" + sortOrder.getUniqueId() + "' value='" + sortOrder.getUniqueId() + "'>" + sortOrder.getName() + "</option>");
		if (sortDropdown.find("option").length > 0)
			sortDropdown.show();
	}).removeSortOrder(function(sortOrder) {
		$('#' + sortOrder.getUniqueId()).remove();
		if (sortDropdown.find("option").length == 0)
			sortDropdown.hide();
	});
	sortDropdown.unbind('change');
	sortDropdown.change(function() { window.spotDOM.sort($(this).val() == "best" ? "bestselling" : $(this).val()); });
	window.spotDOM.initDefaultSortOrders();
	window.spotDOM.sort(sortDropdown.val(), false);

You can see a more exhaustive list of individual methods below.

**/

var SpotDOM = SpotDOM || function(spotAPI) {
	var spotDOM = this;
	this.spotAPI = spotAPI;
	if (!spotAPI)
		throw "Requires SpotAPI. Please pass a copy of the API to SpotDOM's constructor.";
	this.log = function(message) { if (console != undefined) console.log(message); };
	this.map = spotAPI.map;
	this.grep = spotAPI.grep;

	this.getStorage = function(key) {
		var item = sessionStorage.getItem(key)
		if (!item)
			return null;
		var json = JSON.parse(sessionStorage.getItem(key));
		if (!json['expiry'] || json['expiry'] > new Date().getTime())
			return json['value'];
		return null;
	};
	this.setStorage = function(key, value, expiry) {
		var json = { "value": value };
		if (expiry)
			json['expiry'] = (new Date().getTime())+(expiry*1000);
		sessionStorage.setItem(key, JSON.stringify(json));
	};

	this.getUrlVars = function() {
		var vars = {}, hash;
		var href = window.location.href;
		href = href.replace(/#.*?$/, '');
		if (href.indexOf('?') == -1 || href.indexOf('?') == href.length-1)
			return {};
		var hashes = href.slice(window.location.href.indexOf('?') + 1).split('&');
		for(var i = 0; i < hashes.length; i++) {
			hash = hashes[i].split('=');
			if (vars[hash[0]] == null)
				vars[hash[0]] = decodeURIComponent(hash[1] != null ? hash[1].replace(/\+/g, ' ') : '');
			else {
				if(typeof vars[hash[0]] == "string")
					vars[hash[0]] = [vars[hash[0]]];
				vars[hash[0]].push(decodeURIComponent(hash[1] != null ? hash[1].replace(/\+/g, ' ') : ''));
			}

		}
		return vars;
	};

	this.encodeUrlVars = function(hash) { return "?" + spotAPI.encodeUrlVars(hash); }


	this.defaultDiacriticsRemovalMap = [
		{'base':'a', 'letters':'\u0061\u24D0\uFF41\u1E9A\u00E0\u00E1\u00E2\u1EA7\u1EA5\u1EAB\u1EA9\u00E3\u0101\u0103\u1EB1\u1EAF\u1EB5\u1EB3\u0227\u01E1\u00E4\u01DF\u1EA3\u00E5\u01FB\u01CE\u0201\u0203\u1EA1\u1EAD\u1EB7\u1E01\u0105\u2C65\u0250'},
		{'base':'aa','letters':'\uA733'},
		{'base':'ae','letters':'\u00E6\u01FD\u01E3'},
		{'base':'ao','letters':'\uA735'},
		{'base':'au','letters':'\uA737'},
		{'base':'av','letters':'\uA739\uA73B'},
		{'base':'ay','letters':'\uA73D'},
		{'base':'b', 'letters':'\u0062\u24D1\uFF42\u1E03\u1E05\u1E07\u0180\u0183\u0253'},
		{'base':'c', 'letters':'\u0063\u24D2\uFF43\u0107\u0109\u010B\u010D\u00E7\u1E09\u0188\u023C\uA73F\u2184'},
		{'base':'d', 'letters':'\u0064\u24D3\uFF44\u1E0B\u010F\u1E0D\u1E11\u1E13\u1E0F\u0111\u018C\u0256\u0257\uA77A'},
		{'base':'dz','letters':'\u01F3\u01C6'},
		{'base':'e', 'letters':'\u0065\u24D4\uFF45\u00E8\u00E9\u00EA\u1EC1\u1EBF\u1EC5\u1EC3\u1EBD\u0113\u1E15\u1E17\u0115\u0117\u00EB\u1EBB\u011B\u0205\u0207\u1EB9\u1EC7\u0229\u1E1D\u0119\u1E19\u1E1B\u0247\u025B\u01DD'},
		{'base':'f', 'letters':'\u0066\u24D5\uFF46\u1E1F\u0192\uA77C'},
		{'base':'g', 'letters':'\u0067\u24D6\uFF47\u01F5\u011D\u1E21\u011F\u0121\u01E7\u0123\u01E5\u0260\uA7A1\u1D79\uA77F'},
		{'base':'h', 'letters':'\u0068\u24D7\uFF48\u0125\u1E23\u1E27\u021F\u1E25\u1E29\u1E2B\u1E96\u0127\u2C68\u2C76\u0265'},
		{'base':'hv','letters':'\u0195'},
		{'base':'i', 'letters':'\u0069\u24D8\uFF49\u00EC\u00ED\u00EE\u0129\u012B\u012D\u00EF\u1E2F\u1EC9\u01D0\u0209\u020B\u1ECB\u012F\u1E2D\u0268\u0131'},
		{'base':'j', 'letters':'\u006A\u24D9\uFF4A\u0135\u01F0\u0249'},
		{'base':'k', 'letters':'\u006B\u24DA\uFF4B\u1E31\u01E9\u1E33\u0137\u1E35\u0199\u2C6A\uA741\uA743\uA745\uA7A3'},
		{'base':'l', 'letters':'\u006C\u24DB\uFF4C\u0140\u013A\u013E\u1E37\u1E39\u013C\u1E3D\u1E3B\u017F\u0142\u019A\u026B\u2C61\uA749\uA781\uA747'},
		{'base':'lj','letters':'\u01C9'},
		{'base':'m', 'letters':'\u006D\u24DC\uFF4D\u1E3F\u1E41\u1E43\u0271\u026F'},
		{'base':'n', 'letters':'\u006E\u24DD\uFF4E\u01F9\u0144\u00F1\u1E45\u0148\u1E47\u0146\u1E4B\u1E49\u019E\u0272\u0149\uA791\uA7A5'},
		{'base':'nj','letters':'\u01CC'},
		{'base':'o', 'letters':'\u006F\u24DE\uFF4F\u00F2\u00F3\u00F4\u1ED3\u1ED1\u1ED7\u1ED5\u00F5\u1E4D\u022D\u1E4F\u014D\u1E51\u1E53\u014F\u022F\u0231\u00F6\u022B\u1ECF\u0151\u01D2\u020D\u020F\u01A1\u1EDD\u1EDB\u1EE1\u1EDF\u1EE3\u1ECD\u1ED9\u01EB\u01ED\u00F8\u01FF\u0254\uA74B\uA74D\u0275'},
		{'base':'oi','letters':'\u01A3'},
		{'base':'ou','letters':'\u0223'},
		{'base':'oo','letters':'\uA74F'},
		{'base':'p','letters':'\u0070\u24DF\uFF50\u1E55\u1E57\u01A5\u1D7D\uA751\uA753\uA755'},
		{'base':'q','letters':'\u0071\u24E0\uFF51\u024B\uA757\uA759'},
		{'base':'r','letters':'\u0072\u24E1\uFF52\u0155\u1E59\u0159\u0211\u0213\u1E5B\u1E5D\u0157\u1E5F\u024D\u027D\uA75B\uA7A7\uA783'},
		{'base':'s','letters':'\u0073\u24E2\uFF53\u00DF\u015B\u1E65\u015D\u1E61\u0161\u1E67\u1E63\u1E69\u0219\u015F\u023F\uA7A9\uA785\u1E9B'},
		{'base':'t','letters':'\u0074\u24E3\uFF54\u1E6B\u1E97\u0165\u1E6D\u021B\u0163\u1E71\u1E6F\u0167\u01AD\u0288\u2C66\uA787'},
		{'base':'tz','letters':'\uA729'},
		{'base':'u','letters': '\u0075\u24E4\uFF55\u00F9\u00FA\u00FB\u0169\u1E79\u016B\u1E7B\u016D\u00FC\u01DC\u01D8\u01D6\u01DA\u1EE7\u016F\u0171\u01D4\u0215\u0217\u01B0\u1EEB\u1EE9\u1EEF\u1EED\u1EF1\u1EE5\u1E73\u0173\u1E77\u1E75\u0289'},
		{'base':'v','letters':'\u0076\u24E5\uFF56\u1E7D\u1E7F\u028B\uA75F\u028C'},
		{'base':'vy','letters':'\uA761'},
		{'base':'w','letters':'\u0077\u24E6\uFF57\u1E81\u1E83\u0175\u1E87\u1E85\u1E98\u1E89\u2C73'},
		{'base':'x','letters':'\u0078\u24E7\uFF58\u1E8B\u1E8D'},
		{'base':'y','letters':'\u0079\u24E8\uFF59\u1EF3\u00FD\u0177\u1EF9\u0233\u1E8F\u00FF\u1EF7\u1E99\u1EF5\u01B4\u024F\u1EFF'},
		{'base':'z','letters':'\u007A\u24E9\uFF5A\u017A\u1E91\u017C\u017E\u1E93\u1E95\u01B6\u0225\u0240\u2C6C\uA763'}
	];
	this.diacriticsMap = {};
	for (var i=0; i < this.defaultDiacriticsRemovalMap .length; i++) {
		var letters = this.defaultDiacriticsRemovalMap [i].letters;
		for (var j=0; j < letters.length ; j++){
			this.diacriticsMap[letters[j]] = this.defaultDiacriticsRemovalMap[i].base;
		}
	}
	this.removeDiacritics = function(str) {
		return str.replace(/[^\u0000-\u007E]/g, function(a){
			return spotDOM.diacriticsMap[a] || a;
		});
	};

	this.handleize = function(handle) {
		if (handle != null)
			return this.removeDiacritics(handle.toLowerCase()).replace(/\s+/g, '-').replace(/[^a-z0-9_]+/g, '-');
		return null;
	};

	this.scalarField = function(object, field, defaultValue, options) {
		object['_' + field + "Listeners"] = [];
		object[field] = function(variable, callHandlers) {
			if (typeof(variable) == 'function') {
				object['_' + field + "Listeners"].push(variable);
				if (callHandlers)
					variable.call(object, object['_' + field]);
				return object;
			} else {
				if (arguments.length > 0) {
					if (arguments.length == 1)
						callHandlers = true;
					if (options && options['preset'])
						variable = options['preset'].call(object, variable);
					object['_' + field] = variable;
					if (callHandlers) {
						for (var i = 0; i < object['_' + field + "Listeners"].length; ++i)
							object['_' + field + "Listeners"][i].call(object, variable);
					}
					return object;
				}
				return object['_' + field];
			}
		};
		object['_' + field] = (options && options['preset']) ? options['preset'].call(object, defaultValue) : defaultValue;
	};

	this.arrayField = function(object, field) {
		object['_' + field + "s"] = [];
		var methods = [
			"add" + field.charAt(0).toUpperCase() + field.slice(1),
			"add" + field.charAt(0).toUpperCase() + field.slice(1) + "s",
			"remove" + field.charAt(0).toUpperCase() + field.slice(1),
			"remove" + field.charAt(0).toUpperCase() + field.slice(1) + "s"
		];
		for (var idx in methods) {
			var method = methods[idx];
			object['_' + method + "Listeners"] = [];
			object[method] = (function(method, idx) { return function(variable, callListeners) {
				if (typeof(variable) == 'function') {
					object['_' + method + "Listeners"].push(variable);
					if (callListeners) {
						if (idx == 0) {
							for (var i = 0; i < object['_' + field + "s"].length; ++i)
								variable.call(object, object['_' + field + "s"][i]);
						} else
							variable.call(object, object['_' + field + "s"]);
					}
					return object;
				} else {
					if (arguments.length == 0)
						throw "Requires an argument.";
					if (idx == 0 || idx == 1) {
						if (idx == 1)
							object['_' + field + "s"] = object['_' + field + "s"].concat(variable);
						else
							object['_' + field + "s"].push(variable);
					} else {
						if (idx == 2)
							object['_' + field + "s"] = object['_' + field + "s"].filter(function(e) { return e != variable; });
						else {
							object['_' + field + "s"] = object['_' + field + "s"].filter(function(e) { return !variable.includes(e); });
						}
					}
					for (var i = 0; i < object['_' + method + "Listeners"].length; ++i)
						object['_' + method + "Listeners"][i].call(object, variable);
					if (idx == 1 || idx == 3) {
						var targetedMethod = method.slice(0, method.length - 1);
						for (var i = 0; i < object['_' + targetedMethod + "Listeners"].length; ++i) {
							for (var j = 0; j < variable.length; ++j)
								object['_' + targetedMethod + "Listeners"][i].call(object, variable[j]);
						}
					} else {
						var targetedMethod = method + "s";
						for (var i = 0; i < object['_' + targetedMethod + "Listeners"].length; ++i)
							object['_' + targetedMethod + "Listeners"][i].call(object, [variable]);
					}
					return variable;
				}
			} })(method, idx);
		}
		object['_' + field + "sListeners"] = [];
		object[field + "s"] = function(variable, callListeners) {
			if (typeof(variable) == 'function') {
				object['_' + field + "sListeners"].push(variable);
				if (callListeners)
					variable.call(object, object['_' + field + "s"]);
				return object;
			} else if (arguments.length > 0) {
				object['_' + field + "s"] = variable;
				if (callListeners) {
					for (var i = 0; i < object['_' + field + "sListeners"].length; ++i)
						object['_' + field + "sListeners"][i].call(object, variable);
				}
				return object;
			}
			return object['_' + field + "s"];
		};
	};

	this.listeners = function(object, method) {
		object['_' + method + "Listeners"] = [];
		object[method] = function(variable) {
			if (typeof(variable) == 'function') {
				object['_' + method + "Listeners"].push(variable);
				return object;
			} else {
				var results = [];
				for (var i = 0; i < object['_' + method + "Listeners"].length; ++i)
					results.push(object['_' + method + "Listeners"][i].apply(object, arguments));
				return results;
			}
		};
	};
	this.listener = function(object, method, defaultFunction) {
		object['_' + method + "Listener"] = defaultFunction;
		object[method] = function(variable) {
			if (typeof(variable) == 'function') {
				object['_' + method + "Listener"] = variable;
				return object;
			} else if (object['_' + method + "Listener"]) {
				return object['_' + method + "Listener"].apply(object, arguments);
			}
		};
	};


	this["_queryListeners"] = [];
	this["_queryWaiter"] = null;
	this["_queryRequests"] = 0;

	this.query = function(callback) {
		if (callback && typeof(callback) == 'function') {
			this['_' + "queryListeners"].push(callback);
			return this;
		}

		// Set debug mode to true, if we make a query with the debugger open, and debugMode is not explicitly set to false.
		if (this.debugMode() == null && this.isDebuggerOpen())
			this.debugMode(true);

		var hasWaiter = this["_queryWaiter"] != null;

		++this["_queryRequests"];
		if (this["_queryWaiter"])
			return this["_queryWaiter"];
		this["_queryWaiter"] = spotAPI.Deferred();

		this.beginQuery();
		var query = this.generateQuery();
		if (this.shopifyMode()) {
			// Examine the inner attributes here, and try to finagle a URL that'll work.
			var search;
			var collection = query.innerAttributes['collection'];
			var queryVars = {}
			if (query.innerAttributes['search']) {
				if (typeof(query.innerAttributes['search']) == "object") {
					search = query.innerAttributes['search']["query"];
				} else {
					search = query.innerAttributes['search'];
				}
			}
			if (query.innerAttributes['collection']) {
				collection = collection;
			}
			var tags = [];
			var vendor = null;
			var product_type = null;
			var page = query.innerAttributes['page'] || 1;
			var sort = query.innerAttributes['sort'];

			var url;
			var vars = spotDOM.generateQueryVariables();
			if (!collection) {
				url = "/search/"

				function fieldParse(type, field, value) {
					var actualValue;
					if (typeof(value) == "object") {
						var keys = Object.keys(value);
						if (keys.length > 0) {
							if (keys[0] != "==")
								return;
							actualValue = value[keys[0]];
						}
					} else {
						actualValue = value;
					}
					if (field == "product_type")
						return "product_type:\"" + actualValue + "\""
					if (field == "vendor")
						return "vendor:\"" + actualValue + "\""
					if (field == "tag" || field == "tags")
						return "tag:\"" + actualValue + "\""
					if (field == "sku")
						return "variants.sku:\"" + actualValue + "\""
					if (field == "barcode")
						return "variants.barcode:\"" + actualValue + "\""
				}

				function recursiveParse(type, query) {
					var results = [];
					for (var i = 0; i < query.length; ++i) {
						if (typeof(query[i]) == "object") {
							var keys = Object.keys(query[i]);
							if (keys.length > 0) {
								if (keys[0] == "and" || keys[0] == "or")
									results.push(spotDOM.grep(recursiveParse(keys[0], query[i][keys[0]]), function(e) { return e && e != ''; }).join(" " + keys[0].toUpperCase() + " "));
								else if (keys[0] == "facets")
									results.push(spotDOM.grep(recursiveParse("and", query[i][keys[0]]), function(e) { return e && e != ''; }).join(" AND "));
								else if (keys[0] == "product_type" || keys[0] == "vendor" || keys[0] == "tag" || keys[0] == "tags" || keys[0] == "sku" || keys[0] == "barcode")
									results.push(fieldParse(type, keys[0], query[i][keys[0]]));
							}
						}
					}
					return results;
				}
				var searchQuery = spotAPI.map(spotAPI.grep(recursiveParse("AND", query.innerQuery), function(e) { return e && e != ""; }), function(e) { return "(" + e + ")"; }).join(" AND ");
				// This is a double parentheses on purpose, to distinguish from legitimate search groupings.
				vars['q'] = (search ? search : "") + (searchQuery && searchQuery != "()" ? (search ? " AND (" : "(") + searchQuery + ")" : "");
			} else {
				url = "/collections/" + collection;


				function fieldParse(field, value) {
					var actualValue;
					if (typeof(value) == "object") {
						var keys = Object.keys(value);
						if (keys.length > 0) {
							if (keys[0] != "==")
								return;
							actualValue = value[keys[0]];
						}
					} else {
						actualValue = value;
					}
					/* if (field == "product_type")
						product_type = actualValue;
					if (field == "vendor")
						vendor = actualValue; */
					if (field == "tag" || field == "tags")
						tags.push(actualValue);
					return actualValue;
				}

				function recursiveParse(type, query) {
					for (var i = 0; i < query.length; ++i) {
						if (typeof(query[i]) == "object") {
							var keys = Object.keys(query[i]);
							if (keys.length > 0) {
								if (keys[0] == "and" || keys[0] == "or")
									recursiveParse(keys[0], query[i][keys[0]]);
								else if (keys[0] == "facets")
									recursiveParse("and", query[i][keys[0]]);
								else if (/*keys[0] == "product_type" || keys[0] == "vendor" || */keys[0] == "tag" || keys[0] == "tags") {
									if (fieldParse(keys[0], query[i][keys[0]]))
										break;
								}
							}
						}
					}
				}
				recursiveParse("AND", query.innerQuery);
				if (tags.length > 0)
					url += "/" + spotAPI.map(tags, function(e) { return encodeURI(spotDOM.handleize(e)) }).join("+");
				/* if (vendor)
					vars['vendor'] = vendor;
				if (product_type)
					vars['type'] = product_type;*/
			}

			if (this.sort() && this.sort().getShopifySortOrder())
				vars['sort_by'] = this.sort().getShopifySortOrder();

			var queryString = spotAPI.encodeUrlVars(vars);
			if (queryString.length > 0)
				queryString = "?" + queryString;
			var target = window.location.origin + url + queryString;
			if (window.location.href != target) {
				window.location = target;
				this.endQuery();
				return spotAPI.Deferred();
			} else {
				var deferred = this["_queryWaiter"];
				this["_queryWaiter"] = null;
				deferred.resolve();
				this.endQuery();
				return deferred;
			}
		} else {
			var deferred = spotAPI.Deferred();
			query.e().done(function(products, count, options) {
				if (count != null)
					spotDOM.count(count);
				if (options['options'])
					spotDOM.updateFacets(options['options']);
				spotDOM.products(products);
				if (options.spellCheck)
					spotDOM.spellCheck(options['spellCheck']);
				else
					spotDOM.spellCheck(null);
				if (options['warnings'] && options['warnings'].length > 0) {
					for (var i = 0; i < options['warnings'].length; ++i)
						console.log("Spot Warning: " + options['warnings'][i]);
					if (spotDOM.debugMode())
						deferred.reject({ status: 400, responseText: options['warnings'].join("\n") });
				}
				deferred.resolve(products, count, options);
			}).fail(function(xhr) {
				deferred.reject(xhr);
			});
			for (var i = 0; i < spotDOM["_queryListeners"].length; ++i) {
				var internal = i;
				deferred.done((function(internal) { return function() {
					spotDOM["_queryListeners"][internal].apply(spotDOM, arguments);
				} })(internal));
			}
			var queryWaiter = spotDOM["_queryWaiter"];
			deferred.done(function() {
				spotDOM.endQuery();
				var deferred = spotDOM["_queryWaiter"];
				spotDOM["_queryWaiter"] = null;
				spotDOM["_queryRequests"] = 0;
				deferred.resolve.apply(deferred, arguments);
			}).fail(function(xhr) {
				if (spotDOM.failBehavior() && spotDOM.failBehavior() == "shopify" && xhr.status != 400) {
					spotDOM.shopifyMode(true);
					spotDOM["_queryWaiter"] = null;
					spotDOM["_queryRequests"] = 0;
					spotDOM.query();
				} else {
					spotDOM["_queryWaiter"].reject();
				}
			});
			return queryWaiter;
		}
	};



	var vars = this.getUrlVars();



	/**

	## Sub-Classes

	The SpotDOM class uses several sub-classes to keep track of everything; the three primary things that are complex enough to warrant classes are [Facets](#facets), [Facet Values](#facet-values), and [Sort Orders](#sort-orders).

	### Facets

	Facets are objects that represent a particular category of values. As an example, a Facet would be "Color". A facet, when instantiated can take either a simple string, or an object describing what it represents. It can be in one of the following forms:

	#### new

		new Facet("product_type")

		new Facet("Color")

		new Facet({"option": "Color"})

		new Facet({"product-metafield": {"mynamespace":"mykey"}})

		new Facet({"type": "option", "option": "Color", "values": ["Red", "Blue"]})

		new Facet({"type": "option", "option": "Color", "values": [{"name": "Reddish", "value": "Red"},{"name": "Blueish", "value": "Blue"}]})

		new Facet({"type": "option", "option": "Color", "values": [{"name": "Reddish", "value": ["Red", "Magenta"]},{"name": "Blueish", "value": ["Blue", "Cyan"]}]})


	Once constructed, these are generally passed to `.addFacet` on the full DOM object, so that the system will keep track of which facets you'd like to use. After being added for the first time, `.init` should be called on the facet to initialize it. See the [SpotDefault](#spotdefault-documentation) for an example.

	**/
	this.Facet = function(spec) {
		var facet = this;
		this.spotDOM = spotDOM;

		var facetValueHash = {};
		spotDOM.arrayField(this, 'facetValue');

		this.getName = function() {
			if (typeof(spec) == 'object') {
				if (spec['name'])
					return spec['name'];
				return Object.keys(spec)[0];
			}
			return spec;
		};

		this.smartTypeConversion = function() {
			var lower;
			if (typeof(spec) == 'object') {
				if (spec['type']) {
					if (spec['type'] == "option")
						return { "option": spec['option'] };
					else if (spec['type'] == "product-metafield" || spec['type'] == "variant-metafield") {
						var hash = {};
						hash[spec['type']] = { }
						hash[spec['type']][spec['namespace']] = spec['key']
						return hash;
					}
					return spec['type'];
				}
				lower = this.getName().toLowerCase();
			} else
				lower = spec.toLowerCase();
			if (lower == 'type' || lower == 'product type' || lower == 'category')
				return "product_type";
			else if (lower == 'vendor' || lower == 'seller')
				return "vendor";
			else if (lower == 'price')
				return "price";
			else if (lower == 'compare_at_price')
				return "compare_at_price";
			else if (lower == 'grams')
				return "grams";
			else if (lower == 'inventory_quantity')
				return "inventory_quantity";
			else if (lower == 'tags')
				return "tags";
			else if (lower == 'option1' || lower == 'option2' || lower == 'option3')
				return lower;
			else {
				return {"option":spec};
			}
		};
		this.smartType = this.smartTypeConversion();
		if (typeof(this.smartType) == 'object')
			this.smartTypeName = this.smartType['name'] || this.smartType['option'] || this.smartType['key'];
		else
			this.smartTypeName = this.smartType;



		this.isQuantitative = function() { return typeof(this.smartType) != "object" && (this.smartType == 'price' || this.smartType == 'compare_at_price' || this.smartType == 'grams' || this.smartType == 'inventory_quantity') };
		this.isQualitative = function() { return !this.isQuantitative(); }
		this.isOption = function() { return typeof(this.smartType) == "object" && this.smartType['option']; }
		this.isMetafield = function() { return typeof(this.smartType) == "object" && (this.smartType['product-metafield'] || this.smartType['variant-metafield']); }
		this.isProductMetafield = function() { return typeof(this.smartType) == "object" && this.smartType['product-metafield']; }
		this.isVariantMetafield = function() { return typeof(this.smartType) == "object" && this.smartType['variant-metafield']; }
		this.getNamespace = function() { return typeof(this.smartType) == "object" && Object.keys((this.smartType['product-metafield'] || this.smartType['variant-metafield']))[0]; }
		this.getKey = function() { return typeof(this.smartType) == "object" && Object.values((this.smartType['product-metafield'] || this.smartType['variant-metafield']))[0]; }
		// Becuase Shopify doesn't support ORing things, normally.
		this.singleSelection = function() { return spotDOM.shopifyMode(); };

		// Specifies the sorting algorithm for the inner facetValues.
		this.sortValuesFunction = this.isQualitative() ? function(facetValueA, facetValueB) {
			return facetValueA.getName().localeCompare(facetValueB.getName());
		} : function(facetValueA, facetValueB) {
			var halvesA = facetValueA.value.split("-");
			var halvesB = facetValueB.value.split("-");
			return (parseFloat(halvesA[0] || 0) < parseFloat(halvesB[0] || 0));
		};

		this.getOptionValue = function() {
			if (typeof(spec) == 'object') {
				if (spec['values']) {
					var hash = {};
					if (this.isOption()) {
						hash['option'] = { };
						hash.option[this.smartTypeName] = spec['values'];
						return hash;
					} else if (this.isMetafield()) {
						hash['product-metafield'] = { };
						hash['product-metafield'][spec['namespace']] = { }
						hash['product-metafield'][spec['namespace']][spec['key']] = { }
						hash['product-metafield'][spec['namespace']][spec['key']] = spec['values'];
						return hash;
					}
					hash[this.smartTypeName] = spotDOM.map(spec['values'], function(e) {
						if (typeof(e) == "object") {
							if (e.name && facet.isQualitative())
								return e;
							return e.value;
						}
						return e;
					});
					return hash;
				}
			}
			return this.smartType;
		};

		this.getUniqueId = function() {
			if (this.getName())
				return "f_" + spotDOM.handleize(this.getName());
			return "f_" + this.smartTypeName;
		};

		this.composeQuery = function(query) {
			return query.or(spotDOM.map(this.facetValues(), function(e) { return e.composeQuery(spotDOM.spotAPI.se()); }).filter(function(e) { return !e.isEmptyQuery(); }));
		};


		this.composeQueryString = function(vars) {
			var array = this.facetValues().filter(function(e) { return e.enabled(); });
			if (array.length > 0)
				vars[this.getUniqueId()] = spotDOM.map(array, function(e) { return e.getName(); });
			return vars;
		};

		this.init = function(option) {
			var values = [];
			if (spec.values) {
				values = spotDOM.map(spec.values, function(e) {
					if (typeof(e) == "object")
						return { "value": e.value, enabled: false, name: e.name };
					return { "value": e, enabled: false, name: e };
				});
			}
			var vars = option || spotDOM.getUrlVars();
			var facetsSelected = Object.keys(vars).filter(function(e) { return /^f_/.test(e); })
			if (facetsSelected.includes(facet.getUniqueId()) ){
				var selectedValues = vars[facet.getUniqueId()];
				if (typeof selectedValues == "string")
					selectedValues = [selectedValues];
				var unhandledValues = [];
				if (values.length > 0) {
					for (var j = 0; j < selectedValues.length; ++j) {
						var hasOne = false;
						for (var i = 0; i < values.length; ++i ){
							if (values[i].name == selectedValues[j]) {
								values[i].enabled = true;
								hasOne = true;
							}
						}
						if (!hasOne)
							unhandledValues.push(selectedValues[j]);
					}
				} else {
					unhandledValues = selectedValues;
				}
				values = spotDOM.map(unhandledValues, function(e) { return { "value": e, enabled: true }; }).concat(values);
			}
			if (spotDOM.shopifyMode()) {
				if (
					values.length > 0 && (
						((spotDOM.isCollectionPage() || spotDOM.isSearchPage()) && (typeof(this.smartType) != "object" && (this.smartType == "tag" || this.smartType == "tags"))) ||
						((spotDOM.isVendorCollectionPage() || spotDOM.isSearchPage()) && (typeof(this.smartType) != "object" && this.smartType == "vendor")) ||
						((spotDOM.isProductTypeCollectionPage() || spotDOM.isSearchPage()) && (typeof(this.smartType) != "object" && this.smartType == "product_type"))
					)
				)
					this.update(values);
				else
					spotDOM.removeFacet(this);
			} else {
				this.update(values);
			}
		};

		this.update = function(option) {
			var values = [];
			var mergeValues = {};
			if (spec.values) {
				for (var i = 0; i < spec.values.length; ++i)
					mergeValues[spec.values[i].name] = spec.values[i].value;
			}
			if (option && !Array.isArray(option)) {
				var actualType = this.smartTypeName;
				if (actualType == "tags" && option["tag"])
					actualType = "tag";
				var keys = option[actualType] ? Object.keys(option[actualType]) : [];
				var currentOption = option[actualType] ? option[actualType] : {}
				if (keys.length == 0 && option["option"]) {
					// To handle those cases where we're case insensitive.
					keys = option["option"][actualType] ? Object.keys(option["option"][actualType]) : (option["option"][actualType.toLowerCase()] ? Object.keys(option["option"][actualType.toLowerCase()]) : []);
					currentOption = option["option"][actualType] ? option["option"][actualType] : (option["option"][actualType.toLowerCase()] ? option["option"][actualType.toLowerCase()] : {})
				}
				if (keys.length == 0 && (option["product-metafield"] || option["variant-metafield"])) {
					var key = option["product-metafield"] ? 'product-metafield' : 'variant-metafield';
					currentOption = Object.values(Object.values(option[key])[0])[0];
					keys = Object.keys(currentOption);
				}
				for (var i = 0; i < keys.length; ++i) {
					values.push({ "name": keys[i], "value": mergeValues[keys[i]] || keys[i], "count": currentOption[keys[i]] });
				}
			} else if (option) {
				values = option;
			}

			var extantFacets = {};
			for (var i = 0; i < values.length; ++i) {
				value = values[i];
				var facetValue = new spotDOM.FacetValue(this, value);
				var id = facetValue.getUniqueId();
				var newFacets = [];
				if (!facetValueHash[id]) {
					newFacets.push(facetValue);
				} else {
					facetValue = facetValueHash[id];
				}
				newFacets.sort(this.sortValuesFunction);
				for (var j = 0; j < newFacets.length; ++j)
					this.addFacetValue(newFacets[j]);
				extantFacets[id] = true;
				facetValue.update(value);
			}
			for (var i in facetValueHash) {
				if (!extantFacets[i])
					facetValueHash[i].count(0);
			}
			this.initValues = null;
		};

		this.addFacetValue(function(facetValue) {
			var id = facetValue.getUniqueId();
			facetValueHash[id] = facetValue;
			return facetValue;
		});

		this.removeFacetValue(function(facetValue) {
			var id = facetValue.getUniqueId();
			delete facetValueHash[id];
		});

		this.getFacetValue = function(id) {
			return facetValueHash[id];
		};
	};


	/**

	### FacetValues

	FacetValues are objects that represent an individual value that's part of a composing facet. An example here would be "Red", under the Facet "Color".

	FacetValues can be either "simple", or "composite". A simple FacetValue only comprises a single underlying value, whereas a composite FacetValue comprises more
	than one value, or a rule that describes a set of values. An example here would be "Red" simply referring to "Red" in the underlying catalog, vs. "Red" referring
	to a series of values like "Magenta", "Pink", "Red", etc.. in the underlying catalog.

	These are genereally not instantiated directly, but instead are instantiated as part of the values coming back from the filtering system. They do have a number of
	scalar variables, that, like the SpotDOM object as a whole, can be listened to by passing a function to them. Generally, these listeners are applied under an
	`addFacet` listener, which will trigger whenever a facet is added to the SpotDOM system. You can look at the [SpotDefault implementation](#spotdefault-documentation)
	for an example.

	**/
	this.FacetValue = function(facet, value) {
		this.facet = facet;
		this.name = value['name'];
		this.value = value['value'];

		/**

		### Variables

		#### count

		This variable is changed when a filtering resultset comes back with a number that represents how many products lie under this facet. Generally used
		to update a faceting pane.

		**/
		spotDOM.scalarField(this, 'count', value['count']);
		/**

		#### enabled

		Used to determine whether or not a particular facet value has been applied to the query. Generally used to check a checkbox, or provide some other
		indication to the user that the FacetValue is being used.

		**/
		spotDOM.scalarField(this, 'enabled', value['enabled'] != null ? value['enabled'] : false);
		/**


		### Getters

		#### getUniqueId

		Returns an ID that is unique to this facet. Usually is descriptive of the facet as well. Suitable for direct insertion into a query string, or
		for use as a DOM node id.

		**/


		this.getUniqueId = function() {
			if (this.facet.isQualitative())
				return this.facet.getUniqueId() + "_" + spotDOM.handleize(this.getName());
			return this.facet.getUniqueId() + "_" + spotDOM.handleize(this.value);
		};

		/**

		#### getName

		Returns a nice-looking name for this facet, given the information you've supplied.

		**/
		this.getName = function() {
			if (this.name)
				return this.name;
			if (this.facet.isQuantitative()) {
				if (this.value.charAt(0) == "-") {
					return `${spotDefault.locales().results_under} ${this.formatValue(this.value.slice(1))}`;
				} else if (this.value.charAt(this.value.length-1) == "+") {
					return `${spotDefault.locales().results_under} ${this.formatValue(this.value.slice(0, this.value.length-1))}`;
				} else {
					var groups = this.value.split(/\s*\-\s*/);
					return this.formatValue(groups[0]) + " - " + this.formatValue(groups[1]);
				}
			}
			return this.value;
		};
		/**

		#### getValue

		Returns the value of this facet; i.e. something that can be directly passed to Spot.

		**/
		this.getValue = function() { return this.value; };

		this.numericalList = null;
		this.getNumericalList = function() {
			if (this.numericalList)
				return this.numericalList;
			var names = [];
			for (var i in this.facet.values) {
				names.push(this.facet.values[i]);
			}
			names.sort(function(a,b) {
				var convertedA = parseInt(a.value);
				var convertedB = parseInt(b.value);
				if (isNaN(convertedA) && !isNaN(convertedB))
					return 1;
				if (!isNaN(convertedA) && isNaN(convertedB))
					return -1;
				if (isNaN(convertedA) && isNaN(convertedB))
					return 0;
				if (convertedA < convertedB)
					return -1;
				if (convertedA == convertedB)
					return 0;
				return 1;
			});
			this.numericalList = names;
			return names;
		};

		this.getNumericalIndex = function() {
			var names = this.getNumericalList();
			for (var i = 0; i < names.length && names[i] != this; ++i);
			return i;
		};

		this.formatValue = function(value) {
			value = value.replace(/[^0-9\.]+/g, '');
			if (this.facet.smartType.indexOf("price") != -1)
				return spotDOM.formatMoney(value);
			return value;
		};


		this.enable = function() {
			this.enabled(true);
		};

		this.disable = function() {
			this.enabled(false);
		};

		this.toggle = function() {
			if (this.enabled())
				this.disable();
			else
				this.enable();
		};

		this.update = function(value) {
			if (this.count() != value.count)
				this.count(value.count);
		};

		// Compose this into the query, if active.
		this.composeQuery = function(query) {

			if (this.enabled()) {
				if (this.facet.isOption()) {
					if (Array.isArray(this.value)) {
						var queries = [];
						for (var i = 0; i < this.value.length; ++i) {
							if (typeof(this.value[i]) == "object") {
								for (var j in this.value[i]) {
									if (this.value[i].hasOwnProperty(j))
										queries.push(spotDOM.spotAPI.se().option(this.facet.smartType['option'], j, this.value[i][j]));
								}
							} else {
								queries.push(spotDOM.spotAPI.se().option(this.facet.smartType['option'], this.value[i]));
							}
						}
						if (queries.length == 1)
							return query.merge(queries[0]);
						return query.or(queries);
					} else {
						if (typeof(this.value) == "object") {
							for (var j in this.value) {
								if (this.value.hasOwnProperty(j))
									query = query.option(this.facet.smartType['option'], j, this.value[j]);
							}
						} else {
							query = query.option(this.facet.smartType['option'], this.value);
						}
						return query;
					}
				} else if (this.facet.isMetafield()) {
					var target = this.facet.isProductMetafield() ? "product_metafield" : "variant_metafield";
					var value = this.value;
					if (!Array.isArray(value))
						value = [value];
					var queries = [];
					for (var i = 0; i < value.length; ++i) {
						if (typeof(value[i]) == "object") {
							for (var j in value[i]) {
								if (value[i].hasOwnProperty(j))
									queries.push(spotDOM.spotAPI.se()[target](this.facet.getNamespace(), this.facet.getKey(), j, value[i][j]));
							}
						} else {
							queries.push(spotDOM.spotAPI.se()[target](this.facet.getNamespace(), this.facet.getKey(), value[i]));
						}
					}
					if (queries.length == 1)
						return query.merge(queries[0]);
					return query.or(queries);
				} else if (this.facet.isQuantitative()) {
					if (this.value.charAt(0) == "-")
						return query[this.facet.smartType]("<", this.value.slice(1));
					else if (this.value.charAt(this.value.length-1) == "+")
						return query[this.facet.smartType](">=", this.value.slice(0, this.value.length-1));
					else {
						var groups = this.value.split(/\s*\-\s*/);
						return query[this.facet.smartType]("between", [groups[0], groups[1]]);
					}

				}

				var value = this.value;
				if (!Array.isArray(value))
					value = [value];
				var queries = [];
				for (var i = 0; i < value.length; ++i) {
					if (typeof(value[i]) == "object") {
						for (var j in value[i]) {
							if (this.value[i].hasOwnProperty(j))
								queries.push(spotDOM.spotAPI.se()[this.facet.smartType], j, value[i][j]);
						}
					} else {
						queries.push(spotDOM.spotAPI.se()[this.facet.smartType](value[i]));
					}
				}
				if (queries.length == 1)
					return query.merge(queries[0]);
				return query.or(queries);
			}
			return query;
		};
	};
	/**

	### SortOrders

	Sort orders are objects which represent a potential sort order on the Shopify store. There are number of default ones. Generally, this is populated from the Spot control panel, but can also be manually populated.


	#### new

	Takes in the field string, the direction, as well as an optional name. Also takes in JSON format as passed in from the Spot-controlled metafields.

		new spotDOM.SortOrder(field)

		new spotDOM.SortOrder("created-desc")

		new spotDOM.SortOrder("created-descending")

		new spotDOM.SortOrder(field, direction)

		new spotDOM.SortOrder("created", "desc")

		new spotDOM.SortOrder(field, direction, name)

		new spotDOM.SortOrder("created", "desc", "My Created Order")

		new spotDOM.SortOrder({"option":"Size"}, "desc", "My Sizes")

		new spotDOM.SortOrder({"product-metafield":{"mynamespace":"mykey"}}, "asc", "My Metafield Ordering")

		new spotDOM.SortOrder(json)

		new spotDOM.SortOrder({"label":"Best Match","property":"search","direction":"desc","type":"special","active":true})

		new spotDOM.SortOrder({"label":"My Metafield Ordering","property":"product-metafield","direction":"desc","type":"numeric","active":true,"namespace":"mynamespace","key":"mykey"})

	**/
	this.SortOrder =  function(field, direction, name) {
		if (typeof(field) == "object" && field['label']) {
			this.name = field.label;
			if (field.property == "option") {
				this.field = {"option": field.option};
			} else if (field.property == "product-metafield" || field.property == "variant-metafield") {
				this.field = { };
				this.field[field.property] = { }
				this.field[field.property][field.namespace] = field.key;
			} else {
				this.field = field.property;
			}
			this.direction = field.direction;
		} else {
			if (!direction && typeof(field) == "string") {
				var groups = /^(.*?)\-(asc|desc)(ending)?$/.exec(field);
				if (groups) {
					field = groups[1];
					direction = groups[2];
				}
			}

			this.field = field;
			this.direction = direction;
			this.name = name;
		}
		if (this.field == "manual")
			this.direction = "asc";
		else if (this.field == "best-selling" || this.field == "search")
			this.direction = "desc";
		if (!this.direction)
			throw "Requires a direction be supplied.";

		/**

		#### Getters


		##### getName

		Returns a nice looking name that represents this sort order. If `name` was specified on construction, uses that.

		**/

		this.getName = function() {
			if (this.field == "created" && this.direction == "asc")
				return spotDefault.locales().sort_order.asc.created;
			if (this.field == "created" && this.direction == "desc")
				return spotDefault.locales().sort_order.desc.created;
			if (this.field == "title" && this.direction == "asc")
				return spotDefault.locales().sort_order.asc.title;
			if (this.field == "title" && this.direction == "desc")
				return spotDefault.locales().sort_order.desc.title;
			if (this.field == "price" && this.direction == "asc")
				return spotDefault.locales().sort_order.asc.price;
			if (this.field == "price" && this.direction == "desc")
				return spotDefault.locales().sort_order.desc.price;
			if (this.field == "best-selling" && this.direction == "desc")
				return spotDefault.locales().sort_order.desc.best_selling;
			if (this.field == "manual" && this.direction == "asc")
				return spotDefault.locales().sort_order.asc.manual;
			if (this.field == "search" && this.direction == "desc")
				return spotDefault.locales().sort_order.desc.search;
			if (this.name)
				return this.name;

			var internalField = this.field;
			if (typeof(this.field) == "object")
				internalField = this.field['option'] || (this.field['product-metafield'] ? Object.values(this.field['product-metafield'])[0] : null) || (this.field['variant-metafield'] ? Object.values(this.field['variant-metafield'])[0] : null);
			return (internalField.charAt(0).toUpperCase() + internalField.slice(1) + " " + (this.direction == "asc" ? spotDefault.locales().ascending : spotDefault.locales().descending)).replace(/_/g, " ");
		};

		/**

		##### getUniqueId

		Returns an ID that is unique to this sort order. Usually is descriptive of the facet as well. Suitable for direct insertion into a query string, or
		for use as a DOM node id. Should ideally match Shopify's *front-end* sort order values identically, in most cases. (Back-end sort orders are called
		completely different things for some reason.)

		**/
		this.getUniqueId = function() {
			if (typeof(this.field) == "object")
				return spotDOM.handleize(this.getName());
			if (this.field == "best-selling" || (this.field == "sales" && this.direction == "desc"))
				return "best-selling";
			if (this.field == "manual" && this.direction == "asc")
				return this.field;
			return this.field + "-" + this.direction + "ending";
		};

		/**

		##### getShopifySortOrder

		Returns what the Shopify sort order value would be for this sort option; used when `shopifyMode` is active.

		**/
		this.getShopifySortOrder = function() {
			return this.getUniqueId();
		};

		/**

		##### isRelevant

		Returns true if this sort order is relevant to the current situation (i.e. will ignore things like "best match" if you're not searching).

		**/
		this.isRelevant = function() {
			if (this.field == "search")
				return spotDOM.search() || spotDOM.isSearchPage();
			if (this.field == "manual")
				return spotDOM.collection() || spotDOM.isCollectionPage();
			return true;
		};
	};


	/**

	## Events

	SpotDOM will fire off certain events which should be handled by your implementation. A detailed list is below. A callback handler can be supplied by simply passing a single function as the argument
	to the listed event.

	### products

	The **most important** event. A listener which receives the list of products when a query complete. This should be generally used to populate your various DOM elements.

	For example, to simply list all prodcuts returned by a query, you can write the following.

		spotDOM.products(function(products) { console.log(products); });

	**/
	this.listeners(this, 'products');
	/**

	### spellCheck

	A listener which receives the result of a spellingCheck, if one was performed. It is called with an object representing the results of the check, which includes the original string, and what
	it was corrected to.

	**/
	this.scalarField(this, 'spellCheck');
	/**

	### beginQuery

	Called when a query in requested, before any work is done. Can be used to alter the state of the page before things are processed into the query.

	### query

	Called immediately after results are returned, contains all products, and resultset information. If you need to do any manual processing of the return data, to populate some information about the resultset,
	such as to update facets, or pagination, it's usually done in here.

	**/
	this.listeners(this, 'beginQuery');
	/**

	### endQuery

	Called when a query has been completed after all work is done, and all handlers are called.

	**/
	this.listeners(this, 'endQuery');
	/**
	## Variables

	Various variables exist on the SpotDOM object. All variables can be hooked with a listener, which runs whenever the variable is set, in the exact same manner as events listed above.
	This should be your main way of interacting with the SpotDOM object to customize its behaviour.

	### Array Variables

	Array variables have two functions; .addVariableName and .removeVariableName. Both of these can be hooked, by passing a function to either.

	#### facet

	Contains a list of all facets to be used by the system. The full list can be retrieved with `.facets()`, and individual facets can be added/removed with
	`.addFacet(facet)` or `.removeFacet(facet)`.

	**/
	this.arrayField(this, 'facet');
	/**

	#### sortOrder

	Contains a list of all possible sort orders for this view. The full list can be retrieved with `.sortOrders()`, and individual orders can be added/removed
	with `.addSortOrder(sortOrder)` or `.removeSortOrder(sortOrder)`.

	**/
	this.arrayField(this, 'sortOrder');
	// If our current sort order is transient, swap it for the one we've just entered, if their IDs are identical.
	this.addSortOrder(function(sortOrder) {
		if (this.sort() && sortOrder.getUniqueId() == this.sort().getUniqueId())
			this.sort(sortOrder, false);
	});
	/**

	### Scalar Variables

	Scalar variables can be hooked by simply calling the name of the variable, and passing a function, like so:

		spotDOM.collection(function(collection) { console.log("The collection I set is: " + collection); })

	#### collection

	Denotes the collection to search in. By default, this is taken from the address bar path. If not present, no collection is used.
	**/
	this.getCollectionPage = function() {
		var groups = /\/collections\/([^\/\?]+)/.exec(window.location.href);
		return groups && groups.length > 1 && groups[1] != "vendors" && groups[1] != "types" ? groups[1] : null;
	};
	this.scalarField(this, 'collection', this.getCollectionPage());
	/**
	#### sort

	Denotes the sort order. By default, this is taken from the query paramters. Possible values include all normal Shopify values, for the sorting dropdown, as well as "search", "featured", and any custom sort orders you may have set up in Spot.

	Can also take in a `SortOrder` object from the `sortOrders` array. If the `sort` is set to be a string, it will automaticalliy wrap the incoming string in a `SortOrder` object before setting it. This is known as a transient sort order. This sort order
	is not automatically passed to listeners, and once unset, will be removed from the list.

	**/
	this.scalarField(this, 'sort', vars['sort_by'], {
		"preset": function(value) {
			if (typeof(value) == "string") {
				var orders = spotAPI.grep(this.sortOrders(), function(e) { return e.getUniqueId() == value });
				if (orders.length > 0)
					return orders[0];
				return new this.SortOrder(value);
			}
			return value;
		}
	});
	/**
	#### search

	The free-form text search used currently. Taken from the "q" query parameter, by default. If not present, performs no textual search.

	**/
	this.scalarField(this, 'search', vars['q'] && vars['q'].replace(/( AND )?\(\(.*/g, "") ? vars['q'].replace(/( AND )?\(\(.*/g, "") : null);
	/**
	#### autoCorrect

	Determines whether queries should be made with autoCorrect. On by default.

	**/
	this.scalarField(this, 'autoCorrect', true);
	/**

	#### split

	The split used for this query. Splits are set up in the Spot control panel. By default, the split is "auto".

	**/
	this.scalarField(this, 'split', vars['split'] || 'auto');
	/**

	#### page

	The page used. By default, this is 1, unless specified in the query string to be different.

	**/
	this.scalarField(this, 'page', parseInt(vars['page']) || 1);
	/**

	#### locale

	The locale used. By default, this is `Shopify.locale`. If no locales are set up on your store, this will be ignored by Spot.

	**/
	this.scalarField(this, 'locale', Shopify.locale);
	/**

	#### currency

	The currency used. By default, this is taken from `Shopify.currency`. Takes a hash in the form of { code: "3LC", "rate": 1.0000 }.

	**/
	this.scalarField(this, 'currency', Shopify && Shopify.currency ? { code: Shopify.currency.active, rate: Shopify.currency.rate } : null);
	/**

	#### fields

	The fields required of the returned products. By specifying an array here, you can reduce the amount of bandwidth required to retrieve a page of products.
	By default, this is empty, and the query will return the entire product data.

	**/
	this.scalarField(this, 'fields');
	/**

	#### count

	The amount of products in the last resultset queried, as returned by a query.

	**/
	this.scalarField(this, 'count');
	/**

	#### countBehavior

	The type of count that is performed. See the [SpotAPI counting section](#resultset-counting) for details. By default, this is "approximate".

	**/
	this.scalarField(this, 'countBehavior', 'approximate');
	/**

	#### optionBehavior

	The type of count that is performed for facets. The four supported values are `none`, `exists`, `approximate` and `exact`. By default, this is `approximate`. The meanings of this are below, in decreasing order of speed.

	* `none`: Will not pass options to Spot, but will instead simply keep track of facets without counting. Fastest.
	* `exists`: Will return a 1 or 0, depending on whether or not the facet has any values in the resultset. Pretty Fast.
	* `approximate`: What this means varies depending on the situation, but generally it means that if you have a decent amount of facets and/or products returned, the counts displayed next to each facet won't be exact. Semi-Fast.
	* `exact`: Will return the exact amount of products on a particular facet. Slowest.

	**/
	this.scalarField(this, 'optionBehavior', 'approximate');

	/**

	#### paginate

	The amount of products to be returned in a given page of products. By default, this is taken from the query string, in the `rows`, or `paginate` parameters.
	If neither of these are specified, it's taken from whatever spot's default query is, and if that's not specified, the default is 12.

	**/
	this.scalarField(this, 'paginate', parseInt(vars['rows'] || vars['paginate'] || spotAPI.s().innerAttributes.rows || 12));
	/**

	#### allVariants

	By default, false. Determines whether or not we should return ALL the variants present for a particular product that gets a hit from the query. This property can have
	three values.

	`false`: Retrieve only variants which match your criteria.
	`"flag"`: Retrieve all variants for your product resultset, but add a boolean of "relevant" in the product JSON to denote which variants are relevant for your query
	`true`: Retrieve all variants for your product resultset.

	As an example, if this is false, when you query for every "Green" variant that's in stock, you'll get only products who have a green variant that's in stock, and a Red in stock variant on the same product will be omitted.
	If true, you'd get that, Red in stock variant as well. If `"flag"`, you'd get the Red variant, but it wouldn't have `"relevant":true`, whereas the Green variants would.


	**/
	this.scalarField(this, 'allVariants', false);
	/**

	#### debugMode

	A boolean flag. Has three different modes, true, false, and null. By default, the value is the value of the "debugMode" key in session storage.

	If true, the following happens:
	* Warnings are promoted to errors, and throw exceptions.
	* `failBehaviour` is set to `null`.

	If false, the following happens:
	* Warnings are logged to a console, if present, but otherwise ignored.

	If null, the following happens:
	* Upon a request being made, if the debugger is open in at least Chrome, or if Firebug is being used, debugMode is set to true.
	* Warnings are logged to a console, if present, but otherwise ignored.

	**/
	this.scalarField(this, 'debugMode', this.getStorage("spotDebugMode"));
	if (this.debugMode())
		this.log("This instance of Spot has booted in 'Debug Mode'. Warnings are promoted to errors, and failBehaviour has been forced to null.");

	// This is somewhat ill-supported. Consider removing this.
	this.debuggerOpened = false;
	this.isDebuggerOpen = function() {
		/* if (window.console) {
			if (window.console.firebug)
				this.debuggerOpened = true;
			try {
				var devtools = function(){};
				devtools.toString = function() {
					this.debuggerOpened = true;
				};
				console.log('%c', devtools);
			} catch (e) {

			}
		}*/
		return this.debuggerOpened;
	};
	/**
	#### shopifyMode

	If this is set to true, either because of a Spot outage, or other reason, Spot will do its best to serve queries using Shopfy's native system, although it will not be possible to offer
	the full Spot featureset. Should be hooked to enable/disable Spot specific features over and above faceting (if you have any). Related to `failBehavior` below.

	By default, if this is set, its value is stored in sessionStorage. This value will persist until the user quits or restarts their browser,
	or until 10 minutes have passed, whichever is quicker. Whenever a page loads in shopifyMode, a console log will be emitted informing the user of this.

	When set to true, the implementation will remove all facets that cannot be served up by Shopify's native system. This means the following:

	* Anything that is not a tag-based facet will be removed.
	* If the user is on the search page, in addition to tag-based facets, it will also preserve product_type facets, and vendor facets, if the `values` for these are presented to the system as part of its initialization (see [facets](#facets) below).

	**/
	this.scalarField(this, 'shopifyMode', this.getStorage("spotShopifyMode") || false);
	if (this.shopifyMode())
		this.log("This instance of Spot has booted in 'Shopify Mode', due to an error encountered previously. To disable this functionality, make sure you call spotDOM.failBehavior(null) immediately after instantiating.");
	/**

	#### failBehavior

	By default this is set to `"shopify"`. Other valid values are `null`. When set to `"shopify"`, when a Spot query fails, for whatever reason, Spot will set `shopifyMode`
	to true, and call all listeners. This allows you to gracefully degrade into a shopify-powered version of search and faceting. While nowhere near as powerful,
	or sporting many features, this should enable the site to still serve products and searches, even if most of the powerful functionality is lost while the outage
	occurs. When this is set to `null`, and `shopifyMode()` is true, it will automatically set `shopifyMode(false)`.

	**/
	this.scalarField(this, 'failBehavior', "shopify");
	this.failBehavior(function(behavior) {
		if (behavior == null && this.shopifyMode())
			this.shopifyMode(false);
	});
	this.debugMode(function(debug) {
		this.setStorage("spotDebugMode", debug);
		if (debug != null) {
			if (debug) {
				this.failBehaviour(null);
			}
		}
	}, true);

	/**

	#### moneyFormat

	The format you want to render prices in, by default. By default this is '${{amount}}'.

	**/
	this.scalarField(this, 'moneyFormat', '${{amount}}');

	/**

	## Functions

	A number of utility functions which make managing dom elements easier.

	### getProductTitle

	Takes in a product, and returns an appropriate description, based on returned variants, if `allVariants` is false, and splits. Generally should be preferred over simply using `product.title`.
	Basically, loop through all three options on a product. If all variants share the same value for this option, add the option to our returned string.

	As an example, if the result of a product query returns a product called "T-Shirt" has "Size" and "Color" as options, if our returned variants are ["Red", "Small"], ["Red", "Large"],
	["Green", "Small], ["Green", "Large"] the title would be output as "T-Shirt".

	If the same query has the result of ["Red", "Small"], ["Red", "Large"], the returned title would be "T-Shirt - Red".

	**/
	this.getProductTitle = function(product) {
		var option1 = {};
		var option2 = {};
		var option3 = {};
		for (var i = 0; i < product.variants.length; ++i) {
			if (product.variants[i].option1 != null)
				option1[product.variants[i].option1] = 1;
			if (product.variants[i].option2 != null)
				option2[product.variants[i].option2] = 1;
			if (product.variants[i].option3 != null)
				option3[product.variants[i].option3] = 1;
		}
		var option1keys = Object.keys(option1);
		var option2keys = Object.keys(option2);
		var option3keys = Object.keys(option3);
		var extraElements = [];
		if (option1keys.length == 1)
			extraElements.push(option1keys[0]);
		if (option2keys.length == 1)
			extraElements.push(option2keys[0]);
		if (option3keys.length == 1)
			extraElements.push(option3keys[0]);
		if (extraElements.length > 0)
			return product.title + " - " + extraElements.join(" / ");
		return product.title;
	};
	/**

	### getProductURL

	Takes in a product, and returns an appropriate URL. Will either return a direct link to the product, or one nestled in a collection, or with a variant if applicable.

	**/
	this.getProductURL = function(product) {
		if (this.collection())
			return '/collections/' + this.collection() + product.url;
		return product.url;
	};
	/**


	### getProductImage

	Takes in a product, and returns an appropriate description, based on returned variants, if `allVariants` is false, and splits. Generally should be preferred over simply using `product.image`.

	Allows for the system to remove irrelevant images from results that only return some variants, or results that are using a split.

	**/
	this.getProductImage = function(product) {
		if (product.featured_image)
			return product.featured_image;
		return spotAPI.getFeaturedImage(product);
	};
	/**

	### formatMoney

	Takes in the value from Spot, converts it to the local currency be used, as well as an optional formatting string, and returns a nicely formatted string. For example, passing "1000", could return "$1,000.00", depending
	on the money format.

	**/
	this.formatMoney = function(money, format, currency) {
		if (!currency)
			currency = this.currency();
		if (currency)
			money *= currency.rate;
		if (!format)
			format = this.moneyFormat() || "{{ amount }}";
		money = "" + Math.round(money * 100);

		var result = format;
		var dollars = money.slice(0, -2);
		var cents = money.slice(-2);
		result = result.replace(/{{\s*amount\s*}}/gi, dollars.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + "." + cents );
		result = result.replace(/{{\s*amount_no_decimals\s*}}/gi, dollars);
		result = result.replace(/{{\s*amount_with_comma_separator\s*}}/gi, dollars.replace(/\B(?=(\d{3})+(?!\d))/g, ".") + "," + cents);
		result = result.replace(/{{\s*amount_no_decimals_with_comma_separator\s*}}/gi, dollars);
		result = result.replace(/{{\s*amount_with_apostrophe_separator\s*}}/gi, dollars.replace(/\B(?=(\d{3})+(?!\d))/g, "'") + "." + cents);
		return result;
	};
	/**

	### getSizedImage

	Takes in an image url/object, and a size, and then returns the relevant URL for that image, sized at a particular size, using Shopify's image CDN.

	**/
	this.getSizedImage = function(image, size) {
		if (!image)
			return null;
                if (typeof(image) == "object")
                    image = image['src'];
		return image.replace(/(_\d+x\d+)?\.(png|gif|jpg|jpeg)/, '_' + size + '.$2');
	};

	/**

	### isSearchPage

	Tells you whether or not you're on Shopify's search page.

	**/
	this.isSearchPage = function() {
		return /\/search\b/.test(window.location.pathname);
	};
	/**

	### isCollectionPage

	Tells you whether or not you're on a collection page.

	**/
	this.isCollectionPage = function() {
		return /\/collections\b/.test(window.location.pathname) && !this.isProductPage();
	};
	/**

	### getCollectionPage

	Gets the handle of the collection page you're on from the URL.

	**/

	/**

	### isVendorCollectionPage

	Tells you whether or not you're on Shopify's vendor filtration collection.

	**/
	this.isVendorCollectionPage = function() {
		return /\/collections\/vendors\b/.test(window.location.href) && !this.isProductPage();
	};
	/**

	### isProductTypeCollectionPage

	Tells you whether or not you're on Shopify's product type filtration collection.

	**/
	this.isProductTypeCollectionPage = function() {
		return /\/collections\/types\b/.test(window.location.href) && !this.isProductPage();
	};

	/**

	### getCollectionPageTags

	Returns a list of tags that the system is currently filtering by, Shopify style. (present at the end of the path on a collection, separated by '+')

	**/
	this.getCollectionPageTags = function() {
		var groups = /\/collections\/[^\/]+\/(.*)/.exec(window.location.pathname);
		if (groups && groups[1])
			return spotAPI.map(groups[1].split("+"), function(e) { return decodeURI(e); });
		return [];
	};
	/**

	### isProductPage

	Tells you whether or not you're on a product page.

	**/
	this.isProductPage = function() {
		return /\/products/.test(window.location.href);
	};
	/**

	### generateQueryVariables

	Given the existing facets, sort order, page and search, generates a hash that represents the variables that should be placed into the query string.

	**/
	this.generateQueryVariables = function() {
		var vars = this.getUrlVars();
		for (var i in vars) {
			if (/^f_/.test(i) || i == "page" || i == "q" || i == "sort_by")
				delete vars[i];
		}
		this.composeFacetQueryString(vars);
		if (this.page() > 1)
			vars['page'] = this.page();
		if (this.sort())
			vars['sort_by'] = this.sort().getUniqueId();
		if (this.search())
			vars['q'] = this.search();
		return vars;
	};
	/**

	### generateQueryString

	Optionally takes in a hash. Calls `generateQueryVariables`, and merges it with the provided hash, then URL-encodes everything to produce the query string that should be placed
	into the browser's query string.

	**/
	this.generateQueryString = function(mergeHash) {
		var vars = this.generateQueryVariables();
		if (mergeHash) {
			for (var i in mergeHash)
				vars[i] = mergeHash[i];
		}
		return this.encodeUrlVars(vars);
	};
	/**

	### updateQueryString

	Optionally takes in a hash. Calls `generateQueryString`, and places it into the browser's address bar, `using window.history.pushState`, if applicable.

	**/
	this.updateQueryString = function(mergeHash) {
		var url = window.location.protocol + "//" + window.location.host + window.location.pathname;
		var str = this.generateQueryString(mergeHash);
		if (str != "?")
			url += this.generateQueryString(mergeHash);
		if (window.history)
			window.history.pushState({}, document.title, url);
	};

	/**

	### generateQuery

	Generates a query object from spotAPI that takes into account all the selected facets, various behaviour modifiers, sorting, search, spell check,
	split, and everything like that, and returns the actual spotAPI object that does the work, and can be `.e()`'d to retrieve the desired resultset.

	**/
	this.generateQuery = function() {
		var query = this.spotAPI.s().count(true, this.countBehavior()).allVariants(this.allVariants()).paginate(this.paginate()).page(this.page());
		if (this.sort())
			query = query.sort(this.sort().direction == "asc" ? {"asc": this.sort().field } : { "desc": this.sort().field });
		if (this.collection())
			query = query.collection(this.collection());
		query = this.composeFacetQuery(query);
		if (this.search() != null)
			query = query.search(this.search()).autoCorrect(this.autoCorrect());
		if (this.fields())
			query = query.fields(this.fields());
		if (this.split())
			query = query.split(this.split());
		if (this.currency())
			query = query.currency(this.currency());
		if (this.locale())
			query = query.locale(this.locale());
		return query;
	};
	/**

	### initDefaultSortOrders

	Initializes the system with the default set of Shopify sort options.

	* Featured (if on collections page)
	* Best Match (if on search page)
	* Price: Low to High
	* Price: High to Low
	* A-Z
	* Z-A
	* Oldest to Newest
	* Newest to Oldest
	* Best Selling

	**/
	this.initDefaultSortOrders = function() {
		if (this.isCollectionPage())
			this.addSortOrder(new this.SortOrder("manual", "asc"));
		else if (this.isSearchPage())
			this.addSortOrder(new this.SortOrder("search", "desc"));
		this.addSortOrder(new this.SortOrder("price", "asc"));
		this.addSortOrder(new this.SortOrder("price", "desc"));
		this.addSortOrder(new this.SortOrder("title", "asc"));
		this.addSortOrder(new this.SortOrder("title", "desc"));
		this.addSortOrder(new this.SortOrder("created", "asc"));
		this.addSortOrder(new this.SortOrder("created", "desc"));
		this.addSortOrder(new this.SortOrder("best-selling", "desc"));
	};
	/**

	### queryGuard

	Ensures that only one query is run within this block, at the end, if .query() was ever called. If it's never called, then no query is run. Useful for things like initailizations where one might
	have queries triggered in multiple places on changing things. Returns a deferred that contains the query payload if one occurred, or one that resolves with an empty argument list if no query was
	run.

	As an example:

		spotDOM.queryGuard(function() {
			spotDOM.query();
			spotDOM.query();
			spotDOM.query();
		});

	In the above example, only one query would be run.

		spotDOM.queryGuard(function() {
		});

	In this example, no queries are run.

	These can also be nested, but only the query will only ever be run on the outer-most block.

		spotDOM.queryGuard(function() {
			spotDOM.query();
			spotDOM.queryGuard(function() {
				spotDOM.query();
			});
			spotDOM.query();
			spotDOM.query();
		});
		// Query will run here.

	In the above example, a query will only run when the comment is reached.

	**/
	this.queryGuard = function(block) {
		if (this["_queryWaiter"]) {
			block();
			return this["_queryWaiter"];
		}
		this["_queryWaiter"] = spotAPI.Deferred();
		block();
		var deferred = this["_queryWaiter"];
		this["_queryWaiter"] = null;
		if (this["_queryRequests"]) {
			this.query().done(function() {
				deferred.resolve.apply(deferred, arguments);
			}).fail(function() {
				deferred.reject.apply(deferred, arguments);
			});
		} else {
			deferred.resolve();
		}
		return deferred;
	};


	/**

	## Tying it Together

	A great way to get an idea of how all this works together is to look at SpotDefault's sample implementation of a panel. It gives a concise overall view
	of how to build a faceting panel using the above techniques. You can see it [here](#pane).

	**/

	this.updateFacets = function(options) {
		var facets = this.facets();
		for (var i = 0; i < options.length && i < facets.length; ++i)
			facets[i].update(options[i]);
	};


	this.composeFacetQuery = function(query) {
		var array = [];
		var faceting = spotDOM.spotAPI.se();
		var facets = this.facets();
		for (var i = 0; i < facets.length; ++i) {
			faceting = facets[i].composeQuery(faceting);
			array.push(facets[i].getOptionValue());
		}
		query = query.facets(faceting);
		if (this.optionBehavior() != "none")
			query = query.options(array, true, this.optionBehavior());
		return query;
	};
	this.composeFacetQueryString = function(vars) {
		var facets = this.facets();
		for (var i in facets)
			facets[i].composeQueryString(vars);
		return vars;
	};

	// Persist this across sessions.
	this.shopifyMode(function(mode) {
		this.setStorage("spotShopifyMode", mode, 10*60);
	});
};

/***

SpotDefault Documentation
=========================

`SpotDefault` is a class that provides a sample implementation of spot, using `SpotDOM`, and `SpotAPI`. Allows you to use out of the box pagination, and pane construction. The individual components are detailed below. Click here
if you want to skip the component description and just go right to the [implementation checklist](#implementation-checklist-sample-implementation).

If you are going to change anything about SpotDefault's code, you should probably copy the entire object to your own snippet, and make the changes there.

### What are its dependencies?

There are currently no dependencies for the default API.

## Components

**/
var SpotDefault = function(spotDOM) {
        if (!spotDOM)
                throw "Requires SpotDOM. Please pass a copy of the API to SpotDefault's constructor.";

	var spotDefault = this;


	// *** CUSTOM EDIT: Locale strings
	var _locales = {
		default: {
			ascending: "Ascending",
			clear_all: "Clear all",
			descending: "Descending",
			load_more: "Load more",
			next: "Next",
			previous: "Previous",
			results_over: "Over",
			results_under: "Under",
			show_all: "Show all",
			show_results: "Showing results for ",
			sort_by: "Sort",
			sort_order: {
				asc: {
					created: "Oldest to Newest",
					title: "Title: A-Z",
					manual: "Featured",
					price: "Price: Low to High"
				},
				desc: {
					best_selling: "Best Selling",
					created: "Newest to Oldest",
					title: "Title: Z-A",
					price: "Price: High to Low",
					search: "Best Match"
				},
			},
			view_details: "View full details"
		}
	}

	// *** CUSTOM EDIT: Translation function
	this.locales = function(arg) {
		if (typeof arg === 'string' ) {
			return _locales[arg];
		} else if (typeof arg === 'object') {
			let languages = Object.keys(arg);
			languages.forEach(lang => {
				if (_locales) {
					_locales[lang] = Object.assign({}, _locales[lang], arg[lang]);
				} else {
					_locales[lang] = arg[lang];
				}
			});
		}
		return _locales[document.documentElement.lang] ? _locales[document.documentElement.lang] : _locales.default;
	}

	/**

	### PagedPaginationHelper

	This is a small helper function that returns a set of elements based on your inputs that represents a pagination control for `SpotDOM`.

	The pagination helper can be used like so:

		var paginationHelper = window.spotDefault.pagedPaginationHelper({ count: totalProductCount });
		$('body').append(paginationHelper.element);

	Below is the implementation of the helper:

	%{PagedPaginationHelper}

	**/

	/** %+{PagedPaginationHelper} **/
	this.pagedPaginationHelper = function(options) {
		var count = options['count'];
		var page = options['page'] || spotDOM.page();
		var pageInterval = options['interval'] || 2;
		var containerElement = options['containerElement'] || this.createElement("<ul class='pagination-container'></ul>");
		var prevElement = options['prevElement'] || this.createElement("<li class='prev'>&larr;</li>");
		var nextElement = options['nextElement'] || this.createElement("<li class='next'>&rarr;</li>");
		var pageElement = options['pageElement'] || this.createElement("<li></li>");
		var maxElement = options['maxElement'] ||  options['pageElement']  || this.createElement("<li class='max'></li>")
		var minElement = options['minElement'] ||  options['pageElement']  || this.createElement("<li class='min'></li>")
		var activePageElement = options['activePageElement'] || options['pageElement'] || this.createElement("<li class='active'></li>");
		var elipsisElement = options['elipsisElement'] || this.createElement("<li class='disabled'>...</li>");

		var resultsPerPage = options['paginate'] || spotDOM.paginate();
		var maxPages = Math.ceil(count / resultsPerPage);
		var min = Math.max(1, page - pageInterval);
		var max = Math.min(page + pageInterval, maxPages);

		var elements = [];
		if (min != 1 || max != 1) {
			if (page > 1) {
				elements.push(prevElement.cloneNode(true));
				elements[elements.length-1].addEventListener("click", function(e) { spotDOM.page(spotDOM.page()-1); e.preventDefault(); });
			}
			if (min > 1) {
				elements.push(minElement.cloneNode(true));
				elements[elements.length-1].innerText = 1;
				elements[elements.length-1].addEventListener("click", function(e) { spotDOM.page(1); e.preventDefault(); });
				if (min > 2 && elipsisElement)
					elements.push(elipsisElement.cloneNode(true));
			}

			for (var i = min; i <= max; ++i) {
				elements.push((i == page ? activePageElement : pageElement).cloneNode(true));
				elements[elements.length-1].innerText = i;
				elements[elements.length-1].addEventListener("click", function(e) { spotDOM.page(parseInt(this.innerText)); e.preventDefault(); });
			}

			if (max < maxPages) {
				if (max < maxPages - 1)
					elements.push(elipsisElement.cloneNode(true));

				elements.push(maxElement.cloneNode(true));
				elements[elements.length-1].innerText = maxPages;
				elements[elements.length-1].addEventListener("click", function(e) { spotDOM.page(maxPages); e.preventDefault(); });
			}

			if (page < maxPages) {
				elements.push(nextElement.cloneNode(true));
				elements[elements.length-1].addEventListener("click", function(e) { spotDOM.page(spotDOM.page()+1);  e.preventDefault(); });
			}
		}
		spotDOM.page(function() { window.scrollTo(0,0); });
		spotAPI.forEach(elements, function(e) { containerElement.appendChild(e); });
		return containerElement;
	};
	/** %-{PagedPaginationHelper} **/


	this.getDocHeight = function() {
		var D = document;
		return Math.max(D.body.scrollHeight, D.documentElement.scrollHeight, D.body.offsetHeight, D.documentElement.offsetHeight, D.body.clientHeight, D.documentElement.clientHeight);
	};

	this.getWindowOffset = function() {
		return window.pageYOffset || window.scrollY;
	};

	this.getWindowHeight = function() {
		return window.innerHeight;
	};

	/**

	### ScrollPaginationHelper

	This is a small helper class that sets up a listener to auto-retrieve a new set of products when the bottom
	of the page is reached. Used for "infinite scrolling" implementations.

	The pagination helper can be used like so:

		new window.spotDefault.ScollPaginationHelper();

	Below is the implementation of the helper:

	%{ScrollPaginationHelper}

	**/

	/** %+{ScrollPaginationHelper} **/
	this.ScrollPaginationHelper = function(options) {
		var scroll = this;
		if (!options)
			options = {}
		this.reachedProductEnd = false;
		this.triggerHeight = options['triggerHeight'] || 200;
		this.shouldTrigger = true;

		spotDOM.products(function(products) {
			scroll.reachedProductEnd = products.length < spotDOM.paginate();
			setTimeout(function() {
				scroll.shouldTrigger = true;
			}, 500);
		});

		this.handler = function() {
			if (scroll.shouldTrigger && !scroll.reachedProductEnd && spotDefault.getWindowOffset() + spotDefault.getWindowHeight() >= (spotDefault.getDocHeight() - scroll.triggerHeight)) {
				scroll.shouldTrigger = false;
				spotDOM.page(spotDOM.page()+1);
			}
		};

		window.addEventListener("scroll", this.handler);
	};
	/** %-{ScrollPaginationHelper} **/

	/**

	### Pane

	The Spot default is a great example of how to use the SpotDOM system. This single subclass provides a fully featured pane, built off the SpotDOM module. It can be dropped into most themes, and work
	with a minimal amount of extra styling.

	The pane can be instantiated by simply calling new:

		var pane = new window.spotDefault.Pane();
		document.body.appendChild(pane.paneElement);

	Below is the implementation of the pane:

	%{Pane}

	**/

	/** %+{Pane} **/
	this.Pane = function(options) {
		if (!options)
			options = {};
		// Basic default integration of the Spot pane; this can be easily replaced with one's own implementation.
		var pane = this;
		this.paneId = options['id'] || "spot-default-pane";
		this.paneElement = spotDefault.createElement("<div class='pane'></div>");
		if (options['classes'])
			spotAPI.forEach(options['classes'], function(e) { pane.paneElement.classList.add(e); });
		var breadcrumbsEnabled = options['breadcrumbs'] === undefined || options['breadcrumbs'];
		this.facetsElement = spotDefault.createElement("<div class='facets'></div>");
		this.paneElement.appendChild(this.facetsElement);

		spotDOM.arrayField(this, 'facet');
		spotDOM.arrayField(this, 'breadcrumb');

		if (breadcrumbsEnabled) {
			this.breadcrumbsElement = options['breadcrumbs'] || spotDefault.createElement("<div class='breadcrumbs'></div>");
			if (!options['breadcrumbs'])
				this.paneElement.appendChild(this.breadcrumbsElement);
			 // Every time a breadcrumb is added, add breadcrumbs to the breadcrumb element.
			 // Also add in a "Clear All" button; this is more sensible as a default, because it's easily hidden with CSS, if unwanted,
			 // rather than forcing the integrator to write one on their own.
			 // Ideally most of this should be controllable with CSS, rather than JS, for ease of debugging.
			this.clearBreadcrumbsElement = spotDefault.createElement(`<button class='clear-breadcrumbs'>${spotDefault.locales().clear_all}</div>`);

			this.clearBreadcrumbsElement.addEventListener("click", function(e) {
				spotDOM.queryGuard(function(e) { spotAPI.forEach(pane.breadcrumbs(), function(e) { e.enabled(false); }); });
				e.preventDefault();
			});
			this.addBreadcrumb(function(facetValue) {
				if (!facetValue.breadcrumbElement) {
					facetValue.breadcrumbElement = spotDefault.createElement("<div class='breadcrumb breadcrumb-" + facetValue.facet.getUniqueId() + " breadcrumb-" + facetValue.getUniqueId() + "' role='button' id='" + pane.paneId + "-breadcrumb-" + facetValue.getUniqueId() + "'><span class='name'>" + facetValue.getName() + "</span> <span class='remove'>&times;</span></div>");
					facetValue.breadcrumbElement.addEventListener('click', function() { facetValue.enabled(false); });
				}
				pane.breadcrumbsElement.appendChild(facetValue.breadcrumbElement);
				pane.breadcrumbsElement.insertAdjacentElement('beforebegin', this.clearBreadcrumbsElement);
			}).removeBreadcrumb(function(facetValue) {
				facetValue.breadcrumbElement.remove();
				facetValue.breadcrumbElement = null;
				if (pane.breadcrumbs().length == 0)
					this.clearBreadcrumbsElement.remove();
			});
		}

		// Every time a facet is added, hook up all connectors and insert into the DOM.
		this.addFacet(function(facet) {
			facet.element = spotDefault.createElement("<div id='" + pane.paneId + "-facet-" + facet.getUniqueId() + "' class='facet facet-" + facet.getUniqueId() + "' role='listbox'><div class='facet-title'>" + facet.getName() + "</div><div class='facet-values' title='List of " + facet.getName() + " options. Click an option to filter the the products shown.'></div></div>");
			// Allow for us to register a click on a facet element; allowing for us to easily "open/close" the facet via CSS if we want to.
			facet.element.querySelector(".facet-title").addEventListener("click", function(e) {
				if (!facet.element.classList.contains("active")) {
					if (options['singleActive'])
						spotAPI.forEach(spotDOM.facets(), function(f) { f.element.classList.remove("active"); });
					facet.element.classList.add("active");
				} else
					facet.element.classList.remove("active");
			});
			// Every time we get a value for a facet, add it to the list.
			facet.addFacetValue(function(facetValue) {
				// Create a facet value, with a checkbox to indicate whether or not it's active.
				var facetValueElement = pane.paneElement.querySelector("#" + pane.paneId + "-facet-value-" + facetValue.getUniqueId());
				facet.element.classList.remove("inactive");
				var newFacet = false;
				if (!facetValueElement) {
					facetValueElement = spotDefault.createElement("<div class='facet-value facet-value-" + facetValue.getUniqueId() + "' role='option' id='" + pane.paneId + "-facet-value-" + facetValue.getUniqueId() + "' data-value-id='" + facetValue.getUniqueId() + "'><input type='checkbox'/> <span class='name'>" +
						facetValue.getName() + "</span> <span class='count'></span></div>");
					newFacet = true;
				}
				facetValue.element = facetValueElement;

				// Any time a facetValue's count changes, update what it reads.
				facetValue.count(function(count) {
					facetValueElement.querySelector(".count").innerText = count != null && spotDOM.optionBehavior() != "exists" ? "(" + count + ")" : '';
					if (count != null && count == 0) {
						// When we remove a facetValue, make sure to remove the breadcrumb and hide the dom element (this way the order of the elements doesn't jump around arbitrarily).
						if ((options['hideInactiveFacetValues'] === undefined || options['hideInactiveFacetValues']) && !facetValue.enabled()) {
							spotDefault.hideElement(facetValue.element);
							if (facetValue.breadcrumbElement) {
								facetValue.breadcrumbElement.remove();
								facetValue.breadcrumbElement = null;
							}
						}
						facetValue.element.classList.add("inactive");
						if (facet.facetValues().filter(function(e) { return e.count() > 0 }).length == 0 && options['hideInactiveFacets']) {
							spotDefault.hideElement(facet.element);
							facet.element.classList.add("inactive");
						}
					} else {
						spotDefault.showElement(facetValue.element);
						facetValue.element.classList.remove("inactive");
						if (facet.element) {
							facet.element.classList.remove("inactive");
							spotDefault.showElement(facet.element);
						}
					}
				}, true).enabled(function(enabled) {
					// If we're in single selection mode (becuase we're in ShopifyMode, or if this is a sorting facet), DISABLE all other facets in this category.
					window.spotDOM.queryGuard(function() {
						if (facet.singleSelection() && enabled)
							spotAPI.forEach(facet.facetValues(), function(e) { if (e != facetValue) { e.enabled(false); } });
						// If a facet gets enabled, make sure that its checkbox is checked.
						facetValueElement.querySelector("input").checked = enabled;
						facetValueElement.setAttribute("aria-selected", enabled);
						// Add a breadcrumb if enabled, remove its breadcrumb if not, if this facet produces breadcrumbs. This setting is only relevant for panes.
						if (enabled) {
							facetValueElement.classList.add("active");
							facet.element.classList.add("active");
							if (!facet.omitBreadcrumbs && breadcrumbsEnabled)
								pane.addBreadcrumb(facetValue);
						}
						else {
							facetValueElement.classList.remove("active");
							if (facetValue.breadcrumbElement && breadcrumbsEnabled)
								pane.removeBreadcrumb(facetValue);
						}
						// Any time something changes enability, rerun our query. Also, make sure we're on page 1, because we want to reset pagination whenevr we query a facet.
						window.spotDOM.page(1);
						window.spotDOM.query();
					});
				}, facetValue.enabled());
				// If the checkbox is clicked, update the facetValue's enability. Add in a query guard for potential sort facets.
				facetValueElement.addEventListener("click", function(e) { window.spotDOM.queryGuard(function() { facetValue.enabled(!facetValue.enabled()); }) });
				if (newFacet) {
					facet.element.querySelector(".facet-values").appendChild(facetValueElement);
				} else {
					if (facetValue.count() != null)
						facetValueElement.querySelector(".count").innerText = "(" + facetValue.count() + ")";
				}
			}).removeFacetValue(function(facetValue) {
				if (facetValue.element)
					facetValue.element.remove();
				if (facetValue.breadcrumbElement) {
					facetValue.breadcrumbElement.remove();
					facetValue.breadcrumbElement = null;
				}
			});
			// If we have a sorting facet, ensure that we insert before it, rather than after it.
			if (pane.sortingFacet && pane.sortingFacet != facet)
				pane.sortingFacet.element.insertAdjacentElement("afterend", facet.element);
			else
				pane.facetsElement.appendChild(facet.element);
			spotDefault.showElement(pane.paneElement);
		}).removeFacet(function(facet) {
			// When we remove a facet, make sure to remove its dom elements. This shouldn't really ever occur,
			// but if you want to remove things programatically, this allows support for it.
			facet.element.remove();
			if (this.facets().length == 0)
				spotDefault.hideElement(pane.paneElement);
		});

		spotDOM.addFacet(function(facet) {
			pane.addFacet(facet);
		}).removeFacet(function(facet) {
			pane.removeFacet(facet);
		});

		if (options['includeSorting']) {
			// If we are including sorting, wrap sort orders in facet objects, so we use the entirely same stuff.
			// This is NOT added into the general DOM object, only the pane.
			var SortingFacet = function() {
				this.omitBreadcrumbs = true;
				this.getUniqueId = function() { return 'sort_by'; };
				this.singleSelection = function() { return true; }
				this.getName = function() { return spotDefault.locales().sort_by };
				spotDOM.arrayField(this, 'facetValue');
			};
			var SortingFacetValue = function(sortOrder) {
				this.sortOrder = sortOrder;
				this.sortOrder.facetFacade = this;
				this.count = function() { return this; };
				this.getUniqueId = function() { return this.sortOrder.getUniqueId(); };
				this.getName = function() { return this.sortOrder.getName(); };
				spotDOM.scalarField(this, 'enabled', spotDOM.sort() == sortOrder);
			};
			this.sortingFacet = new SortingFacet();
			spotDOM.addSortOrder(function(sortOrder) {
				var facetValue = pane.sortingFacet.addFacetValue(new SortingFacetValue(sortOrder)).enabled(function(enabled) {
					if (!enabled) {
						if (spotDOM.sort() == sortOrder && spotAPI.grep(pane.sortingFacet.facetValues(), function(e) { return e.enabled(); }).length == 0)
							spotDOM.sort(null);
					} else
						spotDOM.sort(sortOrder);

				});
			}).removeSortOrder(function(sortOrder) {
				pane.sortingFacet.removeFacetValue(sortOrder.facetFacade);
			});
			pane.addFacet(this.sortingFacet);
		}
	};
	/** %-{Pane} **/

	/**

	To use a custom pane, you can simply copy the above code from `spot.js`, and paste it into your theme, renaming the pane's class to something appropriate. From there, you can pass a copy instantiated with
	new (e.g. `new CustomPane(paneOptions)`) in as the `pane` option to `SpotDefault.init`. Or, you can simply attach the pane and hook it up manually, if you're so inclined (in that case, see the SpotDefault
	section for details).

	### KeyupListener & SearchBar

	This provides an easy way for you to listen when someone is typing into a text box, and feed those search request into Spot, either for a quick-search, or for automatic page refreshing
	on a collections page. KeyupListener provides the bulk of functionality, SearchBar provides an easy way to create a customized search dropdown.

	A sample way to use this is to hook an input bar into setting the search for SpotDOM, as so:

		new window.spotDefault.KeyupListener(document.querySelector('.productgrid--search-form-field')).doneTyping(function(search) {
			window.spotDOM.search(search);
		});


	The implementation of this is below:

	%{KeyupSearchBar}

	**/

	/** %+{KeyupSearchBar} **/
	this.KeyupListener = function(element) {
		var keyupListener = this;

		spotDOM.scalarField(this, 'element');
		spotDOM.scalarField(this, 'minCharacters', 1);
		spotDOM.scalarField(this, 'interval', 200);
		spotDOM.listener(this, 'doneTyping');
		spotDOM.listener(this, 'off');

		if (!element)
			throw "Requires an element to attach to.";

		this.timeout = null;
		this.lastSearch = element.value;
		this.oldElement = null;

		function debounce(callback, wait) {
			var timeout;
			return (...args) => {
				const context = this;
				clearTimeout(timeout);
				timeout = setTimeout(() => callback.apply(context, args), wait);
			};
		}

		this.handler = function(e) {
			var length = element.value.replace(/\s*/g, "").length;
			if (length > keyupListener.minCharacters()) {
				if (keyupListener.timeout) {
					clearTimeout(keyupListener.timeout);
					keyupListener.timeout = null;
				}
				var timeoutFunction = function() {
					keyupListener.lastSearch = element.value;
					keyupListener.doneTyping(element.value);
				};
				if (keyupListener.lastSearch != element.value)
					keyupListener.timeout = setTimeout(timeoutFunction, keyupListener.interval());
			} else if (length == keyupListener.minCharacters()) {
				if (keyupListener.lastSearch != element.value) {
					keyupListener.lastSearch = element.value;
					keyupListener.doneTyping(element.value);
				}
			} else if (keyupListener.lastSearch != '') {
				keyupListener.lastSearch = '';
				keyupListener.doneTyping('');
			}
		};

		var debounceHandler = debounce(this.handler, 300);

		this.element(function(element) {
			if (this.oldElement)
				this.oldElement.removeEventListener('keyup', this.handler);
			element.autocomplete = 'off';
			element.addEventListener('keyup', debounceHandler);
			this.oldElement = element;
		});
		this.element(element);
	};

	this.SearchBar = function(options) {
		if (!options)
			options = {};
		var inputElement = options['element'];
		var showAll = options['showAllButton'] !== undefined ? options['showAllButton'] : true;
		var searchBar = this;
		this.keyupListener = new spotDefault.KeyupListener(inputElement);
		spotDOM.scalarField(this, 'maxProducts', options['maxProducts'] || 5);

		spotDOM.listener(this, 'getQuery', function(search) {
			return spotDOM.spotAPI.s().paginate(this.maxProducts()).search(search).autoCorrect(spotDOM.autoCorrect()).category("QuickSearch");
		});

		spotDOM.listener(this, 'results', function(search, products, count, options) {
			if (!this.resultsContainer) {
				this.resultsContainer = spotDefault.createElement('<div style="display:none;" class="search-dropdown"></div>');
				this.resultsContainer.style.width = inputElement.offsetWidth + "px";
				inputElement.insertAdjacentElement('afterend', this.resultsContainer);
				this.resultsContainer.style.left = inputElement.offsetLeft + "px";
			}
			this.resultsContainer.innerHTML = '';
			if (products.length > 0) {
				spotAPI.forEach(products, function(product) {
					var url = product.url + "?variant=" + product.variants[0].id;

					searchBar.resultsContainer.appendChild(spotDefault.createElement("<a href='" + url + "' class='product'>\
						<div class='image'>\
						" + (product.featured_image ? "<img src='" + spotDOM.getSizedImage(product.featured_image, "64x64") + "'/>" : '<svg class="placeholder--image" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 525.5 525.5"><path d="M324.5 212.7H203c-1.6 0-2.8 1.3-2.8 2.8V308c0 1.6 1.3 2.8 2.8 2.8h121.6c1.6 0 2.8-1.3 2.8-2.8v-92.5c0-1.6-1.3-2.8-2.9-2.8zm1.1 95.3c0 .6-.5 1.1-1.1 1.1H203c-.6 0-1.1-.5-1.1-1.1v-92.5c0-.6.5-1.1 1.1-1.1h121.6c.6 0 1.1.5 1.1 1.1V308z"></path><path d="M210.4 299.5H240v.1s.1 0 .2-.1h75.2v-76.2h-105v76.2zm1.8-7.2l20-20c1.6-1.6 3.8-2.5 6.1-2.5s4.5.9 6.1 2.5l1.5 1.5 16.8 16.8c-12.9 3.3-20.7 6.3-22.8 7.2h-27.7v-5.5zm101.5-10.1c-20.1 1.7-36.7 4.8-49.1 7.9l-16.9-16.9 26.3-26.3c1.6-1.6 3.8-2.5 6.1-2.5s4.5.9 6.1 2.5l27.5 27.5v7.8zm-68.9 15.5c9.7-3.5 33.9-10.9 68.9-13.8v13.8h-68.9zm68.9-72.7v46.8l-26.2-26.2c-1.9-1.9-4.5-3-7.3-3s-5.4 1.1-7.3 3l-26.3 26.3-.9-.9c-1.9-1.9-4.5-3-7.3-3s-5.4 1.1-7.3 3l-18.8 18.8V225h101.4z"></path><path d="M232.8 254c4.6 0 8.3-3.7 8.3-8.3s-3.7-8.3-8.3-8.3-8.3 3.7-8.3 8.3 3.7 8.3 8.3 8.3zm0-14.9c3.6 0 6.6 2.9 6.6 6.6s-2.9 6.6-6.6 6.6-6.6-2.9-6.6-6.6 3-6.6 6.6-6.6z"></path></svg>') +
						"</div>\
						<div class='info'>\
							<h2 class='title'>"  + product.title + "</h2>\
							<div class='price'>\
								<span class='money' data-currency-usd='" + spotDOM.formatMoney(product.price/100) + "' data-currency='USD'>" + spotDOM.formatMoney(product.price/100) + "</span>\
							</div>\
						</div>\
					</div>"));
				});
				if (showAll)
					searchBar.resultsContainer.appendChild(spotDefault.createElement("<a class='show-all-results' href='/search?q=" + encodeURIComponent(search) + "'>Show all Results</a>"));
				spotDefault.showElement(searchBar.resultsContainer);
			} else {
				spotDefault.hideElement(searchBar.resultsContainer);
			}
		});

		this.keyupListener.doneTyping(function(search) {
			if (!spotDOM.shopifyMode()) {
				searchBar.getQuery(search).e().done(function(products, count, options) {
					searchBar.results(search, products, count, options);
				});
			}
		});


		this.highlightWords = function(suppliedWords, targetWords) {
			if (!Array.isArray(suppliedWords))
				suppliedWords = suppliedWords.split(/\s+/);
			if (!Array.isArray(targetWords))
				targetWords = targetWords.split(/\s+/);
			var highlightedSearch = '';
			var highlightedWords = [];
			for (var i = 0; i < targetWords.length; ++i) {
				highlightedWords[i] = 0;
				for (var j = 0; j < suppliedWords.length; ++j) {
					if (targetWords[i].toLowerCase().indexOf(suppliedWords[j].toLowerCase()) == 0)
						highlightedWords[i] = suppliedWords[j].length;
				}
			}

			for (var i = 0; i < targetWords.length; ++i) {
				if (highlightedWords[i]) {
					highlightedSearch += (i != 0 ? " " : "") + "<span class='text-highlight'>" + targetWords[i].slice(0, highlightedWords[i]) + "</span>" + targetWords[i].slice(highlightedWords[i]);
				} else {
					highlightedSearch += (i != 0 ? " " : "") + targetWords[i];
				}
			}
			return highlightedSearch;
		};
	};
	/** %-{KeyupSearchBar} **/

	/**

	### setupQueryOnChange

	Sets up listeners equivalent to the following, so that new queries are issued when significant attributes change.

	%{setupQueryOnChange}

	This is useful in most setups, though may be omitted if you want to have an 'apply' button that triggers queries. Can be used by simply typing:

		window.spotDefault.setupQueryOnChange();

	**/
	this.setupQueryOnChange = function() {
		/** %+{setupQueryOnChange} **/
		spotDOM.sort(function(sortOrder) {
			this.page(1);
		}).collection(function(collection) {
			this.page(1);
		}).search(function(search) {
			this.page(1);
		}).split(function(split) {
			this.page(1);
		}).paginate(function(resultsPerPage) {
			this.page(1);
		}).currency(function(currency) {
			this.page(1);
		}).locale(function(locale) {
			this.page(1);
		}).page(function(page) {
			this.query();
		});
		/** %-{setupQueryOnChange} **/
	};
	/**

	### needsInitialQuery

	Checks to see whether we're searching anything, have any sort of facets require getting any facets, or do anything
	that needs to be done before showing products to the user. If false, then Spot does not need to be queried to correctly
	render the list of products for this particular collection.

	%{needsInitialQuery}


	**/
	this.needsInitialQuery = function() {
		/** %+{needsInitialQuery} **/
		return spotDOM.sort() || spotDOM.collection() != spotDOM.getCollectionPage() || spotDOM.isSearchPage() || spotDOM.search() || spotAPI.grep(spotAPI.flatMap(spotDOM.facets(), function(f) { return f.facetValues(); }), function(fv) { return fv.enabled(); }).length > 0 || (spotDOM.facets().length > 0 && spotDOM.optionBehavior() != "none");
		/** %-{needsInitialQuery} **/
	};

	/**

	### createElement

	A helper simple function that takes in a string, and returns an HTML element.

	**/
	this.createElement = function(text) {
		return document.createRange().createContextualFragment(text).children[0];
	};

	/**

	### showElement

	A helper simple function that takes in a dom element, and shows it.

	**/
	this.showElement = function(element) {
		var currentDisplay = element.ownerDocument.defaultView.getComputedStyle(element)['display'];
		if (currentDisplay == "none")
			element.style.display = element.dataset.initial_display != null ? element.dataset.initial_display : 'block';
		/*if (element.offsetWidth <= 0 && element.offsetHeight <= 0 && (element.parentNode.offsetWidth > 0 || element.offsetHeight > 0)) {
			if (element.style.display == 'none' && element.dataset.initial_display == null)
				element.style.display = '';
			else
				element.style.display = element.dataset.initial_display != null ? element.dataset.initial_display : 'block';
		}*/
	};
	/**

	### hideElement

	A helper simple function that takes in a dom element, and hides it.

	**/
	this.hideElement = function(element) {
		var currentDisplay = element.ownerDocument.defaultView.getComputedStyle(element)['display'];
		if (currentDisplay != "none") {
			element.dataset.initial_display = element.style.display;
			element.style.display = 'none';
		}
	};


};

/**

### init(options)

The default initailization method. Pass in a bunch of functions, get functional filters in seconds. Takes the following options:

#### clusters

Required. The list of clusters that Spot is present on. Retrieved from `{{ shop.metafields.esafilters.clusters | json }}`

#### sortOrders

The list of sort orders that Spot is supposed to used. Retrieved using `{% if collection and collection.metafields.esafilters.sort_options %}{{ collection.metafields.esafilters.sort_options | json }}{% else %}{{ shop.metafields.esafilters.sort_options | json }}{% endif %}`

#### facets

The list of facets that Spot is supposed to use in this context. Retrieved using `{% if template contains 'search' %}{% if shop.metafields.esafilters.search-faceting %}{{ shop.metafields.esafilters.search-faceting | json }}{% else %}{% if shop.metafields.esafilters.general-faceting %}{{ shop.metafields.esafilters.general-faceting | json  }}{% else %}[]{% endif %}{% endif -%}{%- else -%}{%- if collection and collection.metafields.esafilters.collections-faceting %}{{ collection.metafields.esafilters.collections-faceting | json  }}{% else %}{% if shop.metafields.esafilters.general-faceting %}{{ shop.metafields.esafilters.general-faceting | json  }}{% else %}[]{% endif %}{% endif %}{% endif %}`

#### productContainer

The container that should display products.

#### productRenderer

A function that returns the DOM element for a product in the productContainer.


#### paginationType

The type of pagination to use. Values are as follows:

* `none`: Uses no pagination out of the box.
* `load`: Adds a button into paginationConatiners (or the productContainer, if no pagination containers are specified), that loads more products on click.
* `infinite`: Upon reaching the bottom of the page, more products are loaded.
* `paged`: Fills the paginationContainers with a set of page numbers, and a next and prev button.

#### paginationContainers

The list of containers in which to use pagination. Only valid for `paged` and `load` modes.

#### sortDropdown

The select box that contains your sort orders.

#### searchBars

The search bars. By default, this is hooked into any input that has the name 'q'.

#### searchBarOptions

Options fed to the search bars. Takes a hash, details below.

##### maxProducts

Determines how many products are retrieved by the search bar for instant search. Default is 5.

##### showAllButton

Determines if a "showAllButton" should be added to the search results. Default is false.

##### alwaysDropdown

If set to true, a search dropdown will always pop out regardless of what page the user is on.

If set to false, a search dropdown won't pop out on collection or search pages; instead, searches will refind the existing resultset already being shown. This is the default.

##### deferHookUntilLoad

Defers hooking the search bar until the page is fully loaded. Default false.

#### pane

The facet pane to use. If unspecified, will use the default spot pane. This is where you'd pass in a custom pane (see the pane section for details), if you wanted to use one.

#### paneContainer

The facet pane container to place the pane into. Should be something like an empty div, or larger container that can have elements placed into it. If not specified, no pane is initialized.

#### paneContainerSelectorPosition

Where the facet pane should be placed relative to the container. Default is "beforeend". Uses the enum of values present on insertAdjacentElement.

#### paneOptions

A hash contains that can conatain the following attributes.

##### classes

An array of classes to add to the pane as it's created.

##### singleActive

Ensures that when a user tries to open a facet, all other facets are closed. Useful in horizontal-style facet panes.

##### breadcrumbs

A boolean, determines whether breadcrumbs are used.

##### includeSorting

A boolean; determiens whether or not to include the sort orders as "facets" in the pane. Will not produce breadcrumbs, nor will allow more than one to be selected at any one time.

##### hideInactiveFacetValues

A boolean. Default true. In situations where the countBehaviour is not `none`, if this is true, if a facet value doesn't exist for this resultset, it will be hidden from the display.

##### hideInactiveFacets

A boolean. Default false. In situations where the countBehaviour is not `none`, if this is true, if a facet has no valid facet values for this resultset, it will be hidden from the display.

## Sample Usage

Generally initialized like so:

```html
	{{ "spot.js" | asset_url | script_tag }}
	<script type='text/javascript'>
		SpotDefault.init({
			clusters: {{ shop.metafields.esafilters.clusters | json }},
			facets: {% if template contains 'search' %}{% if shop.metafields.esafilters.search-faceting %}{{ shop.metafields.esafilters.search-faceting | json }}{% else %}{% if shop.metafields.esafilters.general-faceting %}{{ shop.metafields.esafilters.general-faceting | json  }}{% else %}[]{% endif %}{% endif -%}{%- else -%}{%- if collection and collection.metafields.esafilters.collections-faceting %}{{ collection.metafields.esafilters.collections-faceting | json  }}{% else %}{% if shop.metafields.esafilters.general-faceting %}{{ shop.metafields.esafilters.general-faceting | json  }}{% else %}[]{% endif %}{% endif %}{% endif %},
			sortOrders: {% if collection and collection.metafields.esafilters.sort_options %}{{ collection.metafields.esafilters.sort_options | json }}{% else %}{{ shop.metafields.esafilters.sort_options | json }}{% endif %}
			moneyFormat: "{{ shop.money_format }}"
		});
	</script>
```

## FAQ

### How do I make the faceting pane instantiated be horizontal?

This can be achieved entirely with CSS styling, like the following, which will lay out your facet headings to take the entire container, and allow the facet value listing to pop down into its own box that takes up the entire
width of the paneContainer.

```css
	.pane.horizontal .facet.active .facet-values {
		position: absolute;
		background: #fff;
		z-index: 1000;
		left: 0;
		padding: 16px;
		width: 100%;
		text-align: left;
	}
	.pane.horizontal .facet.active .facet-values .facet-value {
		margin-right: 24px;
		margin-bottom: 4px;
		display: inline-block;
	}
	.pane.horizontal .facets {
		display: flex;
		margin: 12px 0;
		position: relative;
	}
	.pane.horizontal .facet-title {
		margin-top: 0;
	}
	.pane.horizontal .facets .facet {
		flex-grow: 1;
		margin: 0 4px;
	}
```

**/
/** %+{SpotDefaultInit} **/
SpotDefault.init = function(options) {
	// ================= STEP 1 =========================
	// Instantiating the APIs and tossing approriate settings on there.
	window.spotAPI = window.spotAPI || new SpotAPI(options.clusters);
	var deferred = spotAPI.Deferred();
	var ready = function(e) {
		if (!options)
			options = {};
		if (!options['clusters'])
			throw "Requires cluster listing from {{ shop.metafields.esafilters.clusters | json }}.";
		window.spotDOM = window.spotDOM || new SpotDOM(spotAPI);
		// If we want to use default HTML/pagination implementations, we do this. For this example, we'll use it.
		window.spotDefault = window.spotDefault || new SpotDefault(window.spotDOM);
		// Useful if multiple domains/domain switches occur.
		if (Shopify.shop)
			spotAPI.defaultQuery(spotAPI.s().hostname(Shopify.shop));
		if (options['moneyFormat'])
			window.spotDOM.moneyFormat(options['moneyFormat']);

		if (options['locales'])
			spotDefault.locales(options['locales']);

		if (options['init'])
			options['init'](window.spotAPI, window.spotDOM, window.spotDefault);

		var searchBars = options['searchBars'] || (options['searchBars'] === undefined ? document.querySelectorAll('input[name="q"]') : []);
		var alwaysDropdown = options['searchBarOptions'] && options['searchBarOptions']['alwaysDropdown'];
		// Only proceed in integrating things if we're actually on a relevant page; i.e. a collection or search page.
		if (window.spotDOM.isSearchPage() || window.spotDOM.isCollectionPage()) {
			// If paginationContainer is null, we'll use infinite scrolling. Otherwise we'll use pagination.
			var paginationContainers = options['paginationContainers'];
			var productContainer = options['productContainer'];
			if (!productContainer)
				throw "Can't find product container.";
			var sortDropdown = options['sortDropdown'] || document.querySelector('select[name="sort_by"]');
			var spellcheckContainers = options['spellcheckContainers'];
			var paneContainer = options['paneContainer'];

			var usesInfiniteScrolling = options['paginationType'] === undefined || (options['paginationType'] || '') == "infinite";
			var usesAutomaticPagination = (options['paginationType'] || '') == "paged";
			var usesLoadMore = (options['paginationType'] || '') == "load";
			var progressiveView = usesLoadMore || usesInfiniteScrolling;

			var productRenderer = options['productRenderer'] || function(product) {
				return window.spotDefault.createElement("<div class='product'>\
					<a class='image' href='" + product.url + "'>\
					" + (product.featured_image ? "<img src='" + spotDOM.getSizedImage(product.featured_image, "275x275") + "'/>" : '<svg class="placeholder--image" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 525.5 525.5"><path d="M324.5 212.7H203c-1.6 0-2.8 1.3-2.8 2.8V308c0 1.6 1.3 2.8 2.8 2.8h121.6c1.6 0 2.8-1.3 2.8-2.8v-92.5c0-1.6-1.3-2.8-2.9-2.8zm1.1 95.3c0 .6-.5 1.1-1.1 1.1H203c-.6 0-1.1-.5-1.1-1.1v-92.5c0-.6.5-1.1 1.1-1.1h121.6c.6 0 1.1.5 1.1 1.1V308z"></path><path d="M210.4 299.5H240v.1s.1 0 .2-.1h75.2v-76.2h-105v76.2zm1.8-7.2l20-20c1.6-1.6 3.8-2.5 6.1-2.5s4.5.9 6.1 2.5l1.5 1.5 16.8 16.8c-12.9 3.3-20.7 6.3-22.8 7.2h-27.7v-5.5zm101.5-10.1c-20.1 1.7-36.7 4.8-49.1 7.9l-16.9-16.9 26.3-26.3c1.6-1.6 3.8-2.5 6.1-2.5s4.5.9 6.1 2.5l27.5 27.5v7.8zm-68.9 15.5c9.7-3.5 33.9-10.9 68.9-13.8v13.8h-68.9zm68.9-72.7v46.8l-26.2-26.2c-1.9-1.9-4.5-3-7.3-3s-5.4 1.1-7.3 3l-26.3 26.3-.9-.9c-1.9-1.9-4.5-3-7.3-3s-5.4 1.1-7.3 3l-18.8 18.8V225h101.4z"></path><path d="M232.8 254c4.6 0 8.3-3.7 8.3-8.3s-3.7-8.3-8.3-8.3-8.3 3.7-8.3 8.3 3.7 8.3 8.3 8.3zm0-14.9c3.6 0 6.6 2.9 6.6 6.6s-2.9 6.6-6.6 6.6-6.6-2.9-6.6-6.6 3-6.6 6.6-6.6z"></path></svg>') + "\
					</a>\
					<div class='info'>\
						<div class='productitem--price'>\
							<span class='money' data-currency-usd='" + spotDOM.formatMoney(product.price/100) + "' data-currency='USD'>" + spotDOM.formatMoney(product.price/100) + "</span>\
						</div>\
						<h2 class='productitem--title'>\
							<a href='" + product.url + "?variant=" + product.variants[0].id + "' tabindex='1'>\
								" + (options['split'] && options['split'] == "none" ? product.title : spotDOM.getProductTitle(product)) + "\
							</a>\
						</h2>\
						<div class='productitem--description'>\
							<p>" + product.description + "</p>\
							<a href='" + product.url + "?variant=" + product.variants[0].id + "' class='productitem--link'>\
									" + spotDefault.locales().view_details + "\
							</a>\
						</div>\
					</div>\
				</div>");
			};


			// Issue a query guard directive; ensure that within this block, only a single query is run at the end of the block.
			// This is used so that setup can happen, checkboxes can be ticked, without actually triggering unecessary filtration.
			window.spotDOM.queryGuard(function() {
				var currentPage = spotDOM.page();
				// ================= STEP 2 =========================
				// Choosing how to render products onto a page when we get them.
				window.spotDOM.products(function(products) {
					if (!progressiveView || this.page() == 1)
						productContainer.innerHTML = '';
					for (var i = 0; i < products.length; ++i)
						productContainer.appendChild(productRenderer(products[i]));
				});
				// ================= STEP 3 =========================
				// Getting a pane HTML element, and attaching it to your page.
				if (paneContainer) {
					window.spotPane = options['pane'] || new window.spotDefault.Pane(options['paneOptions']);
					paneContainer.insertAdjacentElement(options['paneContainerSelectorPosition'] || "beforeend", window.spotPane.paneElement);
					// ================= STEP 4 =========================
					// This will pull facets from the Spot control panel.
					// Here we actually add these facets to our DOM module so that it knows what it should be looking for.
					for (var i = 0; i < options.facets.length; ++i) {
						if (typeof(options.facets[i]) != "object" || options.facets[i].active)
							window.spotDOM.addFacet(new spotDOM.Facet(options.facets[i])).init();
					}
				}
				// ================= STEP 5 =========================
				// Here's an easy way to write pagination to the page when a query changes/is made for the first time.
				if (usesAutomaticPagination) {
					window.spotDOM.count(function(count) {
						// If we're using paged pagination, render pagination.
						spotAPI.forEach(paginationContainers, function(paginationContainer) {
							paginationContainer.innerHTML = '';
							paginationContainer.appendChild(window.spotDefault.pagedPaginationHelper({ count: count }))
						});
					});
				} else if (usesInfiniteScrolling) {
					new window.spotDefault.ScrollPaginationHelper();
				} else if (usesLoadMore) {
					var createButton = function() {
						var button = spotDefault.createElement(`<button class='spot-load-more'>${spotDefault.locales().load_more}</button>`);
						button.addEventListener("click", function(e) { spotDOM.page(spotDOM.page()+1); e.preventDefault(); });
						return button;
          };
          var createViewAllButton = function() {
						var button = spotDefault.createElement(`<button class='spot-view-all'>${spotDefault.locales().show_all}</button>`);
						button.addEventListener("click", function(e) { spotDOM.page(spotDOM.page()+1); usesInfiniteScrolling = true; usesLoadMore = false; new window.spotDefault.ScrollPaginationHelper({'triggerHeight': 600}); document.querySelector('.spot-load-more').remove(); document.querySelector('.spot-view-all').remove(); e.preventDefault(); });
						return button;
					};
					var appendButton = function() {
						if (paginationContainers.length > 0) {
							spotAPI.forEach(paginationContainers, function(paginationContainer) {
								paginationContainer.innerHTML = '';
								paginationContainer.appendChild(createButton());
                paginationContainer.appendChild(createViewAllButton());
							});
						} else {
							productContainer.appendChild(createButton());
              productContainer.appendChild(createViewAllButton());
            }
					};
					spotDOM.products(function(products) {
						spotAPI.forEach(document.querySelectorAll('.spot-load-more'), function(e) { e.remove(); });
						if (products.length == spotDOM.paginate() && usesLoadMore) {
							appendButton();
						}
					});
					if (options['currentCount'] && options['currentCount'] > spotDOM.paginate())
						appendButton();
				}
				// In the case where we load things up; we're required to progress through the various pages in order to
				// load our position correctly, previously.
				if (usesInfiniteScrolling || usesLoadMore) {
					if (currentPage > 1) {
						var countUp = function(page) {
							if (page <= currentPage) {
								spotDOM.queryGuard(function() {
									spotDOM.page(page);
								}).done(function() {
									countUp(page+1);
								});
							}
						};
						countUp(1);
					}
				} else {
					// Reset our pagination to what it should be in the case a facet was applied, which resets page counts.
					if (spotDOM.page() != currentPage)
						spotDOM.page(currentPage);
				}
				// ================= STEP 6 =========================
				// For our sort dropdown, we're going to populate it with all the default Shopify sort orders, or hook them up to already existing values.
				if (sortDropdown) {
					window.spotDOM.addSortOrder(function(sortOrder) {
						sortOrder.element = sortDropdown.querySelector('option[value="' + sortOrder.getUniqueId() + '"]');
						if (!sortOrder.element) {
							sortOrder.element = spotDefault.createElement("<option value='" + sortOrder.getUniqueId() + "'>" + sortOrder.getName() + "</option>");
							sortDropdown.appendChild(sortOrder.element);
						}
					}).removeSortOrder(function(sortOrder) {
						sortOrder.element.remove();
					});
					sortDropdown.addEventListener("change", function(e) {
						window.spotDOM.sort(this.value);
						e.preventDefault();
					});
					window.spotDOM.sort(function(sortOrder) {
						spotAPI.forEach(sortDropdown.querySelectorAll('option'), function(e) { e.removeAttribute('selected'); });
						if (sortOrder.element)
							sortOrder.element.selected = true;
					});
				}
				// Initialize with our default sort orders on a per shop, or per collection basis, or if not present, ues defaults.
				if (options.sortOrders)
					spotAPI.forEach(spotAPI.grep(spotAPI.map(spotAPI.grep(options.sortOrders, function(e) { return e['active'] !== false; }), function(e) { return new window.spotDOM.SortOrder(e); }), function(e) { return e.isRelevant(); }), function(order) { window.spotDOM.addSortOrder(order) })
				else
					window.spotDOM.initDefaultSortOrders();
				// ================= STEP 7 =========================
				// Polishing! A bunch of small stuff.
				// If we have a spellcheck that occurs, display it in the appropriate DOM element.
				if (spellcheckContainers && spellcheckContainers.length) {
					window.spotDOM.spellCheck(function(spellCheck){
						spotAPI.forEach(spellcheckContainers, function(spellcheckContainer) {
							if (spellCheck) {
								window.spotDefault.showElement(spellcheckContainer);
								spellingCorrectionContainer.innerHTML = "<p class='corrected'>" + spotDefault.locales().show_results + "<a href='/search?q=" + encodeURIComponent(spellCheck.corrected) + "'>" + window.spotAPI.map(spellCheck.words, function(e) { if (e.corrected != e.original) { return "<b>" + e.corrected + "</b>"; } return e.corrected; }).join(" ") + "</a>.</p>";
							} else {
								window.spotDefault.hideElement(spellcheckContainer);
							}
						});
					});
				}
				window.spotDOM.query(function(products, count, options) {
					spotDOM.updateQueryString({});
				}).search(function() {
					// Update the search bars if we make a search query, and do it immediately.
					spotAPI.forEach(searchBars, function(e) { e.value = window.spotDOM.search(); });
				}, true);
				window.spotDefault.setupQueryOnChange();
				// If there are reasons to trigger an initial query, trigger one.
				if (window.spotDefault.needsInitialQuery())
					window.spotDOM.query();

				// ================= OPTIONAL STEP 1+2 =========================
				// Deal with searching; in this case, let's hook up the search bar, if we're on a collections page to search the actual collection.
				// If we're on the search page, it'll simply search as normal.
				if (!spotDOM.shopifyMode() && !alwaysDropdown) {
					var searchBarOptions = options['searchBarOptions'];
					var setupSearchBars = function() {
						window.spotAPI.forEach(searchBars, function(e) {
							new window.spotDefault.KeyupListener(e).doneTyping(function(search) {
								window.spotDOM.search(search);
							});
						});
					};
					if (searchBarOptions && searchBarOptions['deferHookUntilLoad'])
						window.addEventListener('load', setupSearchBars);
					else
						setupSearchBars();
				}
			});
		}
		if ((!window.spotDOM.isSearchPage() && !window.spotDOM.isCollectionPage()) || alwaysDropdown) {
			// If we're NOT on a collections or search page, then have the bar stick out a dropdown. (Or if there's a setting override).
			var searchBarOptions = options['searchBarOptions'];
			var setupSearchBars = function() {
				window.spotAPI.forEach(searchBars, function(e) {
					searchBarOptions['element'] = e;
					new window.spotDefault.SearchBar(searchBarOptions);
				});
			};
			if (searchBarOptions && searchBarOptions['deferHookUntilLoad'])
				window.addEventListener('load', setupSearchBars);
			else
				setupSearchBars();
		}
		deferred.resolve();
	};
	if (document.readyState === "complete" || document.readyState === "loaded" || document.readyState === "interactive")
		ready();
	else
		document.addEventListener('DOMContentLoaded', ready);
	return deferred;
};
/** %-{SpotDefaultInit} **/

/**

## Implementation Checklist / Sample Implementation

This is probably where you want to start if you're looking to hit the ground runnning if you're looking to do a custom implemetnation. Before you dive in, you should choose how to structure your Spot code.
Normally, the standard way to do this is to stick all of your code in a snippet, and include that snippet just before the closing body tag of your `theme.liquid` file.
Inside this snippet, you'll write your javascript as single function block, and then have that block run only after document ready. This is the suggested method
of inclusion, but there is no hard requirement, you may call it how you like.

Adding the code below will throw together a default implementation of Spot onto the site. When you implement Spot, the following issues are what you should be tackling, roughly, in order.
The sample code illustrates how each of these is handled; there are corresponding step indicators in the sample code.

### Checklist

1. Instantitate a copy of `SpotAPI`, `SpotDOM` and `SpotDefault`, with the appropriate credentials, by calling `new` on each in order. Ensure that you point Spot to your results container, sort dropdown, and pagination container.
2. Provide a function to `SpotDOM` which takes a JSON representation of the store's products using `.products`, and renders that onto your collections/search results page.
3. Determine where, and how you're going to attach the faceting pane to your collections/search page, and do so by creating it with `new spotDefault.Pane()`.
4. Instantiate what facets you're going to want to show, and register them with `SpotDOM`.
5. Implement how you're going to paginate your results (with infinite scrolling, or with page numbers), generally by hooking the `.count` scalar.
6. Determine how, if at all, you're going to let the user change the sort order, and populate/set the list of sort orders using the array adders, or `spotDOM.initDefaultSortOptions()`.
7. Add in polishing elements. A generic list of things to look out for is below.
	* After each query, make sure the query string is up to date.
	* If you're using spelling correction (and you should most of the time; `.autoCorrect(true)`), consider displaying the result of what the user's search query was corrected to, through the `SpotDOM.spellCheck` hook.
	* Make the filtering/faceting "snappy", by ensure that a new query runs after most filtering changes. (See the sample implementation below).
	* Make sure that when you link any product, that if you're not specifying `allVariants(true)`, that you link to the first relevant variant, as a direct link to the product page may link to a variant the user isn't looking at.
	* When implementing a quick search bar, if you're doing search-as-you-go, ensure that you have some mechanism to either throttle the user's queries, so the user's screend doesn't thrash around (you can use `SpotDefault.keyupListener`). If this is desired behavior, then you'll need to make sure instead that you handle queries in the order in which they were made, not the order which they return. If you do not, when a user stops typing, they could have results for something that wasn't their last search. For example, if a user has typed "shi", and then "shirt", and "shirt" returns before "shi" (likely because shi contains lots more product data then shirt), when the user stops typing, they'll see results for "shi", instead of "shirt", which isn't what the user wants.
	* If you want to only show things that are in stock,
8. Style everything.

### Optional Steps

1. Test with `.shopifyMode(true)` (see [here](#shopifymode) for details), and seeing if the system still works satisfactorily with Spot disabled.
2. Determine if/how you're going to have the search bar of the site interact with your collections page, and tying in a quick-search box, with search-as-you-go functionality.

Your implementation for all of this should ideally be no more than a few hundred lines of javascript.

You can see an example of this implementation in the normal initialization function, called by the Spot section. See above.

%{SpotDefaultInit}

**/


/***

Spot Endpoint Documentation
===========================

This SDK serves as layer of abstraction infront of an endpoint.

To contact Spot directly, all you need to do is send HTTP(S) requests to a particular server. Exactly what servers hold your shop's data is listed in the shop-based metafield that has a namespace of `esafilters` and
a key of `clusters`. This metafield contains an array of at least one hostname. Requests made to the Spot endpoint are load-balanced behind these hostnames.

Spot requests can be served plain, but is encouraged to pass `Accept-Encoding: deflate` as a header in order to take advantage of Spot's zipping capability to make responses smaller, and to take advantage of more aggressive caching.

## Endpoint

Spot queries can be either `POST`s or `GET`s. In a `GET`, Spot will accept the query body in the `q` parameter, or as the body of a `POST` request. Spot supports a maximum of ~8kb in terms of header size, so any apps that have the potential
to submit long queries should be using `POST`, rather than `GET`.

The following query parameters are applicable to both `GET` and `POST` requests:

* `hostname`: Can be used to specify the .myshopify.com hostname of the shop you're looking to query. If not supplied, is inferred from the `Referrer` header.

## Query Structure

Queries are submitted in JSON (RFC8259). Each query consists of a dictionary with exactly two keys, `query`, and `attributes`.

### Query

An array. Contains all expressions that are to be AND'd together. Query expressions come in two flavours, compound or simple.

#### Simple Expressions

Take the form of a dictionary that specifies the field to be filtered as a key, an operator, and the desired operand to be checked against. The operator is optional, and can be omitted for simple equality. If the operator is present,
it forms an additional dictionary object to be passed as the sole value of field key.

	{"inventory_quantity":{">":0}}

	{"price":{"<=":10}}

	{"title":"Exactly This Product Title"}
	{"title":{"==":"Exactly This Product Title"}}

	{"tag":"red"}
	{"tag":{"==":"red"}}

	{"option1":{"!=":"Large"}}

Some fields are *specified* fields, which means that they require additional specification to determine what field you're talking about. This is done by recursively nesting dictionaries, like so:

	{"option":{"Color":{"==":"Red"}}}

	{"product-metafields":{"mynamespace":{"mykey":{">":0}}}}

	{"variant-metafields":{"mynamespace":{"mykey":{">":0}}}}

#### Compound Expressions

These expressions can contain other expressions. There are a number of these.

##### or

OR's together the various sub-expressions.

	{"or":[{"option":{"Color":"Red"}},{"product_type":"Shirt"}]}

##### and

AND's together the various sub-expressions. Implied in the opening array, but useful if ANDs and ORs are nested.

	{"and":[{"price":{"<":20},{"price":{">":10}]}

##### facets

Specifies which of your filters apply to your facets. Enables the use of disjuctive facets.

	{"facets":[{"or":[]},{"or":[]},{"or":[{"option":{"Color":"Red"}}]}]}

##### all

Specifies that only products that have *all* variants present as a result of the subquery should be returned.

	{"all":[{"option":{"Color":"Red"}}]}

##### not

Specifies that only products that do not match the subquery sould be returned.

	{"not":[{"option":{"Color":"Red"}}]}

##### any

Reverts the behaviour of `all` back to the default `any` inside an `all` subexpression.

#### Query Fields

There are many of these. This list is *non-exhaustive*. It includes every relevant property on Product or Variant, as they are named in Shopify's REST api.

##### Product Fields

* `product_type`
* `vendor`
* `title`
* `handle`
* `collection`
* `product-metafield` (qualified; *can be treated as numeric*)

##### Variant Fields

###### Numeric Fields

* `inventory_quantity`
* `price`

###### String Fields

* `available` (Definition is exactly as in Shopify's liquid reference; this should be used over `inventory_quantity > 0` to determine availability of a product).
* `option1`
* `option2`
* `option3`
* `option` (qualified)
* `variant-metafield` (qualified; *can be treated as numeric*)

#### Query Operators

Query operators come in two flavours, numeric operators, or string operators.

##### String Operators

* `==` : Equality.
* `!=` : Inequality.
* `^` : Starts With
* `in` : Checks if within a specified array.
* `not_in`: Checks if not within a specified array.

##### Numeric Operators

* `<`
* `>`
* `>=`
* `<=`
* `==`
* `!=`

### Attributes

Attributes are meta-information about the query that affect the query, and the results returned. The following is a non-exhaustive list of all attributes.

#### count (boolean, default: false)

Determines whether or not Spot should return the total amount of products in the resultset. Can result in longer query times if enabled.

#### countBehavior (string, default: 'approximate')

[See here](#countbehavior).

#### options (array, default: [])

Provides the list of facets you'd like to facet on. Facets are specified as in the Query Fields section. If you'd like to specify a list of particular facets of a given type (say, a list of tags instead of all tags on the store), you can specify this in the form of an array.

	{"options":["product_type","vendor",{"option":"Color"},{"option":"Size"},{"tag":["sale","merch","spring","summer","fall","winter"]}]

The order of these options is relevant, as it will affect your `facets` compound specifier if you use one.

In addition to simple specifiers, one can be more descriptive and group values together into single facets.

	{"options":[{"tag":["sale","merch",{"name":"seasonal","value":["spring","summer","fall","winter"]}]]}}

#### optionBehavior (string, default: 'approximate')

[See here](#optionbehavior).

#### optionDisjunctive (bool, default: true)

Determine whether your facets are disjuntive or conjunctive. [See the entry on disjucntive facet counts for details](#disjunctive-facet-counts).

#### search (string, default: null)

A free-form search string to search through the entire object; all query fields, as well as the body_html. Intelligently performs stemming based on locale. If not empty string or null, can be used with the `search` sort order,
as well as with the `autoCorrect` and `spellCheck` attributes.

#### collection (string, default: null)

A string representing a collection handle, for which this query should operate in the context of. Will only show products relevant to the desired collection, and will fill if not set the `sort` as being whatever the collection's
default is. If a collection does not exist, a warning will be generated, but a full resultset for the entire store will be returned. Also allows for the `manual` sort order, if applicable.

#### sort (string/object, default: null)

A string or object representing the order you'd like to sort the resultset by. In addition to custom sort orders, we support all Shopify sort orders, in all the myriad ways they're spelled, and defined.

If a string, the following strings, in addition to any named sort orders specified in the Spot backend are accepted:

* `created`
* `created-asc`
* `created-desc`
* `title`
* `title-asc`
* `title-desc`
* `alpha`
* `alpha-asc`
* `alpha-desc`
* `bestselling` (If order-reading is enabled in the Spot backend)
* `manual` (If sorting through a collection).
* `search` (If searching, orders by search relevancy).
* `price`
* `price-asc`
* `price-desc`
* Identifier for any custom sort order you've defined.

If an object, you can specify a sort direction (asc/desc), followed the field name.

	{"asc":"price"}

	{"desc":"title"}

In addition, you can also specify complex sort orders that you've defined on the Spot back-end this way.

	{"desc":{"product-metafield":{"mynamespace":"mykey"}}}

#### autoCorrect (boolean, default: false)

If true, will return a spellcheck in the response, based on the search string, as well as automatically correct the search to this spellcheck if there are no results with the initial search.

#### spellCheck (boolean, default: false)

If true, will return a spellcheck in the response, based on the search string.

#### rows (int, default: 12)

Specifies the amount of results to retrieve. Maximum of 100. (1000 is currently supported, but may be revoked depending on performance characteristics).

#### page (int, default: 1)

Specifies the page of results to retrieve.

#### fields (array[string], default: null)

Specifies the field list to retrieve. If not specified, will return the entire object. As an example:

	{"fields":["title","product_type","images"]}

#### split (string, default: "none")

Specifies the product split.

* `none`: Returns products as Shopify has them.
* `auto`: Returns the most appropriate split, given what you have set up in the Spot control panel, and your query.
* `<id>`: Returns the specifier split by identifier from the Spot control panel.

#### allVariants (bool, default: true)

If true, returns all variants for every product, as Shopify does. If false, returns only variants relevant to the filtering/searching criteira specified.

#### locale (string, default: shop default)

Specifies the locale to return products in if they're using Shopify's official translation system. Under construction; unstable.

#### currency (object, default: null)

Specifies the currency. Translates the incoming query to use prices converted by the specifies currenc. Under construction; unstable.

#### popularSearches (int, default: 0)

Specifies the amount of popular searches to return. Under construction; unstable.

## Examples

The following are sample full requests, and response pairs.

Simple request to grab "id" and "title" of two products.

	{"query":[],"attributes":{"fields":["id","title"],"rows":2}}

	Request

	> GET /?q=%7B%22query%22%3A%5B%5D%2C%22attributes%22%3A%7B%22fields%22%3A%5B%22id%22%2C%22title%22%5D%2C%22rows%22%3A2%7D%7D%0A HTTP/1.1
	> Host: spot-cluster-ca-0.moddapps.com
	> User-Agent: curl/7.47.0
	> Accept: application/json
	> Accept-Encoding: deflate, gzip
	> Referer: https://spot-development.myshopify.com

	Response

	< HTTP/1.1 200 OK
	< Content-Length: 135
	< Access-Control-Expose-Headers: X-Session-Id
	< Access-Control-Allow-Headers: X-Shopify-Shop-Domain,Content-Type,Accept,X-Requested-With,X-Session-Id
	< Access-Control-Allow-Credentials: true
	< Access-Control-Allow-Method: GET,POST
	< Access-Control-Allow-Origin: https://spot-development.myshopify.com
	< Content-Type: application/json; charset=UTF-8
	< X-Session-Id: 8b187da44afe7c04e1bc0d0d537e2abd
	< Content-Encoding: deflate
	< Date: Thu, 13 Aug 2020 01:33:10 GMT
	< Via: 1.1 google
	< Alt-Svc: clear
	{"products":[{"title":"Zoo York Zombie Skateboard 8\"","id":"4434463326319"},{"id":"4434463195247","title":"Zoo York Womens Unbreakable Chenille Cropped T-Shirt"}]}

Get the first page of 2 available products, and none of the irrelevant variants, that have the tag "language_en", and correspond to the search "hat", as well as have the Size "O/S". Get an exact count returned.

	{
	"query":[
		{"or": [{"available":{"==":true}},{"tag":{"==":"badge::Coming-Soon"}}]},
		{"available":{"==":true}},
		{"tag":{"==":"language_en"}},
		{"option":{"Size":"O/S"}}
	],
	"attributes":
	{"count":true,"countBehavior":"exact","allVariants":false,"fields":["id","handle","title","product_type","vendor","tags","images","options","variants","metafields"],"rows":2,"page":1,"options":[],"optionDisjunctive":true,"search":"hat"}}

	Request

	> GET /?q=%7B%22query%22%3A%5B%7B%22available%22%3A%7B%22%3D%3D%22%3Atrue%7D%7D%2C%7B%22tag%22%3A%7B%22%3D%3D%22%3A%22language_en%22%7D%7D%2C%7B%22option%22%3A%7B%22Size%22%3A%22O%2FS%22%7D%7D%5D%2C%22attributes%22%3A%7B%22count%22%3Atrue%2C%22countBehavior%22%3A%22exact%22%2C%22allVariants%22%3Afalse%2C%22fields%22%3A%5B%22id%22%2C%22handle%22%2C%22title%22%2C%22product_type%22%2C%22vendor%22%2C%22tags%22%2C%22images%22%2C%22options%22%2C%22variants%22%2C%22metafields%22%5D%2C%22rows%22%3A2%2C%22page%22%3A1%2C%22options%22%3A%5B%5D%2C%22optionDisjunctive%22%3Atrue%2C%22search%22%3A%22hat%22%7D%7D%0A HTTP/1.1
	> Host: spot-cluster-ca-0.moddapps.com
	> User-Agent: curl/7.47.0
	> Accept: application/json
	> Accept-Encoding: deflate, gzip
	> Referer: https://spot-development.myshopify.com

	Response

	< HTTP/1.1 200 OK
	< Content-Length: 1642
	< Access-Control-Expose-Headers: X-Session-Id
	< Access-Control-Allow-Headers: X-Shopify-Shop-Domain,Content-Type,Accept,X-Requested-With,X-Session-Id
	< Access-Control-Allow-Credentials: true
	< Access-Control-Allow-Method: GET,POST
	< Access-Control-Allow-Origin: https://spot-development.myshopify.com
	< Content-Type: application/json; charset=UTF-8
	< X-Session-Id: b57219253b5961a953f6ed12567d462a
	< Content-Encoding: deflate
	< Date: Thu, 13 Aug 2020 01:43:37 GMT
	< Via: 1.1 google
	< Alt-Svc: clear

	{"products":[{"handle":"1648-18958383-zoo-york-mens-graphic-trucker-hat","images":[{"position":1,"updated_at":"2020-04-28T00:41:14-04:00","id":13932966969455,"alt":null,"height":2000,"width":1333,"product_id":4433605230703,"created_at":"2020-04-28T00:41:14-04:00","src":"https://cdn.shopify.com/s/files/1/0271/5952/7535/products/164818958383-69-0.jpg?v=1588048874","admin_graphql_api_id":"gid://shopify/ProductImage/13932966969455","variant_ids":[31478042198127]},{"updated_at":"2020-04-28T00:41:16-04:00","position":2,"id":13932967559279,"alt":null,"height":2000,"admin_graphql_api_id":"gid://shopify/ProductImage/13932967559279","variant_ids":[],"product_id":4433605230703,"created_at":"2020-04-28T00:41:16-04:00","width":1333,"src":"https://cdn.shopify.com/s/files/1/0271/5952/7535/products/164818958383-69-1.jpg?v=1588048876"},{"admin_graphql_api_id":"gid://shopify/ProductImage/13932967592047","variant_ids":[31478042230895],"width":1333,"product_id":4433605230703,"created_at":"2020-04-28T00:41:18-04:00","src":"https://cdn.shopify.com/s/files/1/0271/5952/7535/products/164818958383-40.jpg?v=1588048878","updated_at":"2020-04-28T00:41:18-04:00","position":3,"id":13932967592047,"alt":null,"height":2000},{"admin_graphql_api_id":"gid://shopify/ProductImage/13932967624815","variant_ids":[],"src":"https://cdn.shopify.com/s/files/1/0271/5952/7535/products/164818958383-40-1.jpg?v=1588048880","width":1333,"product_id":4433605230703,"created_at":"2020-04-28T00:41:20-04:00","id":13932967624815,"position":4,"updated_at":"2020-04-28T00:41:20-04:00","height":2000,"alt":null}],"metafields":[],"variants":[{"grams":0,"id":"31478042230895","position":2,"inventory_policy":"deny","admin_graphql_api_id":"gid://shopify/ProductVariant/31478042230895","image_id":13932967592047,"option3":null,"created_at":"2020-04-28T00:41:13-04:00","fulfillment_service":"manual","inventory_quantity":16,"barcode":"61953923","compare_at_price":"11.99","requires_shipping":true,"option2":"O/S","inventory_item_id":33129781821551,"option1":"Blue","updated_at":"2020-04-28T00:41:18-04:00","weight_unit":"lb","weight":0,"title":"Blue / O/S","metafields":[],"taxable":true,"sku":"1648-18958383-401010","product_id":4433605230703,"old_inventory_quantity":16,"price":"11.99","inventory_management":"shopify"}],"title":"Zoo York Mens Graphic Trucker Hat","vendor":"Zoo York","options":[{"position":1,"name":"Color","product_id":4433605230703,"id":5769550102639,"values":["Coral","Blue"]},{"values":["O/S"],"id":5769550135407,"position":2,"product_id":4433605230703,"name":"Size"}],"id":"4433605230703","tags":"brand amnesia, brand west49, gender mens, language_en, locale_ca, mar10, new, private label, promo 50off, promo bogo5, spring, style flamingo, style palm tree, subtype hats, type accessories","product_type":"Mens Hats"},{"metafields":[],"variants":[{"price":"11.99","inventory_management":"shopify","old_inventory_quantity":13,"product_id":4433517772911,"taxable":true,"sku":"2748-18958383-401010","metafields":[],"title":"Blue / O/S","weight_unit":"lb","weight":0,"option1":"Blue","updated_at":"2020-04-27T23:01:52-04:00","inventory_item_id":33129115254895,"requires_shipping":true,"option2":"O/S","fulfillment_service":"manual","inventory_quantity":13,"compare_at_price":"11.99","barcode":"61959516","option3":null,"created_at":"2020-04-27T23:01:50-04:00","admin_graphql_api_id":"gid://shopify/ProductVariant/31477430812783","image_id":13932622250095,"inventory_policy":"deny","id":"31477430812783","position":1,"grams":0},{"inventory_policy":"deny","grams":0,"position":2,"id":"31477430845551","compare_at_price":"11.99","barcode":"61957270","fulfillment_service":"manual","inventory_quantity":16,"option2":"O/S","requires_shipping":true,"image_id":13932622348399,"admin_graphql_api_id":"gid://shopify/ProductVariant/31477430845551","created_at":"2020-04-27T23:01:50-04:00","option3":null,"title":"Coral / O/S","weight":0,"weight_unit":"lb","metafields":[],"inventory_item_id":33129115287663,"updated_at":"2020-04-27T23:01:56-04:00","option1":"Coral","inventory_management":"shopify","price":"11.99","sku":"2748-18958383-691010","taxable":true,"old_inventory_quantity":16,"product_id":4433517772911}],"vendor":"Zoo York","title":"Zoo York Boys Graphic Trucker Hat","handle":"2748-18958383-zoo-york-boys-graphic-trucker-hat","images":[{"admin_graphql_api_id":"gid://shopify/ProductImage/13932622250095","variant_ids":[31477430812783],"src":"https://cdn.shopify.com/s/files/1/0271/5952/7535/products/274818958383-40-0.jpg?v=1588042912","width":1333,"product_id":4433517772911,"created_at":"2020-04-27T23:01:52-04:00","id":13932622250095,"updated_at":"2020-04-27T23:01:52-04:00","position":1,"height":2000,"alt":null},{"id":13932622315631,"updated_at":"2020-04-27T23:01:54-04:00","position":2,"alt":null,"height":2000,"variant_ids":[],"admin_graphql_api_id":"gid://shopify/ProductImage/13932622315631","src":"https://cdn.shopify.com/s/files/1/0271/5952/7535/products/274818958383-40-1.jpg?v=1588042914","width":1333,"product_id":4433517772911,"created_at":"2020-04-27T23:01:54-04:00"},{"id":13932622348399,"updated_at":"2020-04-27T23:01:56-04:00","position":3,"height":2000,"alt":null,"admin_graphql_api_id":"gid://shopify/ProductImage/13932622348399","variant_ids":[31477430845551],"src":"https://cdn.shopify.com/s/files/1/0271/5952/7535/products/274818958383-69.jpg?v=1588042916","width":1333,"created_at":"2020-04-27T23:01:56-04:00","product_id":4433517772911},{"height":2000,"alt":null,"position":4,"updated_at":"2020-04-27T23:01:58-04:00","id":13932622381167,"variant_ids":[],"admin_graphql_api_id":"gid://shopify/ProductImage/13932622381167","width":1333,"product_id":4433517772911,"created_at":"2020-04-27T23:01:58-04:00","src":"https://cdn.shopify.com/s/files/1/0271/5952/7535/products/274818958383-69-1.jpg?v=1588042918"}],"options":[{"values":["Blue","Coral"],"name":"Color","product_id":4433517772911,"position":1,"id":5769436135535},{"id":5769436168303,"position":2,"product_id":4433517772911,"name":"Size","values":["O/S"]}],"product_type":"Boys Hats","id":"4433517772911","tags":"brand amnesia, brand west49, gender boys, language_en, locale_ca, mar10, new, private label, promo 50off, promo bogo5, spring, style flamingo, style palm tree, subtype hats, type accessories"}],"count":22,"spellCheck":{"original":"hat","corrected":"hat","words":[{"original":"hat","corrected":"hat"}]}}

Ask for the first two products that are either "Baby Blue","Blue","Dark Blue","Denim Blue","Grey Blue","Light Blue","Light Denim Blue","Medium Blue","Medium Denim Blue","Midnight Blue","Ocean Blue","Royal Blue","Sky Blue","Navy","Turquoise","Aqua" or "Teal". Get an exact count, as well as a list of facets with their counts, that are "Color", "Size"," Price", english/french tags, as well as "subtype". "Color" groups together many colors into one, as does size. Price lists an array of ranges, english and french are two specific tags, and subtypes are another specific group of tags. All counts are approximate. Asking for the currency in Canadian dollars. Ask for whatever split is most appropriate to this query.

	Request

	> POST / HTTP/1.1
	> Host: spot-cluster-ca-0.moddapps.com
	> User-Agent: curl/7.47.0
	> Accept: application/json
	> Accept-Encoding: deflate, gzip
	> Referer: https://spot-development.myshopify.com
	> Content-Length: 2921
	> Content-Type: application/json; charset=UTF-8

	{"query":[{"facets":[{"or":[{"or":[{"option":{"Color":{"==":"Baby Blue"}}},{"option":{"Color":{"==":"Blue"}}},{"option":{"Color":{"==":"Dark Blue"}}},{"option":{"Color":{"==":"Denim Blue"}}},{"option":{"Color":{"==":"Grey Blue"}}},{"option":{"Color":{"==":"Light Blue"}}},{"option":{"Color":{"==":"Light Denim Blue"}}},{"option":{"Color":{"==":"Medium Blue"}}},{"option":{"Color":{"==":"Medium Denim Blue"}}},{"option":{"Color":{"==":"Midnight Blue"}}},{"option":{"Color":{"==":"Ocean Blue"}}},{"option":{"Color":{"==":"Royal Blue"}}},{"option":{"Color":{"==":"Sky Blue"}}},{"option":{"Color":{"==":"Navy"}}},{"option":{"Color":{"==":"Turquoise"}}},{"option":{"Color":{"==":"Aqua"}}},{"option":{"Color":{"==":"Teal"}}}]}]},{"or":[]},{"or":[]},{"or":[]},{"or":[]}]}],"attributes":{"count":true,"countBehavior":"approximate","allVariants":false,"rows":2,"page":1,"collection":"all","options":[{"option":{"Color":[{"name":"Black","value":["Black","Black With White","Black with White","Black with white","Pure Black","Solid Black"]},{"name":"Blue","value":["Baby Blue","Blue","Dark Blue","Denim Blue","Grey Blue","Light Blue","Light Denim Blue","Medium Blue","Medium Denim Blue","Midnight Blue","Ocean Blue","Royal Blue","Sky Blue","Navy","Turquoise","Aqua","Teal"]},{"name":"Brown","value":["Brown","Copper","Tan"]},{"name":"Beige","value":["Beige","Natural","Camel","Oatmeal","Sand"]},{"name":"Red","value":["Red","Burgundy","Wine"]},{"name":"Green","value":["Green","Dark Green","Hunter Green","Camouflage","Jade","Khaki","Sage","Neon Green"]},{"name":"Grey","value":["Dark Grey","Grey","Heather Grey","Light Grey","Charcoal","Silver"]},{"name":"Orange","value":["Coral","Dark Orange","Neon Orange","Orange","Peach"]},{"name":"Pink","value":["Dark Pink","Light Pink","Neon Pink","Pink","Dusty Rose","Rose"]},{"name":"Purple","value":["Lilac","Magenta","Dark Purple","Eggplant","Fuchsia","Purple"]},{"name":"Yellow","value":["Neon Yellow","Pale Yellow","Yellow","Mustard","Gold"]},{"name":"Plaid","value":["Plaid","Gingham"]},{"name":"Multi","value":["Assorted","Multi"]},{"name":"Neon","value":["Neon Orange","Neon Pink","Neon Yellow","Neon Green"]},{"name":"Camouflage","value":["Camouflage"]}]}},{"option":{"Size":[{"name":"One Size","value":["One Size","O/S"]},{"name":"S (7/8)","value":["S (7/8)","7/8"]},{"name":"XS (6)","value":["XS (6)"]},{"name":"M (10/12)","value":["M (10/12)","9/10","11/12"]},{"name":"L (14/16)","value":["L (14/16)","L (14)","15/16","13/14"]},{"name":"XL (16)","value":["XL (16)"]}]}},{"price":["-10","10-25","25-50","50-75","75-100","100+"]},{"tags":[{"name":"English","value":"language_en"},{"name":"French","value":"language_fr"}]},{"tags":["subtype joggers","subtype lanyards","subtype leggings","subtype long sleeves","subtype longboard","subtype longboards"]}],"optionBehavior":"approximate","optionDisjunctive":true,"split":"auto","currency":{"code":"CAD","rate":"1.0"},"locale":"en"}}

	Response

	< HTTP/1.1 200 OK
	< Content-Length: 2182
	< Access-Control-Expose-Headers: X-Session-Id
	< Access-Control-Allow-Headers: X-Shopify-Shop-Domain,Content-Type,Accept,X-Requested-With,X-Session-Id
	< Access-Control-Allow-Credentials: true
	< Access-Control-Allow-Method: GET,POST
	< Access-Control-Allow-Origin: https://spot-development.myshopify.com
	< Content-Type: application/json; charset=UTF-8
	< X-Session-Id: 25506ca4fd234c8af63e1952be1f60ea
	< Content-Encoding: deflate
	< Date: Thu, 13 Aug 2020 01:58:20 GMT
	< Via: 1.1 google
	< Alt-Svc: clear


	{"products":[{"metafields":[],"handle":"4632-18991954-abstract-art-cruiser-22","body_html":"Grab yourself a board to cruise around the streets this summer. Features abstract art throughout. Comes fully assembled and ready to ride.<br><br><li>Deck size: 22\"</li><br><li>Complete cruiser</li><br><li>Style #4632-18991954</li>","vendor":"West49","created_at":"2020-04-24T10:23:59-04:00","extra_updated":null,"published_at":"2019-07-22T14:04:53-04:00","title":"Abstract Art Cruiser - 22\"","template_suffix":null,"image":{"width":1333,"id":13914620067951,"updated_at":"2020-04-24T10:24:02-04:00","height":2000,"created_at":"2020-04-24T10:24:02-04:00","admin_graphql_api_id":"gid://shopify/ProductImage/13914620067951","variant_ids":[31460652515439],"position":1,"alt":null,"product_id":4429642203247,"src":"https://cdn.shopify.com/s/files/1/0271/5952/7535/products/463218991954-45-0.jpg?v=1587738242"},"variants":[{"metafields":[],"option2":"One Size","old_inventory_quantity":0,"taxable":true,"inventory_management":"shopify","inventory_quantity":0,"position":1,"created_at":"2020-04-24T10:23:59-04:00","option3":null,"compare_at_price":"59.99","requires_shipping":true,"inventory_item_id":33109512028271,"grams":0,"weight_unit":"kg","inventory_policy":"deny","product_id":4429642203247,"option1":"Light Blue","price":"49.00","image_id":13914620067951,"title":"Light Blue / One Size","admin_graphql_api_id":"gid://shopify/ProductVariant/31460652515439","sku":"4632-18991954-450093","fulfillment_service":"manual","id":"31460652515439","weight":0,"updated_at":"2020-04-24T10:24:02-04:00","barcode":"60198488"}],"options":[{"position":1,"name":"Color","product_id":4429642203247,"id":5764567924847,"values":["Light Blue"]},{"values":["One Size"],"id":5764567957615,"name":"Size","position":2,"product_id":4429642203247}],"tags":"apr5, brand amnesia, brand west49, language_en, locale_ca, markdown 49, private label, sale, subtype cruisers, type hardgoods","published_scope":"web","updated_at":"2020-04-24T10:24:05-04:00","id":"4429642203247","images":[{"product_id":4429642203247,"alt":null,"src":"https://cdn.shopify.com/s/files/1/0271/5952/7535/products/463218991954-45-0.jpg?v=1587738242","id":13914620067951,"width":1333,"updated_at":"2020-04-24T10:24:02-04:00","height":2000,"created_at":"2020-04-24T10:24:02-04:00","admin_graphql_api_id":"gid://shopify/ProductImage/13914620067951","position":1,"variant_ids":[31460652515439]},{"width":1333,"id":13914620559471,"updated_at":"2020-04-24T10:24:05-04:00","height":2000,"created_at":"2020-04-24T10:24:05-04:00","admin_graphql_api_id":"gid://shopify/ProductImage/13914620559471","position":2,"variant_ids":[],"alt":null,"product_id":4429642203247,"src":"https://cdn.shopify.com/s/files/1/0271/5952/7535/products/463218991954-45-1.jpg?v=1587738245"}],"product_type":"Cruisers","admin_graphql_api_id":"gid://shopify/Product/4429642203247"},{"body_html":"The Almost Color Bleed skateboard deck is made from high-quality materials and can be used at any skill level. Features an Almost logo at the bottom and a brown print on top.<br><br>\n<li>Width: 8\"</li>\n<li>Length: 31.6\"</li>\n<li>Wheelbase: 14\"</li>\n<li>7-ply construction</li>\n<li>Imported by Almost</li>\n<li>Style #4610-11563111</li>\n<li>Note: Grip tape not included</li>","vendor":"Almost","created_at":"2020-04-24T10:28:48-04:00","extra_updated":null,"metafields":[],"handle":"4610-11563111-almost-color-bleed-skateboard-deck-8","updated_at":"2020-04-24T10:28:55-04:00","published_scope":"web","tags":"8-Nov-18, brand amnesia, brand west49, branded, build your own skate, flow, flow deck, language_en, locale_ca, subtype decks, type hardgoods","images":[{"product_id":4429644300399,"alt":null,"src":"https://cdn.shopify.com/s/files/1/0271/5952/7535/products/461011563111-86-0.jpg?v=1587738533","position":1,"admin_graphql_api_id":"gid://shopify/ProductImage/13914633994351","variant_ids":[31460658905199],"created_at":"2020-04-24T10:28:53-04:00","id":13914633994351,"width":1333,"height":2000,"updated_at":"2020-04-24T10:28:53-04:00"},{"width":1333,"id":13914634190959,"height":2000,"updated_at":"2020-04-24T10:28:55-04:00","admin_graphql_api_id":"gid://shopify/ProductImage/13914634190959","position":2,"variant_ids":[],"created_at":"2020-04-24T10:28:55-04:00","alt":null,"product_id":4429644300399,"src":"https://cdn.shopify.com/s/files/1/0271/5952/7535/products/461011563111-86-1.jpg?v=1587738535"}],"id":"4429644300399","admin_graphql_api_id":"gid://shopify/Product/4429644300399","product_type":"Decks","template_suffix":"","published_at":"2019-07-22T14:01:49-04:00","title":"Almost Color Bleed Skateboard Deck 8\"","image":{"variant_ids":[31460658905199],"position":1,"admin_graphql_api_id":"gid://shopify/ProductImage/13914633994351","created_at":"2020-04-24T10:28:53-04:00","height":2000,"updated_at":"2020-04-24T10:28:53-04:00","id":13914633994351,"width":1333,"src":"https://cdn.shopify.com/s/files/1/0271/5952/7535/products/461011563111-86-0.jpg?v=1587738533","product_id":4429644300399,"alt":null},"variants":[{"title":"Teal / 8","image_id":13914633994351,"price":"59.99","option1":"Teal","inventory_policy":"deny","product_id":4429644300399,"weight_unit":"kg","barcode":"58236001","updated_at":"2020-04-24T10:28:53-04:00","weight":0,"id":"31460658905199","fulfillment_service":"manual","sku":"4610-11563111-860800","admin_graphql_api_id":"gid://shopify/ProductVariant/31460658905199","inventory_management":"shopify","taxable":true,"old_inventory_quantity":0,"option2":"8","metafields":[],"grams":0,"requires_shipping":true,"inventory_item_id":33109520941167,"compare_at_price":null,"option3":null,"created_at":"2020-04-24T10:28:48-04:00","position":1,"inventory_quantity":0}],"options":[{"product_id":4429644300399,"name":"Color","position":1,"values":["Teal"],"id":5764571103343},{"id":5764571136111,"values":["8"],"product_id":4429644300399,"position":2,"name":"Size"}]}],"options":[{"option":{"Color":{"Black":919,"Blue":456,"Brown":33,"Beige":69,"Red":194,"Green":344,"Grey":314,"Orange":131,"Pink":158,"Purple":149,"Yellow":132,"Plaid":30,"Multi":249,"Neon":48,"Camouflage":109}}},{"option":{"Size":{"One Size":82,"S (7/8)":88,"XS (6)":2,"M (10/12)":88,"L (14/16)":88,"XL (16)":96}}},{"price":{"-10":9,"10-25":153,"25-50":116,"50-75":28,"75-100":28,"100+":11}},{"tag":{"English":392}},{"tag":{"subtype joggers":11,"subtype lanyards":1,"subtype long sleeves":18,"subtype longboards":8}}],"count":456,"currency":{"code":"CAD","rate":1.000000}}

***/
